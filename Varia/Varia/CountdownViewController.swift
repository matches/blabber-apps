//
//  CountdownViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class CountdownViewController: GenericViewController, UICollectionViewDataSource {
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
		
	}
	
	var collectionView : UICollectionView!
	
	var filter : Filter?
	var sortMode = 0
	var gridMode = 0
	
	var countdownItems : [AnyObject] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor(white: 0.1, alpha: 1)//.whiteColor()
		
		self.topBarLabel.text = "Countdown"
		
		self.topBarLeftButton.setImage(UIImage(named: "search"), forState: .Normal)
		self.topBarLeftButton.setImage(UIImage(named: "search")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarLeftButton.addTarget(self, action: #selector(openSearch), forControlEvents: .TouchDown)
		
		self.topBarRightButton.setImage(UIImage(named: "filter"), forState: .Normal)
		self.topBarRightButton.setImage(UIImage(named: "filter")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarRightButton.addTarget(self, action: #selector(openFilter), forControlEvents: .TouchDown)
		
		self.setupCollectionView()
		
		self.sortMode = 1; self.reloadContent()
	}
	
	func openSearch() {
		
		let search = Search(owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.showModal(search)
		search.field.becomeFirstResponder()
	}
	
	func reloadContent() {
		
		let sortedOld = myGames.filter({ g in
			
			if g.releasedate != nil && g.addedat != nil {
				
				if g.releasedate?.compare(NSDate()) == .OrderedDescending {
					
					return true
				}
				return false
			}
			return false
		
		}).filter({ g in
			
			if g.releasedate != nil && sortMode == 1 { return true }
			else if g.addedat != nil && sortMode == 0 { return true }
			return false
		
		}).sort { (g1, g2) -> Bool in
			
			if (g1.releasedate?.compare(g2.releasedate!) == NSComparisonResult.OrderedDescending) && sortMode == 1 {
				
				return false
			}
			if (g1.addedat?.compare(g2.addedat!) == NSComparisonResult.OrderedDescending) && sortMode == 0 {
				
				return false
			}
			return true
		}
		
		var sortedNew : [AnyObject] = []
		var previousMonth : Int?
		var previousYear : Int?
		
		for g in sortedOld {
			
			var m : Int?
			var y : Int?
				
			if sortMode == 1 {
				m = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: g.releasedate!)
				y = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: g.releasedate!)
			}
			else {
				m = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: g.addedat!)
				y = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: g.addedat!)
			}
			
			if m != previousMonth || y != previousYear {
				
				if sortedNew.count > 1 {
					
					var itemsInPreviousSection = 0
					
					for (var x = sortedNew.count-1; x > -1; x -= 1) {
						
						if sortedNew[safe: x]?.isKindOfClass(Game.self) == false {
							
							break
						}
						itemsInPreviousSection += 1;
					}
					
					if itemsInPreviousSection % 2 != 0 {
						
						sortedNew.append(["blank":true])
					}
				}
				
				sortedNew.append(["isheader":true, "title": "\(NSDateFormatter().monthSymbols[m!-1]) \(y!)"])
			}
			
			sortedNew.append(g);
			
			previousMonth = m
			previousYear = y
		}
		
		self.countdownItems = sortedNew
		self.collectionView.reloadData()
	}
	
	func openFilter() {
		
		self.filter = Filter(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.filter?.mode = self.sortMode
		self.filter?.mode2 = self.gridMode
		self.filter?.switchTo = {  m, m2 in self.sortMode = m; self.gridMode = m2; self.reloadContent() }
		help.APPDELEGATE.tabBarController.view.addSubview(self.filter!)
		self.filter?.open()
	}
	
	//collection view
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.scrollDirection = .Vertical
		self.collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.view.frame.size), collectionViewLayout: layout)
		self.collectionView?.dataSource = self
		self.collectionView?.delegate = self
		self.collectionView?.registerClass(GameCell.self, forCellWithReuseIdentifier: "GameCell")
		self.collectionView?.backgroundColor = .clearColor()
		self.collectionView?.alpha = 1
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.alwaysBounceVertical = true
		self.collectionView?.scrollEnabled = true
		self.collectionView?.clipsToBounds = false
		self.collectionView?.tag = 0
		
		self.view.insertSubview(self.collectionView!, belowSubview: self.topBar)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		
		return CGFloat(13)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		if self.countdownItems.count == 0 { return UIEdgeInsetsZero }
		return UIEdgeInsets(top: -13, left: 14, bottom: 16, right: 14)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if self.countdownItems.count == 0 { return 1 }
		return self.countdownItems.count
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if self.countdownItems.count == 0 {
			
			return CGSize(width: self.view.width, height: self.view.height-114)
		}
		else if self.countdownItems[safe: indexPath.item]?.isKindOfClass(Game.self) == false {
			
			if self.countdownItems[safe: indexPath.item]?.valueForKey("blank") as? Bool == true && self.gridMode == 1 {
				return CGSizeZero
			}
			else if self.countdownItems[safe: indexPath.item]?.valueForKey("blank") as? Bool == true {
				return CGSize(width: (self.view.width/2)-20, height: (self.view.width/2)+60)
			}
			return CGSize(width: self.view.width, height: 52)
		}
		
		if self.gridMode == 1 {
			
			return CGSize(width: self.view.width-20, height: 100)
		}
		return CGSize(width: (self.view.width/2)-20, height: (self.view.width/2)+70)
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
		
		cell.layer.shouldRasterize = true
		cell.layer.rasterizationScale = UIScreen.mainScreen().scale
		cell.backgroundColor = .clearColor()
		cell.contentView.alpha = 1
		
		cell.indexPath = indexPath
		cell.label.alpha = 0
		cell.badge.text = ""
		cell.textView.alpha = 0
		cell.imageView.image = nil
		cell.imageView.alpha = 0
		cell.imageView.contentMode = .ScaleAspectFill
		let r = Int(arc4random_uniform(1000))
		cell.imageView.tag = r
		
		if self.countdownItems.count == 0 {
			
			cell.type = .Custom
			cell.imageView.alpha = 1
			cell.imageView.y = 0
			cell.imageView.width = self.view.width
			cell.imageView.height = self.view.height-124
			cell.imageView.contentMode = .Center
			cell.imageView.image = UIImage(named: "countdownBlank")
			
			cell.refresh()
		}
		else if let game = self.countdownItems[safe: indexPath.item] as? Game {
			
			cell.backgroundColor = .clearColor()
			cell.type = .Grid
			if self.gridMode == 1 { cell.type = .List }
			
			cell.textView.text = game.name
			cell.game = game
			
			if game.coverurl.length > 0 {
				
				let tempIP = cell.indexPath
				let tempCount = collectionView.numberOfItemsInSection(indexPath.section)
				
				getMediaBySrc(game.coverurl, game: game.id, completion: { img in
					
					if tempIP == cell.indexPath && r == cell.imageView.tag {
						
						cell.imageView.image = img
						UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
							cell.imageView.alpha = 1
							}, completion: nil)
					}
					else if tempCount == self.collectionView.numberOfItemsInSection(tempIP!.section) && tempIP != cell.indexPath {
						
						self.collectionView.reloadItemsAtIndexPaths([tempIP!, cell.indexPath!])
					}
					
				})
			}
			else {
				
				//cell.imageView.image = gradientImage(help.purple1, toColor: help.babyPurple)
				//cell.imageView.alpha = 1
			}
			
			cell.badge.text = game.releasedate?.timeElapsed().uppercaseString
			
			cell.refresh()
		}
		else if self.countdownItems[safe: indexPath.item]?.valueForKey("blank") as? Bool == true {
			
			cell.type = .Grid
			cell.refresh()
			cell.contentView.alpha = 0
			cell.backgroundColor = .clearColor()
		}
		else {
			cell.type = .LeftHeader
			cell.label.text = self.countdownItems[safe: indexPath.item]?.valueForKey("title") as? String
			
			cell.refresh()
		}
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if let g = self.countdownItems[safe: indexPath.item] as? Game {
			
			let v = GameScreen(game: g, owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
			
			self.showModal(v)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
