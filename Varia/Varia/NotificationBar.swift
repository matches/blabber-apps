//
//  NotificationBar.swift
//  Bunker
//
//  Created by Hazim Judi on 2016-02-15.
//  Copyright © 2016 Butlered Bits. All rights reserved.
//

import UIKit

enum NotificationType : Int {
    
    case Error = 0, Simple = 1, Blue = 2, Prompt = 3, Debug = 4
}

private var currentBar : Bar?

class Bar : UIButton {
	
}

/*func showNotificationBar(data: NSDictionary) {
    
    if let text = data.valueForKey("text") as? String, type = NotificationType(rawValue: data.valueForKey("type") as! Int) {
        
        if type == .Message {
            
            if let chat = data.valueForKey("chat") as? String {
                
                let b = notificationBar(text, type: type)
                b.chat = chat
                b.addTarget(b, action: #selector(Bar.switchToChat), forControlEvents: .TouchDown)
                showNotificationBar(text, type: type, barTemp: b)
            }
        }
        else {
            
            showNotificationBar(text, type: type)
        }
    }
}*/

func showNotificationBar(text: String?, type: NotificationType) {
    
    showNotificationBar(text, type: type, barTemp: nil)
}

func showNotificationBar(text: String?, type: NotificationType, barTemp: Bar?) {
	
	if UIApplication.sharedApplication().keyWindow == nil { return }
	
    var bar = barTemp
    
    if text != nil {
        
        print(type, ": \(text!)")
	}
	
	if type == .Debug { return }
	
    //
    
    currentBar?.removeFromSuperview()
    currentBar = nil
    
    //
    
    //
    
    let w = UIApplication.sharedApplication().keyWindow!
    let delay = 3
    
    if bar == nil { bar = notificationBar(text, type: type) }
    
    help.APPDELEGATE.tabBarController.view.insertSubview(bar!, belowSubview: help.APPDELEGATE.tabBarController.bar)
    currentBar = bar
	
	let tempY = currentBar!.y
    currentBar?.y = w.height
    
    UIView.animateWithDuration(0.1, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
        
        currentBar?.y = tempY
        
        }, completion: { fin in
            
    })
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(UInt64(delay) * UInt64(NSEC_PER_SEC))), dispatch_get_main_queue(), {
        
        hideBar()
    })
}

private func hideBar() {
    
    UIView.animateWithDuration(0.1, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
        
        currentBar?.y = UIApplication.sharedApplication().keyWindow!.height
        
        }, completion: { fin in
            
            currentBar?.removeFromSuperview()
            currentBar = nil
    })
}

private func notificationBar(text: String?, type: NotificationType) -> Bar {
    
    let w = help.APPDELEGATE.window!
	var activeVCTopBarY = CGFloat(0)
	if let y = help.APPDELEGATE.tabBarController.bar {
		
		activeVCTopBarY = y.y - 50
	}
    
    let bar = Bar(frame: CGRect(x: 0, y: activeVCTopBarY, width: w.width, height: 50))
    bar.setTitle(text, forState: .Normal)
    bar.titleLabel?.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
    
    var textColor = UIColor.whiteColor()
    
    if type == .Error {
        
        bar.backgroundColor = hexToColor("ff4157")
    }
    if type == .Simple || type == .Debug {
        
        bar.backgroundColor = UIColor(white: 0.92, alpha: 1)
        textColor = UIColor(white: 0, alpha: 0.5)
    }
    if type == .Blue {
        
        bar.backgroundColor = help.babyBlue
	}
    if type == .Prompt {
        
        bar.backgroundColor = hexToColor("ffed54")
        textColor = .blackColor()
    }
    
    bar.setTitleColor(textColor, forState: .Normal)
    
    return bar
}