//
//  Search.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-17.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Search: GenericModal, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
	
	var bar:UIVisualEffectView!
	var glass:UIImageView!
	var field:UITextField!
	var closeButton:UIButton!
	
	var collectionView:UICollectionView!
	var results : [AnyObject] = []
	
	var keyboardHeight = CGFloat(0)
	
	override init(owner: GenericViewController, frame: CGRect) {
		super.init(owner: owner, frame: frame)
		
		self.side = false
		
		self.backgroundColor = UIColor(white: 0.1, alpha: 1)
		
		self.bar = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.width, height: 64))
		self.bar.effect = UIBlurEffect(style: .Dark)
		self.bar.backgroundColor = UIColor(white: 0.4, alpha: 0.8)//help.purple1
		self.addSubview(self.bar)
		
		self.glass = UIImageView(frame: CGRect(x: 0, y: 20, width: 48, height: 44))
		self.glass.image = UIImage(named: "search")
		self.glass.contentMode = .Center
		self.bar.addSubview(self.glass)
		
		self.field = UITextField(frame: CGRect(x: 50, y: 20, width: frame.width-50-50, height: 46))
		self.field.delegate = self
		self.field.autocorrectionType = .No
		self.field.autocapitalizationType = .None
		self.field.font = UIFont.systemFontOfSize(17)
		self.field.textColor = UIColor.whiteColor()
		self.field.attributedPlaceholder = NSAttributedString(string: "Search games", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6),NSFontAttributeName:self.field.font!])
		self.bar.addSubview(self.field)
		
		self.closeButton = UIButton(frame: CGRect(x: frame.width-48, y: 20, width: 48, height: 44))
		self.closeButton.setImage(UIImage(named: "close"), forState: .Normal)
		self.closeButton.addTarget(self, action: #selector(close), forControlEvents: .TouchDown)
		self.bar.addSubview(self.closeButton)
		
		self.setupCollectionView()
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillChangeSize), name: UIKeyboardWillChangeFrameNotification, object: nil)
	}

	func close() {
		
		self.closing = true
		self.owner?.hideLastModalAnimated()
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		return true
	}
	
	var searchToken = 0
	
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		
		if NSString(string: self.field.text!).stringByReplacingCharactersInRange(range, withString: string).length > 0 {
			
			self.collectionView.alpha = 1
			
			if let user = User.userID {
				
				cancelAllDownloads()
				
				self.searchToken = Int(arc4random_uniform(1000))
				
				socket.emitWithAck("search", ["term":NSString(string: self.field.text!).stringByReplacingCharactersInRange(range, withString: string), "user":user, "token": self.searchToken])(timeoutAfter: 3, callback: { data in
					//print(data)
					if let results = data[safe: 0]?["results"] as? [NSDictionary] {
						
						dispatch_async(dispatch_get_main_queue(), {
							
							if data[safe: 0]?["token"] as? String != self.searchToken.description { return }
							
							self.results.removeAll()
							self.results = results
							self.collectionView.reloadData()
						})
					}
					else {
						
						showNotificationBar("Couldn't find games, try again!", type: .Error)
					}
				})
			}
		}
		else {
			
			self.collectionView.alpha = 0
		}
		
		
		return true
	}
	
	
	
	//collection view
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.scrollDirection = .Vertical
		self.collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.frame.size), collectionViewLayout: layout)
		self.collectionView?.dataSource = self
		self.collectionView?.delegate = self
		self.collectionView?.registerClass(GameCell.self, forCellWithReuseIdentifier: "GameCell")
		self.collectionView?.backgroundColor = .clearColor()
		self.collectionView?.alpha = 1
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight, right: 0)
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight, right: 0)
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.alwaysBounceVertical = true
		self.collectionView?.scrollEnabled = true
		self.collectionView?.clipsToBounds = true
		self.collectionView?.tag = 0
		
		self.insertSubview(self.collectionView, belowSubview: self.bar)
	}

	func scrollViewWillBeginDragging(scrollView: UIScrollView) {
		
		self.field.resignFirstResponder()
	}
	
	func textFieldDidEndEditing(textField: UITextField) {
		
		self.keyboardHeight = 0
		
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight+50, right: 0)
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight+50, right: 0)
	}
	
	func textFieldDidBeginEditing(textField: UITextField) {
		
		self.collectionView.setContentOffset(CGPoint(x: 0, y: -64), animated: true)
	}
	
	func keyboardWillChangeSize(notification: NSNotification) {
		
		let info = notification.userInfo
		let keyboardRect = info?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
		self.keyboardHeight = keyboardRect!.height
		
		
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight, right: 0)
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: self.keyboardHeight, right: 0)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		
		return CGFloat(13)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		return UIEdgeInsets(top: 16, left: 14, bottom: 16, right: 14)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		
		return 1
		
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		return self.results.count
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		return CGSize(width: (self.width/2)-20, height: (self.width/2)+70)
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
		
		cell.indexPath = indexPath
		cell.layer.shouldRasterize = true
		cell.layer.rasterizationScale = UIScreen.mainScreen().scale
		cell.backgroundColor = .clearColor()
		
		cell.label.alpha = 0
		cell.textView.alpha = 0
		cell.imageView.image = nil
		cell.imageView.alpha = 0
		let r = Int(arc4random_uniform(1000))
		cell.imageView.tag = r
		
		if let o = self.results[safe: indexPath.item] as? NSDictionary {
			
			cell.type = .Grid
			cell.textView.text = (o.valueForKey("name") as? String)?.stringByRemovingPercentEncoding
			
			if (o.valueForKey("cover_url") as? String)?.length > 0 || (o.valueForKey("cover_url_override") as? String)?.length > 0 {
				
				let tempIP = cell.indexPath
				let tempCount = collectionView.numberOfItemsInSection(indexPath.section)
    
				var c = o.valueForKey("cover_url") as! String
				if (o.valueForKey("cover_url_override") as? String)?.length > 0 { c = o.valueForKey("cover_url_override") as! String }
				
				getMediaBySrc(c, game: o.valueForKey("id") as! Int, completion: { img in
					
					if tempIP == cell.indexPath && r == cell.imageView.tag {
						
						cell.imageView.image = img
						UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
							cell.imageView.alpha = 1
							}, completion: nil)
					}
					else if tempCount == self.collectionView.numberOfItemsInSection(tempIP!.section) && tempIP != cell.indexPath {
						
						self.collectionView.reloadItemsAtIndexPaths([tempIP!, cell.indexPath!])
					}
					
				})
			}
			else {
				
				//cell.imageView.image = gradientImage(help.purple1, toColor: help.babyPurple)
				//cell.imageView.alpha = 1
			}
			
			cell.refresh()
		}
		else {
		}
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		self.field.resignFirstResponder()
		
		if let g = results[safe: indexPath.item] {
			
			let v = GameScreen(game: g, owner: self.owner!, frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
			
			self.owner?.showModal(v)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
