//
//  Start.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-25.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Start: UIView, UITextFieldDelegate {
	
	var step1 : UIView!
	
	var step2 : UIView!
	var terms : UIView!
	var name : UITextField!
	var keyboardHeight = CGFloat(0)
	
	var alert : UIImageView!
	var step3 : UIView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.backgroundColor = help.darkBlue
		
		var bodySize = CGFloat(22)
		
		if help.APPDELEGATE.window?.frame.width < 375 {
			
			bodySize = 18
		}
		
		self.step1 = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.addSubview(self.step1)
		self.step1.alpha = 1
		
		let logo = UIImageView(frame: CGRect(x: self.width*0.12, y: 0, width: self.width*0.75, height: self.height*0.7))
		logo.contentMode = .ScaleAspectFit
		logo.image = UIImage(named: "bigLogo")?.fillWithColor(hexToColor("4f5769")!)
		self.step1.addSubview(logo)
		
		let attStr = NSMutableAttributedString(string: "Varia", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(bodySize, weight: UIFontWeightBold), NSParagraphStyleAttributeName:paragraphStyleWithLineHeight(2, alignment: .Center)])
		attStr.appendAttributedString(NSAttributedString(string: " gives you an anonymous profile you can use to save and track game updates and reviews.", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(bodySize, weight: UIFontWeightRegular), NSParagraphStyleAttributeName:paragraphStyleWithLineHeight(2, alignment: .Center)]))
		attStr.appendAttributedString(NSAttributedString(string: "\n\nTo use Varia, you must agree to the ", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6), NSFontAttributeName:UIFont.systemFontOfSize(15, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: c]))
		attStr.appendAttributedString(NSAttributedString(string: "Terms.", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.8), NSFontAttributeName:UIFont.systemFontOfSize(15, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: c]))
		
		let tv = UITextView(frame: CGRect(x: 15, y: self.height*0.62, width: self.width-30, height: 160))
		tv.backgroundColor = .clearColor()
		tv.selectable = false
		tv.editable = false
		tv.scrollEnabled = false
		tv.attributedText = attStr
		self.step1.addSubview(tv)
		
		tv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTerms)))
		
		self.terms = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.terms.backgroundColor = help.darkBlue.lighterByPercentage(5).colorWithAlphaComponent(0.95)
		self.addSubview(self.terms)
		self.terms.alpha = 0
		
		let tv2 = UITextView(frame: CGRect(x: 12, y: 50, width: self.width-24, height: self.height-50))
		tv2.editable = false
		tv2.backgroundColor = .clearColor()
		tv2.font = UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)
		tv2.textColor = .whiteColor()
		tv2.text = "Two thousand dollars a pound seems like a lot to pay for a mushroom. It really does. Yes, October marks the start of white-truffle season, the time of year when the rare mushrooms are showered on dishes, signifying luxury to even the most jaded palates. One of Daniel Boulud's favorite stories involves Puff Daddy, as he was known at the time, urging the chef to \"shave that bitch\" onto his food; Boulud told me that he obliged (as, I'm sure, the bill mounted accordingly). Truffles are rare. The white ones are only available a couple of months of the year, almost exclusively from one part of Italy, where they must be foraged by special pigs, and there are fewer of them, and of lesser quality, every year. They are, in short, the perfect luxury commodity, precious and getting more so all the time. Whether they are worth the money has a lot to do with how you like to spend and why you go to dinner. Which makes them very interesting to me. There's no question that white truffles have a unique aroma, a combination of newly plowed soil, fall rain, burrowing earthworms and the pungent memory of lost youth and old love affairs. I literally was not able to find a chef who doesn't love them. The most eloquent was Alex Guarnaschelli of Butter, a highly fashionable restaurant in New York City that caters to a moneyed clientele. The way truffles smell is \"disconcerting,\" she says. \"It conjures up images of a locker room. But the aroma deceptively conceals their complex yet delicate taste. They are sublime.\" Guarnaschelli shaves them over risotto or mashed potatoes, and likes them a little warm; other chefs find a little creamy or buttery pasta the perfect vehicle. They are all as careful in their handling of it as a museum curator moving the Mona Lisa. This is a mushroom, mind you."
		self.terms.addSubview(tv2)
		
		let l = UILabel(frame: CGRect(x: 0, y: 0, width: self.width, height: 50))
		l.attributedText = NSAttributedString(string: "Terms of Use", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(20, weight: UIFontWeightBold)])
		l.textAlignment = .Center
		self.terms.addSubview(l)
		
		let cl = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
		cl.setImage(UIImage(named: "close"), forState: .Normal)
		cl.addTarget(self, action: #selector(hideTerms), forControlEvents: .TouchDown)
		self.terms.addSubview(cl)
		
		let agree = UIButton(frame: CGRect(x: 30, y: self.height-80, width: self.width-60, height: 50))
		agree.backgroundColor = help.babyBlue
		agree.layer.cornerRadius = 12
		agree.layer.masksToBounds = true
		agree.setAttributedTitle(NSAttributedString(string: "Yes, I agree", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Normal)
		agree.setAttributedTitle(NSAttributedString(string: "Yes, I agree", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Highlighted)
		agree.setBackgroundImage(help.babyBlue.darkerByPercentage(10).toImage(), forState: .Highlighted)
		agree.addTarget(self, action: #selector(self.agree), forControlEvents: .TouchUpInside)
		self.step1.addSubview(agree)
		
		self.step2 = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.addSubview(self.step2)
		self.step2.alpha = 0
		
		self.name = UITextField(frame: CGRect(x: 0, y: self.height, width: self.width, height: 30))
		self.name.backgroundColor = .clearColor()
		self.name.attributedPlaceholder = NSAttributedString(string: "Make your nickname", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.5), NSFontAttributeName: UIFont.systemFontOfSize(26, weight: UIFontWeightRegular)])
		self.name.textColor = .whiteColor()
		self.name.font = UIFont.systemFontOfSize(26, weight: UIFontWeightRegular)
		self.name.textAlignment = .Center
		self.name.delegate = self
		self.name.tintColor = help.babyBlue
		self.name.autocorrectionType = .No
		self.name.autocapitalizationType = .None
		self.name.returnKeyType = .Done
		self.name.alpha = 0
		self.step2.addSubview(self.name)
		
		let skip = UIButton(frame: CGRect(x: 0, y: 0, width: 84, height: 80))
		skip.setAttributedTitle(NSAttributedString(string: "Skip", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.7), NSFontAttributeName: UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)]), forState: .Normal)
		skip.addTarget(self, action: #selector(alerts), forControlEvents: .TouchDown)
		self.step2.addSubview(skip)
		
		let done = UIButton(frame: CGRect(x: self.width-84, y: 0, width: 84, height: 80))
		done.setAttributedTitle(NSAttributedString(string: "Save", attributes: [NSForegroundColorAttributeName:help.babyBlue, NSFontAttributeName: UIFont.systemFontOfSize(16, weight: UIFontWeightBold)]), forState: .Normal)
		done.addTarget(self, action: #selector(save), forControlEvents: .TouchDown)
		self.step2.addSubview(done)
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillChangeSize), name: UIKeyboardWillChangeFrameNotification, object: nil)
		
		self.step3 = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.step3.alpha = 0
		self.addSubview(self.step3)
		
		let attStr2 = NSMutableAttributedString(string: "To alert you when Added games get updated or released, Varia sends you push notifications.\n\nWould you like to enable this?", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(bodySize, weight: UIFontWeightRegular), NSParagraphStyleAttributeName:paragraphStyleWithLineHeight(2, alignment: .Center)])
		
		let tv3 = UITextView(frame: CGRect(x: 20, y: self.height*0.6, width: self.width-40, height: 200))
		tv3.backgroundColor = .clearColor()
		tv3.selectable = false
		tv3.editable = false
		tv3.scrollEnabled = false
		tv3.attributedText = attStr2
		self.step3.addSubview(tv3)
		
		self.alert = UIImageView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height*0.65))
		self.alert.contentMode = .Center
		self.alert.image = UIImage(named: "alert")
		self.step3.addSubview(self.alert)
		
		let no = UIButton(frame: CGRect(x: 30, y: self.height-80, width: (self.width*0.35)-10, height: 50))
		no.backgroundColor = hexToColor("ef6060")
		no.layer.cornerRadius = 12
		no.layer.masksToBounds = true
		no.setAttributedTitle(NSAttributedString(string: "No thanks", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Normal)
		no.setAttributedTitle(NSAttributedString(string: "No thanks", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Highlighted)
		no.setBackgroundImage(hexToColor("ef6060")!.darkerByPercentage(10).toImage(), forState: .Highlighted)
		no.addTarget(self, action: #selector(self.no), forControlEvents: .TouchUpInside)
		self.step3.addSubview(no)
		
		let okay = UIButton(frame: CGRect(x: 30+(self.width*0.35), y: self.height-80, width: (self.width-60)-(self.width*0.35)-10, height: 50))
		okay.backgroundColor = help.babyBlue
		okay.layer.cornerRadius = 12
		okay.layer.masksToBounds = true
		okay.setAttributedTitle(NSAttributedString(string: "Okay", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Normal)
		okay.setAttributedTitle(NSAttributedString(string: "Okay", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Highlighted)
		okay.setBackgroundImage(help.babyBlue.darkerByPercentage(10).toImage(), forState: .Highlighted)
		okay.addTarget(self, action: #selector(self.okay), forControlEvents: .TouchUpInside)
		self.step3.addSubview(okay)
		
		
		if help.APPDELEGATE.window?.frame.width < 375 {
			
			logo.y -= 40
			tv.y -= 40
			tv3.y -= 30
		}
	}
	
	func no() {
		
		User.setNotifications(false)
		self.close()
	}
	
	func okay() {
		
		User.setNotifications(true)
		help.APPDELEGATE.evaluateIfShouldRegisterForNotifications()
		self.close()
	}
	
	func keyboardWillChangeSize(notification: NSNotification) {
		
		let info = notification.userInfo
		let keyboardRect = info?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
		self.keyboardHeight = keyboardRect!.height
		
		self.name.y = (self.height-self.keyboardHeight)/2-15
	}
	
	func textFieldDidBeginEditing(textField: UITextField) {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.name.alpha = 1
			
			}, completion: nil)
	}
	
	func agree() {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.step1.alpha = 0
			self.step2.alpha = 1
			self.name.becomeFirstResponder()
			
			}, completion: nil)
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		if textField.text?.length == 0 { return false }
		
		textField.resignFirstResponder()
		
		self.save()
		
		return true
	}
	
	func alerts() {
		
		self.name.resignFirstResponder()
		self.alert.alpha = 0
		self.alert.y += 100
		
		UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
			
			self.step2.alpha = 0
			self.step3.alpha = 1
			self.alert.alpha = 1
			self.alert.y -= 100
			
			}, completion: nil)
	}
	
	func save() {
		
		if self.name.text?.length == 0 { return }
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("editUser", [creds, ["name" : self.name.text!]])(timeoutAfter: 5, callback: { data in
			
				if let user = data[safe: 0]?["user"] as? NSDictionary {
			
					User.setUserData(user)
					profileVC?.reloadContent()
				}
			})
		}
		
		self.alerts()
	}
	
	func close() {
		
		
		UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.alpha = 0
			
			}, completion: { f in
				
				self.removeFromSuperview()
		})
		
		
		help.APPDELEGATE.tabBarController.statusBarHidden = false
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.2)
	}
	
	func showTerms() {
		
		self.bringSubviewToFront(self.terms)
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.terms.alpha = 1
			
			}, completion: { f in
		})
	}
	
	func hideTerms() {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.terms.alpha = 0
			
			}, completion: { f in
		})
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
