//
//  GameCell.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

let c = paragraphStyleWithLineHeight(nil, alignment: .Center)
let l = paragraphStyleWithLineHeight(nil, alignment: .Left)

class GameCell: UICollectionViewCell {
	
	var game : Game?
	var indexPath : NSIndexPath?
	var type = GameCellType.Grid
	var label : UILabel!
	var textView : UITextView!
	var imageView : UIImageView!
	var month : UILabel!
	var logo : UIImageView!
	var badge : UILabel!
	var placeholder : UIImageView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.clipsToBounds = true
		
		self.placeholder = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
		self.placeholder.image = UIImage(named: "placeholder")?.fillWithColor(UIColor(white: 1, alpha: 0.2))
		self.placeholder.contentMode = .Center
		self.placeholder.clipsToBounds = true
		self.contentView.addSubview(self.placeholder)
		
		self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
		self.imageView.contentMode = .ScaleAspectFill
		self.imageView.clipsToBounds = true
		self.contentView.addSubview(self.imageView)
		
		self.label = UILabel(frame: CGRect(x: 14, y: 10, width: 200, height: 25))
		self.contentView.addSubview(self.label)
		
		self.month = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 25))
		self.contentView.addSubview(self.month)
		
		self.textView = UITextView(frame: CGRect(x: 0, y: 0, width: 0, height: 28))
		self.textView.backgroundColor = .clearColor()
		self.textView.userInteractionEnabled = false
		self.textView.clipsToBounds = true
		self.contentView.addSubview(self.textView)
		
		self.logo = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
		self.logo.contentMode = .Center
		self.contentView.addSubview(self.logo)
		
		self.badge = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 32))
		self.badge.backgroundColor = help.yellow
		
		let l = CAShapeLayer(); l.path = UIBezierPath(roundedRect: self.badge.layer.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(UIRectCorner.TopLeft), cornerRadii: CGSize(width: 6, height: 6)).CGPath
		self.badge.layer.mask = l
		
		self.badge.textAlignment = .Center
		self.badge.layer.masksToBounds = true
		self.contentView.addSubview(self.badge)
	}
	
	func refresh() {
		
		self.month.alpha = 0
		self.placeholder.alpha = 0
		
		if self.type == .Grid || self.type == .List {
			
			self.backgroundColor = help.babyPurple.darkerByPercentage(12)
			self.backgroundColor = UIColor(white: 0.15, alpha: 1)
			self.layer.cornerRadius = 6
			
			self.textView.backgroundColor = UIColor(white: 0.2, alpha: 0.96)
		}
		
		if self.type == .Grid {
			
			self.textView.attributedText = NSAttributedString(string: self.textView.text!, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold), NSParagraphStyleAttributeName:l])
			
			self.textView.height = 65
			self.textView.y = self.height-65
			self.textView.x = 0
			self.textView.width = self.width
			
			self.textView.textContainer.size.height = 44
			self.textView.textContainerInset = UIEdgeInsets(top: 7, left: 6, bottom: 0, right: 6)
			
			self.textView.alpha = 1
			
			self.imageView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height-65)
			//self.imageView.layer.cornerRadius = 6
			//self.imageView.layer.masksToBounds = true
			self.placeholder.alpha = 1
			self.placeholder.width = self.width
			self.placeholder.height = self.height-70
		}
		else if self.type == .List {
			
			let attStr = NSMutableAttributedString(string: self.textView.text!+"\n", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightBold), NSParagraphStyleAttributeName:paragraphStyleWithLineHeight(2, alignment: .Left)])
			
			if self.game?.releasedate != nil {
				
				let dd = NSCalendar.currentCalendar().component(NSCalendarUnit.Day, fromDate: self.game!.releasedate!)
				let mm = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: self.game!.releasedate!)-1
				let yy = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: self.game!.releasedate!)
				let rd = "\(NSCalendar.currentCalendar().monthSymbols[mm]) \(dd), \(yy)"
				
				attStr.appendAttributedString(NSAttributedString(string: rd, attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular), NSParagraphStyleAttributeName:l]))
			}
			
			self.textView.attributedText = attStr
			
			self.backgroundColor = UIColor(white: 0.2, alpha: 0.96)
			self.textView.backgroundColor = .clearColor()
			
			self.textView.width = self.width-self.height-20
			self.textView.sizeToFit()
			self.textView.y = self.height/2-self.textView.height/2-2
			//self.textView.height = 65
			self.textView.x = self.height+20
			
			//self.textView.textContainer.size.height = 44
			//self.textView.textContainerInset = UIEdgeInsets(top: 7, left: 6, bottom: 0, right: 6)
			self.textView.alpha = 1
			
			self.imageView.frame = CGRect(x: 0, y: 0, width: self.height+10, height: self.height)
			
			self.placeholder.alpha = 1
			self.placeholder.width = self.height+10
			self.placeholder.height = self.height
		}
		else if self.type == .GridReview {
			
			self.backgroundColor = help.babyPurple.darkerByPercentage(12)
			self.backgroundColor = UIColor(white: 0.2, alpha: 1)
			self.layer.cornerRadius = 6
			
			self.label.attributedText = NSAttributedString(string: self.label.text!, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(34, weight: UIFontWeightBold),NSKernAttributeName:0.5, NSParagraphStyleAttributeName:c])
			self.label.y = self.height/2-26
			self.label.alpha = 1
			self.label.width = self.width-26
			self.label.height = 50
			
			self.imageView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
		}
		else if self.type == .LeftHeader {
			
			self.backgroundColor = UIColor(white: 0.15, alpha: 1)//help.babyPurple
			self.layer.cornerRadius = 0
			
			self.label.height = self.height-10
			self.label.attributedText = NSAttributedString(string: self.label.text!.uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()/*help.purple1*/, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold), NSKernAttributeName:0.5, NSParagraphStyleAttributeName:l])
			self.label.alpha = 1
			self.label.y = 16
			self.label.width = 200
			self.label.height = 25
			
			if self.month.text?.length > 0 {
				
				self.month.alpha = 1
				self.month.y = 16
				self.month.x = self.width-215
				self.month.attributedText = NSAttributedString(string: self.month.text!.uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()/*.darkerByPercentage(6)*/, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold), NSKernAttributeName:0.5, NSParagraphStyleAttributeName:paragraphStyleWithLineHeight(nil, alignment: .Right)])
			}
		}
		else if self.type == .Badge {
			
			self.layer.cornerRadius = 0
		}
		
		if self.badge.text?.length > 0 {
			
			self.badge.alpha = 1
			self.badge.attributedText = NSAttributedString(string: self.badge.text!, attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.8),NSFontAttributeName:UIFont.systemFontOfSize(13, weight: UIFontWeightBold),NSKernAttributeName:0.5, NSParagraphStyleAttributeName:c])
			self.badge.layoutIfNeeded()
			self.badge.sizeToFit()
			self.badge.width += 16
			self.badge.height = 32
			let l = CAShapeLayer(); l.path = UIBezierPath(roundedRect: self.badge.layer.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(UIRectCorner.TopLeft), cornerRadii: CGSize(width: 6, height: 6)).CGPath
			self.badge.layer.mask = l
			self.badge.x = self.width-self.badge.width
			self.badge.y = 8
			
			if self.type == .List {
				
				self.badge.x = self.height-self.badge.width+10
			}
			
			if self.type == .Badge {
				
				self.badge.y = 0
				self.badge.x = 0
				let l = CAShapeLayer(); l.path = UIBezierPath(roundedRect: self.badge.layer.bounds, byRoundingCorners: UIRectCorner.BottomRight.union(UIRectCorner.TopRight), cornerRadii: CGSize(width: 6, height: 6)).CGPath
				self.badge.layer.mask = l
			}
		}
		else {
			
			self.badge.alpha = 0
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}


enum GameCellType {
		
	case Grid
	case GridReview
	case List
	case LeftHeader
	case Custom
	case Badge
}
