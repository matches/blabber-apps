//
//  Games.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-20.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import RealmSwift

var myGames : [Game] = []
var myGamesReviewed : [Game] { return myGames.filter({ g in return g.reviewed }) }

class Game: Object {
	
	dynamic var name = ""
	dynamic var id = 0
	dynamic var igdbid = 0
	dynamic var summary = ""
	dynamic var releasedate : NSDate? = nil
	dynamic var companies = ""
	dynamic var genre = "Action"
	dynamic var genres = "[]"
	dynamic var platforms = ""
	dynamic var metascore = ""
	dynamic var coverurl = ""
	dynamic var screenshots = ""
	
	dynamic var reviewed = false
	dynamic var rating = 0
	dynamic var state = "upcoming"
	dynamic var addedat : NSDate? = nil
	
	convenience init(obj: NSDictionary) {
		self.init()
		
		self.name = (obj.valueForKey("name") as! String).stringByRemovingPercentEncoding!
		
		if let aa = obj.valueForKey("addedat") as? String {
			self.addedat = help.APPDELEGATE.dateFormatter.dateFromString(aa)
		}
		
		self.id = obj.valueForKey("id") as! Int
		
		if let st = obj.valueForKey("state") as? String {
			self.state = st
		}
		
		if let su = (obj.valueForKey("description") as? String)?.stringByRemovingPercentEncoding! {
			self.summary = su
		}
		
		
		if let c = obj.valueForKey("cover_url") as? String {
			
			self.coverurl = c
		}
		
		if (obj.valueForKey("cover_url_override") as? String)?.length > 0 {
			
			self.coverurl = obj.valueForKey("cover_url_override") as! String
		}
		
		if let g = (obj.valueForKey("genres") as? String)?.stringByRemovingPercentEncoding?.lowercaseString {
			
			self.genres = g
			
			if g.rangeOfString("shooter") != nil {
				
				self.genre = "Shooter"
			}
			else if g.rangeOfString("arcade") != nil {
				
				self.genre = "Arcade"
			}
			else if g.rangeOfString("sim") != nil {
				
				self.genre = "Simulation"
			}
			else if g.rangeOfString("strategy") != nil {
				
				self.genre = "Strategy"
			}
			else if g.rangeOfString("adventure") != nil {
				
				self.genre = "Adventure"
			}
		}
		if let pfd = ((obj.valueForKey("platforms") as? String)?.stringByRemovingPercentEncoding)?.dataUsingEncoding(NSUTF8StringEncoding) {
			
			if let pf = try? NSJSONSerialization.JSONObjectWithData(pfd, options: .MutableContainers) as? [String] {
				
				var platforms = ""
				
				for p in pf! {
					
					var ps = p
					if (ps.rangeOfString("Windows") != nil) { ps = "PC" }
					if (ps.rangeOfString("PlayStation 3") != nil) { ps = "PS3" }
					if (ps.rangeOfString("PlayStation 4") != nil) { ps = "PS4" }
					
					platforms += ps+", "
				}
				
				if pf?.count > 0 {
					
					self.platforms = platforms.substringToIndex(platforms.endIndex.advancedBy(-2))
				}
			}
		}
		if let ss = (obj.valueForKey("screenshots") as? String)?.stringByRemovingPercentEncoding {
			self.screenshots = ss
		}
		if let cd = ((obj.valueForKey("companies") as? String)?.stringByRemovingPercentEncoding)?.dataUsingEncoding(NSUTF8StringEncoding) {
			
			if let ca = try? NSJSONSerialization.JSONObjectWithData(cd, options: .MutableContainers) as? [String] {
				
				var companies = ""
				
				for c in ca! {
					
					companies += c+", "
				}
				
				if ca?.count > 0 {
					
					self.companies = companies.substringToIndex(companies.endIndex.advancedBy(-2))
				}
			}
		}
		if let igdbid = obj.valueForKey("igdb_id") as? Int {
			
			self.igdbid = igdbid
		}
		if let rat = obj.valueForKey("rating") as? Int {
			
			self.rating = rat
		}
		if let ms = (obj.valueForKey("metascore") as? Int)?.description {
			self.metascore = ms
		}
		if let r = (obj.valueForKey("reviewed") as? Int)?.description.toBool() {
			self.reviewed = r
		}
		if let rd = obj.valueForKey("releasedate") as? String {
			if let rdd = help.APPDELEGATE.dateFormatter.dateFromString(rd) {
				self.releasedate = rdd
			}
		}
	}
	
	override static func primaryKey() -> String? {
		
		return "id"
	}
}

func dropGames(gamesTemp: NSArray) {
	
	myGames.removeAll()
	let games = gamesTemp.reverse() as NSArray
	
	var finishDrop : Optional<()->()>
	var iterate : Optional<()->()>
	var g = 0
	
	iterate = {
		
		if g == games.count {
			
			//dispatch_async(dispatch_get_main_queue(), {
				
				finishDrop?()
			//})
			return
		}
		
		if let gameObj = games[g] as? NSDictionary {
			
			var addedAt : NSDate? = nil
			
			if let lma = gameObj.valueForKey("addedat") as? String {
				
				addedAt = help.APPDELEGATE.dateFormatter.dateFromString(lma)
				
				if lma.length > 0 && addedAt != nil && (addedAt?.compare(latestUpdateManifest) == NSComparisonResult.OrderedDescending || latestUpdate == "0000-00-00 00:00:00") {
					
					latestUpdateManifest = addedAt!
					latestUpdate = lma
				}
			}
			
			//dispatch_async(realmBGQueue, {
			//dispatch_async(dispatch_get_main_queue(), {
				
				let r = try! Realm()
				
				let game = Game(obj: gameObj)
				
				try! r.write({
					
					r.add(game, update: true)
				})
				
			//	dispatch_async(dispatch_get_main_queue(), {
					
					g += 1
					iterate?()
			//	})
			//})
			
		}
	}
	
	finishDrop = {
		
		let r = try! Realm()
		
		myGames = r.objects(Game.self).filter({ g in if g.state != "" && g.addedat != nil { return true }; return false })
		
		gamesVC?.reloadContent()
		countdownVC?.reloadContent()
		profileVC?.reloadContent()
	}
	
	iterate?()
}

class Media: Object {
	
	dynamic var width : Float = 0
	dynamic var height : Float = 0
	dynamic var src = ""
	dynamic var game : Int = 0
	dynamic var data : NSData? = nil
	dynamic var igdbid = ""
	dynamic var type = ""
	dynamic var index : Int = 0
	
	override static func primaryKey() -> String? {
		return "src"
	}
}

let realmBGQueue = dispatch_queue_create("realmQueue", DISPATCH_QUEUE_SERIAL)

func getMediaBySrc(src: String, game: Int, completion: (img: UIImage)->()) {
	
	dispatch_async(realmBGQueue, {
		
		let r = try! Realm()
		
		let o = r.objects(Media.self).filter("src = \'\(src)\'")
		
		if let d = o.first?.data {
			
			let i = UIImage(data: d)
			
			if i != nil {
				
				UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
				let context = UIGraphicsGetCurrentContext();
				CGContextDrawImage(context!, CGRect(origin: CGPointZero, size: CGSize(width: 1, height: 1)), i!.CGImage!)
				UIGraphicsEndImageContext()
				
				dispatch_sync(dispatch_get_main_queue(), {
					
					completion(img: i!)
				})
			}
		}
		else {
			
			dispatch_async(dispatch_get_main_queue(), {
				
				let request = NSMutableURLRequest(URL: NSURL(string: src)!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 15)
				request.HTTPMethod = "GET"
				
				//let tempIndexPath = cell.indexPath!
				//let tempCellCount = self.collectionView?.numberOfItemsInSection(0)
				
				let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
					
					UIApplication.sharedApplication().networkActivityIndicatorVisible = false
					help.APPDELEGATE.activeMediaDownloads.removeValueForKey(src)
					
					if response != nil {
						
						let tempResponse = response as! NSHTTPURLResponse?
						
						if tempResponse?.statusCode == 200 && data != nil {
							
							let img = UIImage(data: data!)
							
							if img != nil {
								
								dispatch_async(dispatch_get_main_queue(), {
									
									completion(img: img!)
									
									dispatch_async(realmBGQueue, {
										
										if let r2 = try? Realm() {
											
											let mediaObj = Media()
											mediaObj.data = data
											mediaObj.height = Float(img!.size.height)//Float(media.valueForKey("height") as! String)!
											mediaObj.width = Float(img!.size.width)//Float(media.valueForKey("width") as! String)!
											//mediaObj.game = message.id
											mediaObj.src = src
											mediaObj.type = "cover"
											mediaObj.game = game
											
											r2.refresh()
											
											_ = try? r2.write({
												
												r2.add(mediaObj, update: true)
											})
										}
									})
								})
							}
							else {
								
							}
						}
					}
					else {
						if error != nil {
							print(error)
						}
					}
				})
				
				help.APPDELEGATE.activeMediaDownloads[src] = task
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = true
				task.resume()
			})
		}
	})
}
