//
//  GamesViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class GamesViewController: GenericViewController, UICollectionViewDataSource {
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
	}
	
	var collectionView : UICollectionView!
	
	var filterBar : UIView!
	var backlog : UIButton!
	var played : UIButton!
	var wishlist : UIButton!
	
	var myGamesBacklog : [Game] {
		
		return myGames.filter({ g in if (g.state == "backlog") { return true }; return false  })
	}
	var myGamesPlayed : [Game] {
		
		return myGames.filter({ g in if (g.state == "played") { return true }; return false  })
	}
	var myGamesWishList : [Game] {
		
		return myGames.filter({ g in if (g.state == "wishlist") { return true }; return false  })
	}
	
	var myGamesFiltered : [Game] {
		
		var mgf : [Game] = []
		
		if self.showMode == 0 {
			mgf = myGamesBacklog
		}
		else if self.showMode == 1 {
			mgf = myGamesPlayed
		}
		else {
			mgf = myGamesWishList
		}
		
		mgf = mgf.sort { (g1, g2) -> Bool in
			
			if g1.addedat?.compare(g2.addedat!) == .OrderedDescending && self.sortMode == 0 {
    
				return true
			}
			else if g1.releasedate != nil && g2.releasedate != nil && self.sortMode == 1 {
				
				if g1.releasedate?.compare(g2.releasedate!) == .OrderedDescending {
					
					return true
				}
				return false
			}
			return false
		}
		
		return mgf
	}
	
	var filter : Filter?
	var sortMode = 0
	var showMode = 0
	var gridMode = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor(white: 0.1, alpha: 1)//.whiteColor()
		
		self.topBarLabel.text = "My Games"
		
		self.topBarLeftButton.setImage(UIImage(named: "search"), forState: .Normal)
		self.topBarLeftButton.setImage(UIImage(named: "search")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarLeftButton.addTarget(self, action: #selector(openSearch), forControlEvents: .TouchDown)
		
		self.topBarRightButton.setImage(UIImage(named: "filter"), forState: .Normal)
		self.topBarRightButton.setImage(UIImage(named: "filter")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarRightButton.addTarget(self, action: #selector(openFilter), forControlEvents: .TouchDown)
		
		self.setupCollectionView()
		self.setupFilterBar()
		
	}
	func openSearch() {
		
		let search = Search(owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.showModal(search)
		search.field.becomeFirstResponder()
	}
	
	func openFilter() {
		
		self.filter = Filter(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.filter?.mode = self.sortMode
		self.filter?.mode2 = self.gridMode
		self.filter?.switchTo = { m, m2 in self.sortMode = m; self.gridMode = m2; self.reloadContent() }
		help.APPDELEGATE.tabBarController.view.addSubview(self.filter!)
		self.filter?.open()
	}
	
	func reloadContent() {
		
		if self.myGamesBacklog.count > 0 {
			
			self.switchMode(self.backlog)
		}
		else if self.myGamesPlayed.count > 0 {
			
			self.switchMode(self.played)
		}
		else if self.myGamesWishList.count > 0 {
			
			self.switchMode(self.wishlist)
		}
		else {
			
			//showNotificationBar("You haven't added any games yet", type: .Blue)
		}
		
		self.collectionView.reloadData()
	}
	
	func setupFilterBar() {
		
		self.filterBar = UIView(frame: CGRect(x: 0, y: 64, width: self.view.width, height: 50))
		self.filterBar.backgroundColor = UIColor(white: 0.15, alpha: 1)//UIColor(white: 0.98, alpha: 0.98)
		self.view.addSubview(self.filterBar)
		
		let atts = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(13, weight: UIFontWeightBold), NSKernAttributeName:0.5]
		
		self.backlog = UIButton(frame: CGRect(x: 14, y: 8, width: self.view.width/3-28, height: 34))
		self.backlog.tag = 0
		self.backlog.addTarget(self, action: #selector(switchMode(_:)), forControlEvents: .TouchDown)
		self.backlog.setAttributedTitle(NSAttributedString(string: "BACKLOG", attributes: atts), forState: .Normal)
		self.backlog.layer.cornerRadius = 8
		self.backlog.backgroundColor = help.yellow
		self.filterBar.addSubview(self.backlog)
		
		self.played = UIButton(frame: CGRect(x: self.view.width/3+14, y: 8, width: self.view.width/3-28, height: 34))
		self.played.tag = 1
		self.played.addTarget(self, action: #selector(switchMode(_:)), forControlEvents: .TouchDown)
		self.played.setAttributedTitle(NSAttributedString(string: "PLAYED", attributes: atts), forState: .Normal)
		self.played.layer.cornerRadius = 8
		self.filterBar.addSubview(self.played)
		
		self.wishlist = UIButton(frame: CGRect(x: (self.view.width/3)*2+14, y: 8, width: self.view.width/3-28, height: 34))
		self.wishlist.tag = 2
		self.wishlist.addTarget(self, action: #selector(switchMode(_:)), forControlEvents: .TouchDown)
		self.wishlist.setAttributedTitle(NSAttributedString(string: "WISH LIST", attributes: atts), forState: .Normal)
		self.wishlist.layer.cornerRadius = 8
		self.filterBar.addSubview(self.wishlist)
	}
	
	
	override func scrollViewDidScroll(scrollView: UIScrollView) {
		super.scrollViewDidScroll(scrollView)
		
		UIView.animateWithDuration(0.1, delay: 0, options: .CurveEaseInOut, animations: {
			self.filterBar?.y = self.topBar.y + 64
			}, completion: nil)
	}
	
	func switchMode(button: UIButton) {
		
		let tempMode = self.showMode
		self.showMode = button.tag
		
		self.backlog.backgroundColor = .clearColor()
		self.played.backgroundColor = .clearColor()
		self.wishlist.backgroundColor = .clearColor()
		button.backgroundColor = help.yellow.darkerByPercentage(5)
		
		self.collectionView.reloadData()
		
		if tempMode == button.tag {
			
			self.collectionView.setContentOffset(CGPoint(x: 0, y: -64), animated: true)
		}
		else {
			self.collectionView.setContentOffset(CGPoint(x: 0, y: -64), animated: false)
		}
	}
	
	func refreshModes() {
		
		
	}
	
	//collection view
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.scrollDirection = .Vertical
		layout.itemSize = CGSize(width: self.view.width, height: 1)
		self.collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.view.frame.size), collectionViewLayout: layout)
		self.collectionView?.dataSource = self
		self.collectionView?.delegate = self
		self.collectionView?.registerClass(GameCell.self, forCellWithReuseIdentifier: "GameCell")
		self.collectionView?.backgroundColor = .clearColor()
		self.collectionView?.alpha = 1
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64+50, left: 0, bottom: 50, right: 0)
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.alwaysBounceVertical = true
		self.collectionView?.scrollEnabled = true
		self.collectionView?.clipsToBounds = false
		self.collectionView?.tag = 0
		
		self.view.insertSubview(self.collectionView!, belowSubview: self.topBar)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		
		return CGFloat(13)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		if self.myGamesFiltered.count == 0 { return UIEdgeInsetsZero }
		return UIEdgeInsets(top: 15+50, left: 14, bottom: 40, right: 14)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		
		return 1
		
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if self.myGamesFiltered.count == 0 { return 1 }
		return self.myGamesFiltered.count
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if self.myGamesFiltered.count == 0 {
			
			return CGSize(width: self.view.width, height: self.view.height-114)
		}
		if self.gridMode == 1 {
			
			return CGSize(width: self.view.width-20, height: 100)
		}
		return CGSize(width: (self.view.width/2)-20, height: (self.view.width/2)+70)
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
		
		cell.layer.shouldRasterize = true
		cell.layer.rasterizationScale = UIScreen.mainScreen().scale
		
		cell.indexPath = indexPath
		cell.label.alpha = 0
		cell.textView.alpha = 0
		cell.imageView.image = nil
		cell.imageView.alpha = 0
		cell.imageView.contentMode = .ScaleAspectFill
		let r = Int(arc4random_uniform(1000))
		cell.imageView.tag = r
		
		cell.backgroundColor = .clearColor()
		cell.type = .Grid
		if self.gridMode == 1 { cell.type = .List }
		
		if self.myGamesFiltered.count == 0 {
			
			cell.type = .Custom
			cell.imageView.alpha = 1
			cell.imageView.y = 0
			cell.imageView.width = self.view.width
			cell.imageView.height = self.view.height-124
			cell.imageView.contentMode = .Center
			cell.imageView.image = UIImage(named: "myGamesBlank")
			
			cell.refresh()
		}
		else if let game = self.myGamesFiltered[safe: indexPath.item] {
			
			cell.textView.text = game.name
			cell.game = game
			
			if game.coverurl.length > 0 {
				
				let tempIP = cell.indexPath
				let tempCount = collectionView.numberOfItemsInSection(indexPath.section)
				
				getMediaBySrc(game.coverurl, game: game.id, completion: { img in
					
					if tempIP == cell.indexPath && r == cell.imageView.tag {
						
						cell.imageView.image = img
						UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
							cell.imageView.alpha = 1
							}, completion: nil)
					}
					else if tempCount == self.collectionView.numberOfItemsInSection(tempIP!.section) && tempIP != cell.indexPath {
						
						self.collectionView.reloadItemsAtIndexPaths([tempIP!, cell.indexPath!])
					}
					
				})
			}
			else {
				
				//cell.imageView.image = gradientImage(help.purple1, toColor: help.babyPurple)
				//cell.imageView.alpha = 1
			}
		}
		
		cell.refresh()
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if let g = self.myGamesFiltered[safe: indexPath.item] {
			
			let v = GameScreen(game: g, owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
			
			self.showModal(v)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
