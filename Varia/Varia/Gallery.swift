//
//  MediaView.swift
//
//
//  Created by Hazim Judi on 2016-04-26.
//
//

import UIKit
import RealmSwift

class Gallery: UIView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
	
	var collectionView:UICollectionView?
	
	var closeButton:UIButton?
	
	var productID:Int?
	var media: [Media] = []
	var images: [UIImage] = []
	
	var indicator : UILabel!
	
	var dotIndicator : UIPageControl?
	
	var game : Int!
	
	init(game: Int, frame: CGRect) {
		super.init(frame: frame)
		
		self.game = game
		self.backgroundColor = UIColor.blackColor()//(white: 0.95, alpha: 1)
		
		self.closeButton = UIButton(frame: CGRectMake(0, 0, 66, 66))
		self.closeButton?.setImage(UIImage(named: "close")?.fillWithColor(UIColor.whiteColor()), forState: .Normal)
		self.closeButton?.addTarget(self, action: #selector(dismiss), forControlEvents: .TouchUpInside)
		self.addSubview(self.closeButton!)
		
		self.setupCollectionView()
		
		
		let r = try! Realm()
		
		self.media = r.objects(Media.self).filter("type = \'screenshot\' && game = \(self.game)").sorted("index", ascending: true).reverse().reverse()
		
		self.collectionView?.reloadData()
		
		self.dotIndicator = UIPageControl(frame: CGRectMake(0, self.frame.height-30, self.frame.width, 20))
		self.dotIndicator?.numberOfPages = self.media.count
		self.dotIndicator?.currentPage = 0
		self.dotIndicator?.currentPageIndicatorTintColor = UIColor(white: 0.3, alpha: 0.8)
		self.dotIndicator?.pageIndicatorTintColor = UIColor(white: 0.4, alpha: 0.4)
		//self.addSubview(self.dotIndicator!)
		
		self.indicator = UILabel(frame: CGRect(x: 0, y: 0, width: self.width, height: 66))
		self.indicator.textAlignment = .Center
		self.indicator.font = UIFont.systemFontOfSize(24, weight: UIFontWeightRegular)
		self.indicator.textColor = .whiteColor()
		self.addSubview(self.indicator)
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
	}
	
	func goToImage(i: Int) {
		
		if self.collectionView?.numberOfItemsInSection(0) < i+1 {
			
			return
		}
		
		self.collectionView?.scrollToItemAtIndexPath(NSIndexPath(forItem: i, inSection: 0), atScrollPosition: .Left, animated: false)
		self.p = i+1
		self.refreshIndicator()
	}
	
	var p = 0
	
	func refreshIndicator() {
		
		self.indicator.text = "\(self.p) / \(self.media.count)"
	}
	
	func rotated()
	{
		
		let p = CGFloat(self.media.count)/(self.collectionView!.contentSize.width/self.collectionView!.contentOffset.x)
		self.p = Int(p)+1
		self.collectionView?.collectionViewLayout.invalidateLayout()
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			if UIDevice.currentDevice().orientation == .LandscapeLeft {
				
				self.collectionView?.transform = CGAffineTransformMakeRotation(0.5*CGFloat(M_PI))
				self.closeButton?.transform = CGAffineTransformMakeRotation(0.5*CGFloat(M_PI))
				self.closeButton?.x = self.width-66
				self.closeButton?.y = 0
				
				self.indicator.alpha = 0
				//self.indicator?.transform = CGAffineTransformMakeRotation(0.5*CGFloat(M_PI))
				//self.indicator?.x = 0
				//self.indicator?.y = 0
				//self.indicator?.width = self.height
			}
			else if UIDevice.currentDevice().orientation == .LandscapeRight {
				
				self.collectionView?.transform = CGAffineTransformMakeRotation(-0.5*CGFloat(M_PI))
				self.closeButton?.transform = CGAffineTransformMakeRotation(-0.5*CGFloat(M_PI))
				self.closeButton?.x = 0
				self.closeButton?.y = self.height-66
				
				self.indicator.alpha = 0
				//self.indicator?.transform = CGAffineTransformMakeRotation(-0.5*CGFloat(M_PI))
				//self.indicator?.x = 0
				//self.indicator?.y = self.width-66
				//self.indicator?.width = self.height
			}
			else if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
				
			}
			
			if (UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation)) {
				
				self.collectionView?.transform = CGAffineTransformMakeRotation(0)
				self.closeButton?.transform = CGAffineTransformMakeRotation(0)
				self.closeButton?.x = 0
				self.closeButton?.y = 0
				
				self.indicator.alpha = 1
				//self.indicator?.transform = CGAffineTransformMakeRotation(0)
				//self.indicator?.x = 0
				//self.indicator?.y = 0
				//self.indicator?.width = self.width
			}
			
			self.collectionView?.width = self.width
			self.collectionView?.height = self.height
			self.collectionView?.x = 0
			self.collectionView?.y = 0
			self.collectionView?.backgroundColor = .blackColor()
			
			
			}, completion: nil)
		
		self.collectionView?.reloadData()
		self.collectionView?.setContentOffset(CGPoint(x: p*self.collectionView!.bounds.width, y: 0), animated: false)
	}
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
		layout.minimumLineSpacing = 0
		layout.scrollDirection = .Horizontal
		self.collectionView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
		self.collectionView!.dataSource = self
		self.collectionView!.delegate = self
		self.collectionView!.registerClass(MediaCell.self, forCellWithReuseIdentifier: "Cell")
		self.collectionView!.backgroundColor = UIColor.clearColor()
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.pagingEnabled = true
		self.collectionView?.tag = 0
		
		self.insertSubview(self.collectionView!, belowSubview: self.closeButton!)
		
	}
	
	func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
		self.p = Int(scrollView.contentOffset.x/self.collectionView!.bounds.width)+1
		self.refreshIndicator()
		//self.dotIndicator?.currentPage = Int(scrollView.contentOffset.x/self.frame.width)
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		return self.media.count
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if UIDevice.currentDevice().orientation == .LandscapeLeft || UIDevice.currentDevice().orientation == .LandscapeRight {
			
			return self.collectionView!.bounds.size//CGSize(width: self.width, height: self.height)
		}
		return self.collectionView!.bounds.size
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell : MediaCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! MediaCell
		cell.backgroundColor = UIColor.clearColor()
		cell.imageView?.alpha = 0
		
		let m = self.media[indexPath.item]
		
		if let d = m.data {
			
			let img = UIImage(data: d)
			cell.imageView?.image = img
			
			let scaleWidth = self.collectionView!.bounds.width / (img!.size.width/2)
			let scaleHeight = self.collectionView!.bounds.height / (img!.size.height/2)
			
			cell.imageView?.frame.size.width = (img!.size.width/2)
			cell.imageView?.frame.size.height = (img!.size.height/2)
			
			let finalScale = min(scaleWidth, scaleHeight)
			cell.scrollView?.frame.size = self.collectionView!.bounds.size
			cell.scrollView?.contentSize = cell.imageView!.frame.size
			cell.scrollView?.minimumZoomScale = finalScale
			cell.scrollView?.maximumZoomScale = 1
			cell.scrollView?.zoomScale = finalScale
			cell.scrollView?.multipleTouchEnabled = true
			
			cell.imageView?.frame = CGRectMake(0, self.collectionView!.bounds.height/2-((img!.size.height/2)*scaleWidth)/2, self.collectionView!.bounds.width, (img!.size.height/2)*scaleWidth)
			cell.scrollView?.setZoomScale(finalScale, animated: false)
			
			cell.imageView?.alpha = 1
		}
		else if let link = NSURL(string: m.src) {
			
			//let tempIndex = indexPath
			
			let request = NSMutableURLRequest(URL: link, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 10)
			request.HTTPMethod = "GET"
			
			let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
				
				if response != nil {
					
					let tempResponse = response as! NSHTTPURLResponse?
					
					if tempResponse?.statusCode == 200 {
						
						dispatch_async(dispatch_get_main_queue(), {
							
							m.data = data
							
							self.collectionView?.reloadData()
						})
					}
				}
				else {
					if error != nil {
						print(error)
					}
				}
			})
			
			UIApplication.sharedApplication().networkActivityIndicatorVisible = true
			task.resume()
		}
		
		
		
		return cell
	}
	
	
	class MediaCell: UICollectionViewCell, UIScrollViewDelegate {
		
		var scrollView:UIScrollView?
		var imageView:UIImageView?
		
		override init(frame: CGRect) {
			super.init(frame: frame)
			
			self.scrollView = UIScrollView(frame: CGRectMake(0, 0, self.frame.width, self.frame.height))
			self.scrollView?.backgroundColor = UIColor.clearColor()
			self.scrollView?.delegate = self
			//let rec = UITapGestureRecognizer(target: self, action: "refresh")
			//rec.numberOfTapsRequired = 2
			//rec.numberOfTouchesRequired = 1
			//self.scrollView?.addGestureRecognizer(rec)
			self.contentView.addSubview(self.scrollView!)
			
			self.imageView = UIImageView(frame: CGRectMake(0, 0, self.frame.width, self.frame.height))
			self.imageView?.backgroundColor = UIColor(white: 1, alpha: 0.1)
			self.scrollView?.addSubview(self.imageView!)
		}
		
		func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
			//  println(self.scrollView?.subviews.first)
			return self.scrollView?.subviews.first
		}
		
		func scrollViewDidZoom(scrollView: UIScrollView) {
			let imgView = scrollView.subviews.first as! UIImageView
			imgView.y = self.height/2-imgView.height/2
			imgView.x = self.width/2-imgView.width/2
		}
		
		func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
			
			//  println(scrollView.zoomScale)
		}
		
		required init?(coder aDecoder: NSCoder) {
			super.init(coder: aDecoder)
		}
	}
	
	func dismiss() {
		
		help.APPDELEGATE.tabBarController.statusBarHidden = false
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.15)
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.alpha = 0

			}, completion: { finished in
				
				self.removeFromSuperview()
		})
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
