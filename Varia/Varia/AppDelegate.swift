//
//  AppDelegate.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

var profileVC : ProfileViewController? { return help.APPDELEGATE.tabBarController.profileViewController }
var exploreVC : ExploreViewController? { return help.APPDELEGATE.tabBarController.exploreViewController }
var gamesVC : GamesViewController? { return help.APPDELEGATE.tabBarController.gamesViewController }
var countdownVC : CountdownViewController? { return help.APPDELEGATE.tabBarController.countdownViewController }

func cancelAllDownloads() {
	
	for t in help.APPDELEGATE.activeMediaDownloads {
		
		t.1.cancel()
	}
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	var tabBarController = TabBarController(nibName: nil, bundle: nil)
	
	var activeMediaDownloads: Dictionary<String,NSURLSessionDataTask> = [:]
	
	let dateFormatter = NSDateFormatter()
	
	var APNSRegistered = false

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		
		self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
		self.window?.backgroundColor = UIColor(white: 0.1, alpha: 1)
		self.window?.rootViewController = tabBarController
		self.window?.makeKeyAndVisible()
		
		self.dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
		self.dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
		//self.dateFormatter.timeZone = NSTimeZone(name: "UTC")
		
		Localytics.integrate("5e5a2881d0cd3bc8d92ae44-22279a62-10b2-11e6-b07e-00342b7f5075")
		
		if (application.applicationState != UIApplicationState.Background) {
			Localytics.openSession()
		}
		
		return true
	}
	
	var fullscreen = false
	
	func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
		
		if fullscreen == false { return .Portrait }
		
		return UIInterfaceOrientationMask.AllButUpsideDown
	}
	
	// Notifications
	
	func evaluateIfShouldRegisterForNotifications() {
		
		if User.getCredentials() != nil && self.APNSRegistered == false && User.notifications == true {
			
			let pushSettings: UIUserNotificationSettings =
				UIUserNotificationSettings(forTypes:
					[UIUserNotificationType.Alert, UIUserNotificationType.Badge],
				                           categories: nil)
			
			UIApplication.sharedApplication().registerUserNotificationSettings(pushSettings)
			UIApplication.sharedApplication().registerForRemoteNotifications()
		}
	}
	
	
	func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
		print(error)
	}
	
	func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
		
		Localytics.setPushToken(deviceToken)
		
		print(application.currentUserNotificationSettings()?.types)
		if application.currentUserNotificationSettings()?.types != nil && User.getCredentials() != nil {
			
			let plainToken = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>")).stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)
			
			if let creds = User.getCredentials() {
				
				print("New token: \(plainToken)")
				socket.emit("saveAPNSToken", [creds, ["pushtoken" : plainToken]])
			}
			
		}
	}
	
	func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
		print(userInfo)
	}

	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}

	func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
		
		return Localytics.handleTestModeURL(url)
	}
	
	func applicationDidEnterBackground(application: UIApplication) {
		
		Localytics.dismissCurrentInAppMessage()
		Localytics.closeSession()
		Localytics.upload()
	}

	func applicationWillEnterForeground(application: UIApplication) {
		
		Localytics.openSession();
		Localytics.upload();
	}

	func applicationDidBecomeActive(application: UIApplication) {
		
		Localytics.openSession();
		Localytics.upload();
	}

	func applicationWillTerminate(application: UIApplication) {
		
	}


}

