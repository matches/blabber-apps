//
//  ProfileViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ProfileViewController: GenericViewController, UICollectionViewDataSource, UITextFieldDelegate, MFMailComposeViewControllerDelegate {
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
		
	}
	
	var profileBG : UIView!
	var avatar : UIImageView!
	var name : UILabel!
	var location : UIButton!
	var finished : UILabel!
	var backlog : UILabel!
	var orb : UIView!
	var finishedShape : CAShapeLayer!
	var backlogShape : CAShapeLayer!
	var noGames : UILabel!
	var line1 : UIView!
	var push : UILabel!
	var pushSwitch : UISwitch!
	
	var collectionView: UICollectionView!
	
	var keyboardHeight = CGFloat(0)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor(white: 0.15, alpha: 1)//help.babyPurple
		
		self.topBarLabel.text = "Profile"
		
		self.topBarLeftButton.setImage(UIImage(named: "search"), forState: .Normal)
		self.topBarLeftButton.setImage(UIImage(named: "search")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarLeftButton.addTarget(self, action: #selector(openSearch), forControlEvents: .TouchDown)
		
		self.topBarRightButton.setImage(UIImage(named: "settings"), forState: .Normal)
		self.topBarRightButton.setImage(UIImage(named: "settings")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarRightButton.addTarget(self, action: #selector(openSettings), forControlEvents: .TouchDown)
		
		self.setupCollectionView()
		
		self.profileBG = UIView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: 380))
		self.profileBG.backgroundColor = UIColor(white: 0.3, alpha: 0.8)
		self.collectionView.addSubview(self.profileBG)
		
		self.avatar = UIImageView(frame: CGRect(x: self.view.width/2-50, y: 30, width: 100, height: 100))
		self.avatar.backgroundColor = help.babyPurple
		self.avatar.contentMode = .ScaleAspectFill
		self.avatar.layer.cornerRadius = 50
		self.avatar.layer.masksToBounds = true
		self.profileBG.addSubview(self.avatar)
		
		self.name = UILabel(frame: CGRect(x: 0, y: self.avatar.y+self.avatar.height+16, width: self.view.width, height: 25))
		self.name.textAlignment = .Center
		self.profileBG.addSubview(self.name)
		
		self.location = UIButton(frame: CGRect(x: 0, y: self.name.y+self.name.height+2, width: self.view.width, height: 25))
		self.location.imageEdgeInsets.right = 6
		self.location.setImage(UIImage(named: "pin")?.fillWithColor(UIColor(white: 1, alpha: 0.6)), forState: .Normal)
		self.profileBG.addSubview(self.location)
		
		//
		
		self.finished = UILabel(frame: CGRect(x: 0, y: self.location.y+self.location.height+35, width: (self.profileBG.width/2)-40, height: 80))
		self.finished.textAlignment = .Center
		self.finished.numberOfLines = 0
		self.profileBG.addSubview(self.finished)
		
		self.backlog = UILabel(frame: CGRect(x: (self.profileBG.width/2)+40, y: self.location.y+self.location.height+35, width: (self.profileBG.width/2)-40, height: 80))
		self.backlog.textAlignment = .Center
		self.backlog.numberOfLines = 0
		self.profileBG.addSubview(self.backlog)
		
		
		self.orb = UIView(frame: CGRect(x: (self.view.width/2)-38, y: self.location.y+self.location.height+35, width: 80, height: 80))
		self.profileBG.addSubview(self.orb)
		
		let path = UIBezierPath(ovalInRect: CGRect(x: 5, y: 5, width: self.orb.width-10, height: self.orb.height-10))
		let path2 = UIBezierPath(ovalInRect: CGRect(x: 5, y: 5, width: self.orb.width-10, height: self.orb.height-10))
		
		self.backlogShape = CAShapeLayer()
		self.backlogShape.frame = CGRect(origin: CGPointZero, size: self.orb.frame.size)
		self.backlogShape.strokeColor = help.babyBlue.CGColor
		self.backlogShape.strokeStart = 0.02
		self.backlogShape.lineWidth = 6
		self.backlogShape.fillColor = UIColor.clearColor().CGColor
		self.backlogShape.path = path2.CGPath
		self.orb.layer.addSublayer(self.backlogShape)
		
		self.finishedShape = CAShapeLayer()
		self.finishedShape.frame = CGRect(origin: CGPointZero, size: self.orb.frame.size)
		self.finishedShape.strokeColor = help.yellow.CGColor
		self.finishedShape.lineWidth = 3
		self.finishedShape.strokeStart = 0.02
		self.finishedShape.fillColor = UIColor.clearColor().CGColor
		self.finishedShape.path = path.CGPath
		self.orb.layer.addSublayer(self.finishedShape)
		
		self.noGames = UILabel(frame: CGRect(x: 0, y: self.location.y+self.location.height+35, width: self.view.width, height: 60))
		let attStr = NSMutableAttributedString(string: "No stats...\n", attributes: [NSForegroundColorAttributeName:help.babyPurple.darkerByPercentage(40), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)])
		attStr.appendAttributedString(NSAttributedString(string: "You haven't added any games", attributes: [NSForegroundColorAttributeName:help.babyPurple.darkerByPercentage(40), NSFontAttributeName:UIFont.systemFontOfSize(15, weight: UIFontWeightRegular)]))
		self.noGames.attributedText = attStr
		self.noGames.textAlignment = .Center
		self.noGames.numberOfLines = 0
		self.profileBG.addSubview(self.noGames)
		
		//
		
		self.line1 = UIView(frame: CGRect(x: 12, y: self.profileBG.height-50, width: self.profileBG.width-24, height: 1))
		self.line1.backgroundColor = UIColor(white: 1, alpha: 0.08)
		self.profileBG.addSubview(self.line1)
		
		self.push = UILabel(frame: CGRect(x: 14, y: self.line1.y, width: 200, height: 50))
		self.push.attributedText = NSAttributedString(string: "Push notifications".uppercaseString, attributes: [NSForegroundColorAttributeName:help.babyPurple.darkerByPercentage(20), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold),NSKernAttributeName:0.5])
		self.profileBG.addSubview(self.push)
		
		self.pushSwitch = UISwitch(frame: CGRect(x: self.profileBG.width-62, y: self.line1.y+10, width: 60, height: 38))
		self.pushSwitch.onTintColor = help.babyBlue
		self.profileBG.addSubview(self.pushSwitch)
		self.pushSwitch.addTarget(self, action: #selector(alertsChanged(_:)), forControlEvents: .ValueChanged)
		
		self.reloadContent()
	}
	
	func openSearch() {
		
		let search = Search(owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.showModal(search)
		search.field.becomeFirstResponder()
	}
	
	func alertsChanged(s: UISwitch) {
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("editUser", [creds, ["notifications" : s.on.description]])(timeoutAfter: 5, callback: { data in
				
				if let user = data[safe: 0]?["user"] as? NSDictionary {
					
					User.setUserData(user)
					profileVC?.reloadContent()
					help.APPDELEGATE.evaluateIfShouldRegisterForNotifications()
				}
			})
		}
	}
	
	var settings : UIVisualEffectView!
	var avatarPreview : UIImageView!
	var avatarEdit : UIButton!
	var avatarDelete : UIButton!
	var nameField : UITextField!
	var cityField : UITextField!
	
	func openSettings() {
		
		self.settings = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.settings.effect = UIBlurEffect(style: .ExtraLight)
		self.settings.backgroundColor = UIColor(white: 1, alpha: 0.25)
		self.settings.alpha = 0
		help.APPDELEGATE.window?.addSubview(self.settings)
		
		let l = UILabel(frame: CGRect(x: 0, y: 20, width: self.view.width, height: 50))
		l.attributedText = NSAttributedString(string: "Profile settings", attributes: [NSForegroundColorAttributeName: UIColor(white: 0, alpha: 0.8), NSFontAttributeName: UIFont.systemFontOfSize(20, weight: UIFontWeightBold)])
		l.textAlignment = .Center
		self.settings.addSubview(l)
		
		let c = UIButton(frame: CGRect(x: 0, y: 20, width: 60, height: 50))
		c.setImage(UIImage(named: "close")?.fillWithColor(UIColor(white: 0, alpha: 0.4)), forState: .Normal)
		c.addTarget(self, action: #selector(closeSettings), forControlEvents: .TouchDown)
		self.settings.addSubview(c)
		
		let s = UIButton(frame: CGRect(x: self.view.width-90, y: 20, width: 90, height: 50))
		s.setAttributedTitle(NSAttributedString(string: "Save", attributes: [NSForegroundColorAttributeName: help.babyBlue.darkerByPercentage(10), NSFontAttributeName:UIFont.systemFontOfSize(20, weight: UIFontWeightBold)]), forState: .Normal)
		s.addTarget(self, action: #selector(saveSettings), forControlEvents: .TouchDown)
		self.settings.addSubview(s)
		
		//
		
		self.avatarPreview = UIImageView(frame: CGRect(x: self.view.width/2-40, y: self.view.height*0.15, width: 80, height: 80))
		self.avatarPreview.contentMode = .ScaleAspectFill
		self.avatarPreview.layer.masksToBounds = true
		self.avatarPreview.layer.cornerRadius = 40
		
		self.settings.addSubview(self.avatarPreview)
		
		
		let r1 = try? Realm()
		
		let o = r1?.objects(Media.self).filter("type = \'avatar\'")
		
		if let d = o?.first?.data {
			
			self.avatarPreview.image = UIImage(data: d)
		}
		else {
			
			self.avatarPreview.image = UIImage(named: "profileBlank")
		}
		
		
		self.avatarEdit = UIButton(frame: CGRect(x: self.view.width/2-60-(55), y: self.view.height*0.32, width: 120, height: 44))
		self.avatarEdit.backgroundColor = help.yellow
		self.avatarEdit.setBackgroundImage(help.yellow.darkerByPercentage(10).toImage(), forState: .Highlighted)
		self.avatarEdit.setAttributedTitle(NSAttributedString(string: "Edit avatar", attributes: [NSForegroundColorAttributeName:UIColor.blackColor(), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightBold)]), forState: .Normal)
		self.avatarEdit.addTarget(self, action: #selector(openMediaPicker), forControlEvents: .TouchUpInside)
		self.avatarEdit.layer.cornerRadius = 8
		self.avatarEdit.layer.masksToBounds = true
		self.settings.addSubview(self.avatarEdit)
		
		self.avatarDelete = UIButton(frame: CGRect(x: self.view.width/2+(60-45), y: self.view.height*0.32, width: 100, height: 44))
		self.avatarDelete.backgroundColor = hexToColor("ef6060")
		self.avatarDelete.setBackgroundImage( hexToColor("ef6060")?.darkerByPercentage(10).toImage(), forState: .Highlighted)
		self.avatarDelete.layer.masksToBounds = true
		self.avatarDelete.layer.cornerRadius = 8
		self.avatarDelete.setAttributedTitle(NSAttributedString(string: "Delete", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightBold)]), forState: .Normal)
		self.avatarDelete.addTarget(self, action: #selector(deleteMedia), forControlEvents: .TouchUpInside)
		self.settings.addSubview(self.avatarDelete)
		
		
		self.nameField = UITextField(frame: CGRect(x: 25, y: self.view.height*0.45, width: self.view.width-50, height: 50))
		self.nameField.tag = 0
		self.nameField.delegate = self
		self.nameField.clipsToBounds = false
		self.nameField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSForegroundColorAttributeName: UIColor(white: 0, alpha: 0.4), NSFontAttributeName: UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)])
		self.nameField.font = UIFont.systemFontOfSize(20, weight: UIFontWeightBold)
		self.nameField.textColor = UIColor.blackColor()
		self.settings.addSubview(self.nameField)
		let l1 = UIView(frame: CGRect(x: 0, y: self.nameField.height, width: self.nameField.width, height: 2))
		l1.backgroundColor = UIColor(white: 0, alpha: 0.07)
		self.nameField.addSubview(l1)
		self.nameField.text = User.userName
		
		self.cityField = UITextField(frame: CGRect(x: 25, y: self.nameField.y+self.nameField.height+20, width: self.view.width-50, height: 50))
		self.cityField.tag = 1
		self.cityField.delegate = self
		self.cityField.clipsToBounds = false
		self.cityField.attributedPlaceholder = NSAttributedString(string: "Location", attributes: [NSForegroundColorAttributeName: UIColor(white: 0, alpha: 0.4), NSFontAttributeName: UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)])
		self.cityField.font = UIFont.systemFontOfSize(20, weight: UIFontWeightBold)
		self.cityField.textColor = UIColor.blackColor()
		self.settings.addSubview(self.cityField)
		let l2 = UIView(frame: CGRect(x: 0, y: self.cityField.height, width: self.cityField.width, height: 2))
		l2.backgroundColor = UIColor(white: 0, alpha: 0.07)
		self.cityField.addSubview(l2)
		self.cityField.text = User.city
		
		let rb = UIButton(frame: CGRect(x: 25, y: self.view.height-70, width: self.view.width/2-25-20-36, height: 44))
		rb.layer.cornerRadius = 8
		rb.layer.masksToBounds = true
		rb.backgroundColor = help.babyBlue
		rb.setAttributedTitle(NSAttributedString(string: "Contact us", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(15, weight: UIFontWeightBold)]), forState: .Normal)
		rb.addTarget(self, action: #selector(openMail), forControlEvents: .TouchUpInside)
		self.settings.addSubview(rb)
		
		let rb2 = UIButton(frame: CGRect(x: (self.view.width/2)-45, y: self.view.height-70, width: (self.view.width/2)+20, height: 44))
		rb2.layer.cornerRadius = 8
		rb2.layer.masksToBounds = true
		rb2.backgroundColor = help.babyBlue
		rb2.setAttributedTitle(NSAttributedString(string: "Report bad game data", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(15, weight: UIFontWeightBold)]), forState: .Normal)
		rb2.addTarget(self, action: #selector(openMail2), forControlEvents: .TouchUpInside)
		self.settings.addSubview(rb2)
		
		help.APPDELEGATE.tabBarController.statusBarBlack = true
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.2)
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.settings.alpha = 1
			
			}, completion: nil)
		
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillChangeSize), name: UIKeyboardWillChangeFrameNotification, object: nil)
	}
	
	var mf : MFMailComposeViewController?
	
	func openMail() {
		
		if self.mf?.view.superview != nil { return }
		
		self.mf = MFMailComposeViewController(nibName: nil, bundle: nil)
		self.mf?.mailComposeDelegate = self
		
		self.mf?.setToRecipients(["contact@varia.io"])
		self.mf?.setSubject("Contact Varia")
		self.mf?.setMessageBody("", isHTML: false)
		
		self.mf?.loadView()
		help.APPDELEGATE.window?.addSubview(self.mf!.view)
		//help.APPDELEGATE.tabBarController.presentViewController(self.mf!, animated: true, completion: nil)
		//help.APPDELEGATE.tabBarController.view.bringSubviewToFront(self.mf!.view)
		
		self.mf?.view.y = self.view.height
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			self.mf?.view.y = 0
			}, completion: nil)
	}
	
	func openMail2() {

		if self.mf?.view.superview != nil { return }
		
		self.mf = MFMailComposeViewController(nibName: nil, bundle: nil)
		self.mf?.mailComposeDelegate = self
		
		self.mf?.setToRecipients(["report@varia.io"])
		self.mf?.setSubject("Report inaccurate / old game data")
		self.mf?.setMessageBody("", isHTML: false)
		
		self.mf?.loadView()
		help.APPDELEGATE.window?.addSubview(self.mf!.view)
		//help.APPDELEGATE.tabBarController.presentViewController(self.mf!, animated: true, completion: nil)
		//help.APPDELEGATE.tabBarController.view.bringSubviewToFront(self.mf!.view)
		
		self.mf?.view.y = self.view.height
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			self.mf?.view.y = 0
			}, completion: nil)
	}

	func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			self.mf?.view.y = self.view.height
			}, completion: { f in self.mf?.view.removeFromSuperview() })
	}
	
	func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.avatarPreview.alpha = 0
			self.avatarEdit.alpha = 0
			self.avatarDelete.alpha = 0
			
			}, completion: nil)
		
		return true
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		if textField.tag == 0 {

			self.cityField.becomeFirstResponder()
		}
		else {
			
			self.cityField.resignFirstResponder()
			
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(200) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
				
				UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
					
					self.nameField.y = (self.view.height)*0.45
					self.cityField.y = self.nameField.y+self.nameField.height+20
					
					}, completion: nil)
			}
		}
		
		return true
	}
	
	func textFieldShouldEndEditing(textField: UITextField) -> Bool {
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			if self.cityField.isFirstResponder() == false && self.nameField.isFirstResponder() == false {
				
				UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
					
					self.avatarPreview.alpha = 1
					self.avatarEdit.alpha = 1
					self.avatarDelete.alpha = 1
					
					}, completion: nil)
			}
		}
		
		return true
	}
	
	func keyboardWillChangeSize(notification: NSNotification) {
		
		let info = notification.userInfo
		let keyboardRect = info?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
		self.keyboardHeight = keyboardRect!.height
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.nameField.y = (self.view.height-self.keyboardHeight)*0.45
			self.cityField.y = self.nameField.y+self.nameField.height+20
			
			}, completion: nil)
	}
	
	func deleteMedia() {
		
		let r1 = try? Realm()
		
		if r1 != nil {
			
			_ = try? r1?.write({
				r1?.delete(r1!.objects(Media.self).filter("type = \'avatar\'"))
			})
		}
		
		self.avatarPreview.image = UIImage(named: "profileBlank")
		profileVC?.reloadContent()
	}
	
	var mediaPicker : MediaPicker!
	var pickedMedia : NSData?
	
	func pickMedia(d: NSData) {
		
		self.pickedMedia = d
		
		self.avatarPreview.image = UIImage(data: self.pickedMedia!)
	}
	
	func saveMedia() {
		
		if self.pickedMedia != nil {
			
			self.deleteMedia()
			
			if let r2 = try? Realm() {
				
				let mediaObj = Media()
				mediaObj.data = self.pickedMedia!
				mediaObj.type = "avatar"
				
				_ = try? r2.write({
					
					r2.add(mediaObj, update: true)
				})
			}
		}
	}
	
	func openMediaPicker() {
		
		self.nameField?.resignFirstResponder()
		self.cityField?.resignFirstResponder()
		
		self.mediaPicker = MediaPicker(frame: CGRect(x: 0, y: 0, width: help.APPDELEGATE.window!.width, height: help.APPDELEGATE.window!.height))
		self.settings.addSubview(self.mediaPicker)
		self.mediaPicker.alpha = 0
		self.mediaPicker.open()
	}
	
	func closeMediaPicker() {
		
		self.mediaPicker?.close()
	}
	
	func saveSettings() {
		
		if self.nameField.text?.length == 0 || self.cityField.text?.length == 0 { return }
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("editUser", [creds, ["name" : self.nameField.text!, "city": self.cityField.text!]])(timeoutAfter: 5, callback: { data in
				
				if let user = data[safe: 0]?["user"] as? NSDictionary {
					
					self.saveMedia()
					User.setUserData(user)
					profileVC?.reloadContent()
				}
			})
		}
		
		self.closeSettings()
	}
	
	func closeSettings() {
		
		self.nameField?.resignFirstResponder()
		self.cityField?.resignFirstResponder()
		
		help.APPDELEGATE.tabBarController.statusBarBlack = false
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.2)
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.settings?.alpha = 0
			
			}, completion: { f in self.settings?.removeFromSuperview() })
	}
	
	func reloadContent() {
		
		if User.getCredentials() != nil {
			
			self.name.attributedText = NSAttributedString(string: User.userName!, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(24, weight: UIFontWeightBold)])
			
			if let city = User.city {
				
				self.location.setAttributedTitle(NSAttributedString(string: city, attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6), NSFontAttributeName:UIFont.systemFontOfSize(15)]), forState: .Normal)
			}
			
			self.pushSwitch.setOn(User.notifications, animated: true)
			
			if gamesVC?.myGamesBacklog.count > 0 || gamesVC?.myGamesPlayed.count > 0 {
				
				self.noGames.alpha = 0
				self.finishedShape.opacity = 1
				self.backlogShape.opacity = 1
				self.finished.alpha = 1
				self.backlog.alpha = 1
				
				let r = CGFloat(gamesVC!.myGamesBacklog.count) / CGFloat(gamesVC!.myGamesPlayed.count+gamesVC!.myGamesBacklog.count)
				
				self.finishedShape.strokeEnd = r
				self.backlogShape.strokeEnd = 1.0 - r
				
				self.finishedShape.setAffineTransform(CGAffineTransformMakeRotation(CGFloat(M_PI * 2.0) * -0.25))
				self.backlogShape.setAffineTransform(CGAffineTransformMakeRotation(CGFloat(M_PI * 2.0) * -(0.25-r)))
				
				if gamesVC!.myGamesBacklog.count == 0 {
					
					self.backlogShape.strokeStart = 0
				}
				else if gamesVC!.myGamesPlayed.count == 0 {
					
					self.finishedShape.strokeStart = 0
				}
				
				let attStr = NSMutableAttributedString(string: "\(gamesVC!.myGamesPlayed.count)\n", attributes: [NSForegroundColorAttributeName:help.babyBlue, NSFontAttributeName:UIFont.systemFontOfSize(30, weight: UIFontWeightBold), NSKernAttributeName:0.6])
				attStr.appendAttributedString(NSAttributedString(string: "Finished".uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold),NSKernAttributeName:0.5]))
				
				let attStr2 = NSMutableAttributedString(string: "\(gamesVC!.myGamesBacklog.count)\n", attributes: [NSForegroundColorAttributeName:help.yellow, NSFontAttributeName:UIFont.systemFontOfSize(30, weight: UIFontWeightBold), NSKernAttributeName:0.6])
				attStr2.appendAttributedString(NSAttributedString(string: "Backlog".uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold),NSKernAttributeName:0.5]))
				
				self.finished.attributedText = attStr
				self.backlog.attributedText = attStr2
			}
			else {
				
				self.finishedShape.opacity = 0
				self.backlogShape.opacity = 0
				self.finished.alpha = 0
				self.backlog.alpha = 0
				self.noGames.alpha = 1
			}
			
			
			//
			
			let r1 = try? Realm()
			
			let o = r1?.objects(Media.self).filter("type = \'avatar\'")
			
			if let d = o?.first?.data {
				
				self.avatar.image = UIImage(data: d)
			}
			else {
				
				self.avatar.image = UIImage(named: "profileBlank")
			}
			
			//
			
			self.collectionView.reloadData()
		}
		else {
			
			
		}
	}
	
	//collection view
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.scrollDirection = .Vertical
		layout.itemSize = CGSize(width: self.view.width, height: 1)
		self.collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.view.frame.size), collectionViewLayout: layout)
		self.collectionView?.dataSource = self
		self.collectionView?.delegate = self
		self.collectionView?.registerClass(GameCell.self, forCellWithReuseIdentifier: "GameCell")
		self.collectionView?.backgroundColor = .clearColor()
		self.collectionView?.alpha = 1
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.alwaysBounceVertical = true
		self.collectionView?.scrollEnabled = true
		self.collectionView?.clipsToBounds = false
		self.collectionView?.tag = 0
		
		self.view.insertSubview(self.collectionView!, belowSubview: self.topBar)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		
		return CGFloat(13)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		if section % 2 == 0 { return UIEdgeInsets(top: self.profileBG.height, left: 0, bottom: 0, right: 0) }
		return UIEdgeInsets(top: 0, left: 14, bottom: 40, right: 14)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		
		return 2
		
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if section % 2 == 0 { return 1 }
		return myGamesReviewed.count
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if indexPath.section % 2 == 0 { return CGSize(width: self.view.width, height: 52) }
		return CGSize(width: (self.view.width/2)-20, height: (self.view.width/2))
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
		
		cell.layer.shouldRasterize = true
		cell.layer.rasterizationScale = UIScreen.mainScreen().scale
		cell.backgroundColor = .clearColor()
		
		cell.indexPath = indexPath
		cell.label.alpha = 0
		cell.badge.alpha = 0
		cell.textView.alpha = 0
		cell.imageView.alpha = 0
		
		if indexPath.section % 2 == 0 {
			
			cell.type = .LeftHeader
			cell.label.text = "My reviews"
		}
		else {
			
			cell.type = .GridReview
			
			let r = Int(arc4random_uniform(1000))
			cell.imageView.tag = r
			
			if let game = myGamesReviewed[safe: indexPath.item] {
				
				cell.label.text = "\(game.rating.description) / 10"
				
				if game.coverurl.length > 0 {
					
					let tempIP = cell.indexPath
					let tempCount = collectionView.numberOfItemsInSection(indexPath.section)
					
					getMediaBySrc(game.coverurl, game: game.id, completion: { img in
						
						if tempIP == cell.indexPath && r == cell.imageView.tag {
							
							cell.imageView.image = img
							UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
								cell.imageView.alpha = 0.5
								}, completion: nil)
						}
						else if tempCount == self.collectionView.numberOfItemsInSection(tempIP!.section) {
							
							self.collectionView.reloadItemsAtIndexPaths([tempIP!, indexPath])
						}
						
					})
				}
				else {
					
					//cell.imageView.image = gradientImage(help.purple1, toColor: help.babyPurple)
					//cell.imageView.alpha = 1
				}
			}
		}
		
		cell.refresh()
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if indexPath.section == 0 { return }
		
		if let g = myGamesReviewed[safe: indexPath.item] {
			
			let v = GameScreen(game: g, owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
			
			self.showModal(v)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
}
