//
//  MediaPicker.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-26.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class MediaPicker: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
	
	var cameraRoll : UICollectionView!
	
	var active = false
	var media : PHFetchResult?
	var mediaTempStorage : Array<UIImage?> = []
	
	var selectedCell:Int?
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.setupCollectionView()
		self.loadCameraRollData()
		
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		
		if touches.first?.locationInView(self).y < self.cameraRoll.y-10 {
			
			profileVC?.closeMediaPicker()
		}
	}
	
	func loadCameraRollData() {
		
		if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum) == true {
			
			let fetchOptions: PHFetchOptions = PHFetchOptions()
			
			fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
			
			//
			
			let status = PHPhotoLibrary.authorizationStatus()
			
			if status == PHAuthorizationStatus.Authorized {
				
				self.media = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions)
				for (var m = 0; m<self.media!.count ; m+=1) { self.mediaTempStorage.append(nil) }
				self.cameraRoll.reloadData()
				
			} else if status == PHAuthorizationStatus.NotDetermined {
				
				PHPhotoLibrary.requestAuthorization({ (status) -> Void in
					
					if status == PHAuthorizationStatus.Authorized {
						
						dispatch_async(dispatch_get_main_queue(), {
							
							self.media = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions)
							for (var m = 0; m<self.media!.count ; m += 1) { self.mediaTempStorage.append(nil) }
							self.cameraRoll.reloadData()
						})
					}
					else {
						
						dispatch_async(dispatch_get_main_queue(), {
							
							self.close()
						})
					}
				})
			} else {
				
				self.close()
			}
		}
	}
	
	func open() {
		
		self.active = true
		self.alpha = 1
		self.backgroundColor = .clearColor()
		self.cameraRoll.alpha = 0
		
		self.cameraRoll.y = self.height-10
		
		UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .CurveEaseInOut, animations: {
			
			self.cameraRoll.alpha = 1
			self.cameraRoll.y -= self.cameraRoll.height
			self.backgroundColor = UIColor(white: 0, alpha: 0.6)
			
			}, completion: nil)
		
		self.loadCameraRollData()
		self.deselectAnyCell()
		
		self.cameraRoll.layoutIfNeeded()
		self.cameraRoll.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
	}
	
	func close() {
		
		self.active = false
		
		UIView.animateWithDuration(0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .CurveEaseInOut, animations: {
			
			self.cameraRoll.alpha = 0
			self.cameraRoll.y = self.height
			self.backgroundColor = .clearColor()
			
			}, completion: { finished in
				
				self.alpha = 0
		})
	}
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.scrollDirection = .Horizontal
		layout.itemSize = CGSizeMake(1, 1)
		self.cameraRoll = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.width, height: 200), collectionViewLayout: layout)
		self.cameraRoll.dataSource = self
		self.cameraRoll.delegate = self
		self.cameraRoll.registerClass(CameraRollCell.self, forCellWithReuseIdentifier: "CameraRollCell")
		self.cameraRoll.backgroundColor = .clearColor()
		self.cameraRoll.alpha = 1
		self.cameraRoll.scrollIndicatorInsets = UIEdgeInsetsMake(10, 0, 0, 0);
		self.cameraRoll.directionalLockEnabled = true
		self.cameraRoll.showsHorizontalScrollIndicator = true
		self.cameraRoll.tag = 0
		
		self.addSubview(self.cameraRoll)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		return CGFloat(2)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
		return CGFloat(3)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if self.media?.count > 0 {
			
			return self.media!.count
		}
		else {
			return 0
		}
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		let r = self.cameraRoll.height / CGFloat(self.media!.objectAtIndex(indexPath.item).pixelHeight)
		let w = CGFloat(self.media!.objectAtIndex(indexPath.item).pixelWidth) * r
		return CGSize(width: w, height: self.cameraRoll.height)
	}
	
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell:CameraRollCell = collectionView.dequeueReusableCellWithReuseIdentifier("CameraRollCell", forIndexPath: indexPath) as! CameraRollCell
		
		cell.backgroundColor = UIColor(white: 0.94, alpha: 1)
		cell.clipsToBounds = true
		cell.layer.masksToBounds = true
		cell.layer.cornerRadius = 4
		cell.selectedGlyph?.alpha = 0
		if self.selectedCell != indexPath.item { cell.imageView?.alpha = 0 }
		cell.imageView?.image = nil
		cell.alpha = 1
		
		if let img = self.mediaTempStorage[indexPath.item] as UIImage! {
			
			cell.imageView?.image = img
			if self.selectedCell != nil && self.selectedCell != indexPath.item { cell.alpha = 0.4 }
			if self.selectedCell == indexPath.item { cell.alpha = 1 }
			cell.imageView?.alpha = 1
		}
		else {
			
			PHImageManager.defaultManager().requestImageForAsset(self.media?.objectAtIndex(indexPath.item) as! PHAsset, targetSize: CGSize(width: self.frame.width, height: self.frame.height), contentMode: PHImageContentMode.AspectFit, options: PHImageRequestOptions(), resultHandler: { (result, info) -> Void in
				
				if result != nil {
					
					if info?[PHImageResultIsDegradedKey] as? Bool == false {
						
						self.mediaTempStorage[indexPath.item] = result
						cell.imageView?.image = result
						
						UIView.animateWithDuration(0.2, animations: {
							
							cell.imageView?.alpha = 1
							cell.alpha = 1
							if self.selectedCell != nil { cell.alpha = 0.4 }
							if self.selectedCell == indexPath.item { cell.alpha = 1 }
							}, completion: nil)
						
					}
				}
				
			})
		}
		
		
		cell.refreshViews()
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if self.selectedCell != nil { return }
			
			let a = self.media?.objectAtIndex(indexPath.item) as! PHAsset
			//let w = a.pixelWidth
			//let h = a.pixelHeight
			
			PHImageManager.defaultManager().requestImageDataForAsset(a, options: PHImageRequestOptions(), resultHandler: { (data, uti, o, d) -> Void in
				
				if uti?.rangeOfString("gif") != nil || uti?.rangeOfString("GIF") != nil {
					
					profileVC?.pickMedia(data!)
				}
				else if uti?.rangeOfString("jpeg") != nil || uti?.rangeOfString("JPEG") != nil {
					
					profileVC?.pickMedia(data!)
				}
				else if uti?.rangeOfString("png") != nil || uti?.rangeOfString("PNG") != nil {
					
					profileVC?.pickMedia(data!)
					
				}
				
				//let oldSelectedCell = self.selectedCell
				self.selectedCell = indexPath.item
				
				self.cameraRoll.reloadData()
				self.flashSelected()
				
				
				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
				
					self.close()
				}
			})
		
	}
	
	var l : UILabel!
	
	func flashSelected() {
		
		self.l?.removeFromSuperview()
		
		var s = "No attachments"
		if self.selectedCell != nil { s = "Attached: 1 image!" }
		
		self.l = UILabel(frame: CGRect(x: 0, y: self.cameraRoll.y-40, width: self.width, height: 20))
		self.l.attributedText = NSAttributedString(string: s, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)])
		self.l.textAlignment = .Center
		self.l.alpha = 0
		self.addSubview(self.l)
		
		UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.l.alpha = 1
			
			}, completion: { f in
				
				UIView.animateWithDuration(0.2, delay: 2, options: .CurveEaseInOut, animations: {
					
					self.l.alpha = 0
					
					}, completion: { f in
				})
		})
	}
	
	func deselectAnyCell() {
		
		self.selectedCell = nil
		self.cameraRoll.reloadData()
	}
	
	func resetAllTempData() {
		
		self.deselectAnyCell()
		self.media = nil
		self.mediaTempStorage.removeAll(keepCapacity: false)
		self.cameraRoll.reloadData()
	}
	
	
	
	class CameraRollCell: UICollectionViewCell {
		
		var imageView:UIImageView?
		var selectedGlyph:UIButton?
		
		override init(frame: CGRect) {
			super.init(frame: frame)
			
			self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
			self.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
			self.imageView?.backgroundColor = UIColor.clearColor()
			self.imageView?.clipsToBounds = true
			self.contentView.addSubview(self.imageView!)
			
			/*self.selectedGlyph = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
			self.selectedGlyph?.contentMode = .Center
			self.selectedGlyph?.backgroundColor = UIColor(white: 0, alpha: 0.3)
			self.selectedGlyph?.setTitle("📡", forState: .Normal)
			self.selectedGlyph?.titleLabel?.font = UIFont.systemFontOfSize(30)
			//self.selectedGlyph?.addTarget(help.APPDELEGATE.chatViewController.bottomBar, action: #selector(BottomBar.sendMedia), forControlEvents: .TouchUpInside)
			//self.contentView.addSubview(self.selectedGlyph!)*/
		}
		
		func refreshViews() {
			
			self.imageView?.frame.size = self.frame.size
			self.selectedGlyph?.frame.size = self.frame.size
			
		}
		
		required init?(coder aDecoder: NSCoder) {
			super.init(coder: aDecoder)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
