//
//  Socket.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-20.
//  Copyright © 2016 LC. All rights reserved.
//

import Foundation

let socket = SocketIOClient(socketURL: "52.91.254.172:4200", opts: [:/*"connectParams":["key":help.CFConnectKey]*/])

var latestUpdate = "0000-00-00 00:00:00"
var latestUpdateManifest = NSDate(timeIntervalSince1970: 0)

class SocketManager {
	
	private static var hasShownOfflineMessage = false
	private static var shouldAcceptFreshAuth = true
	
	class func authenticateAndConnect(completion: (error: String?) -> ()) {
		
		socket.once("authenticationFailed") { (data, ack) -> Void in
			print("Authentication failed.")
			
			completion(error: "AuthFailed")
		}
		
		socket.once("connect") { data, ack in
			print("socket connected")
			
			if self.shouldAcceptFreshAuth == false { return }; self.shouldAcceptFreshAuth = false
			
			if let credentials = User.getCredentials() {
				
				let latest = latestUpdate
				
				socket.emitWithAck("authenticate", [credentials, ["checkpoint": latest]])(timeoutAfter: 4, callback: { data in
					
					if (data[safe: 0] as? String)?.lowercaseString == "no ack" {
						
						print("No ack.")
						shouldAcceptFreshAuth = true
						completion(error: "Timeout")
					}
					else {
						
						print("Authenticated: \(data)")
						
						if latestUpdate == "0000-00-00 00:00:00" {
							
							latestUpdate = "1111-11-11 11:11:11"
							latestUpdateManifest = help.APPDELEGATE.dateFormatter.dateFromString("1111-11-11 11:11:11")!
						}
						
						if let user = data[safe: 0]?.objectForKey("user") as? NSDictionary {
							
							User.setUserData(user)
						}
						
						completion(error: nil)
					}
				})
			}
			else {
				
				completion(error: "NoAuth")
			}
		}
		socket.connect()
		
	}
	class func setupSocket() {
		
		socket.on("myGames", callback: { (data, ack) -> Void in
			print(data)
			if let games = (data[0] as? NSDictionary)?.objectForKey("games") as? NSArray {
				
				dropGames(games)
			}
			else {
				print("Problem obtaining games!")
			}
		})
		
		socket.on("newSlides", callback: { (data, ack) -> Void in
			
			if let slides = (data[0] as? NSDictionary)?.objectForKey("slides") as? [NSDictionary] {
				
					exploreVC?.slider.slides = slides
					exploreVC?.slider.refreshAll()
			}
			else {
				print("Problem obtaining slides!")
			}
		})
		
		socket.on("connect") { data, ack in
			
		}
		
		socket.on("reconnect", callback: { (data, ack) -> Void in
			print("Disconnected! (Reconnect)")
			
			self.showOfflineAndTryToReconnect(data)
		})
		
		socket.on("authenticationFailed") { (data, ack) -> Void in
			print("Authentication failed.")
			
			resetEverything()
		}
		
		socket.on("disconnect") { (data, ack) -> Void in
			print("Disconnected!")
			
			self.shouldAcceptFreshAuth = true
			self.showOfflineAndTryToReconnect(data)
		}
		
		socket.onAny { (SocketAnyEvent) -> Void in
			//   print(SocketAnyEvent)
		}
	}
	
	class func resetEverything() {
		
		User.resetUserData()
		//latestUpdate = "0000-00-00 00:00:00";
	}
	private static func showOfflineAndTryToReconnect(data: [AnyObject]?) {
		
		if (data?[0] as? String)?.lowercaseString.rangeOfString("offline") != nil && hasShownOfflineMessage == false {
			showNotificationBar("No internet connection :(", type: .Error)
			hasShownOfflineMessage = true
		}
		//_ = NSTimer.scheduledTimerWithTimeInterval(2, target: help.APPDELEGATE.chatViewController, selector: #selector(ChatViewController.willEnter), userInfo: nil, repeats: false)
	}
}
