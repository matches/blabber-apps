//
//  ExploreViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import RealmSwift

let sliderHeight = CGFloat(200)

class ExploreViewController: GenericViewController, UICollectionViewDataSource {
	
	var currentMonth : Int!
	var currentYear : Int!
	var pickedMonth : NSDate!
	var pickedMonthString : String!
	
	var collectionView : UICollectionView!
	var slider : Slider!
	var calendar : Calendar?
	
	var whatsNewRaw : [AnyObject] = []
	var whatsNew : [AnyObject] = []
	
	var spinner : TinySpinner!
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor(white: 0.1, alpha: 1)//.whiteColor()
		
		let r = try? Realm()
		
		if r?.objects(Media.self).count > 150 {
			
			print("Media objs: ", r?.objects(Media.self).count, "Deleting!")
	
			_ = try? r?.write({
				
				r?.delete(r!.objects(Media.self))
			})
		}
		
		self.pickedMonth = NSDate()
		
		self.topBarLabel.text = "Explore"
		
		self.topBarLeftButton.setImage(UIImage(named: "search"), forState: .Normal)
		self.topBarLeftButton.setImage(UIImage(named: "search")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarLeftButton.addTarget(self, action: #selector(openSearch), forControlEvents: .TouchDown)
		
		self.topBarRightButton.setImage(UIImage(named: "calendar"), forState: .Normal)
		self.topBarRightButton.setImage(UIImage(named: "calendar")?.fillWithColor(help.babyBlue), forState: .Highlighted)
		self.topBarRightButton.addTarget(self, action: #selector(openCalendar), forControlEvents: .TouchDown)
		
		self.setupSlider()
		self.setupCollectionView()
		
		SocketManager.setupSocket()
		self.startupAuth()
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(willEnter), name: UIApplicationWillEnterForegroundNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(willResign), name: UIApplicationDidEnterBackgroundNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(willResign), name: UIApplicationWillTerminateNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(willEnter), name: UIApplicationDidBecomeActiveNotification, object: nil)
	}
	
	func startupAuth() {
		
		SocketManager.authenticateAndConnect { (error) -> () in
			
			self.pickedMonthString = help.APPDELEGATE.dateFormatter.stringFromDate(self.pickedMonth)
			self.currentMonth = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: self.pickedMonth)-1
			self.currentYear = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: self.pickedMonth)
			
			if error == nil {
				
				self.refreshWhatsNew()
				profileVC?.reloadContent()
				help.APPDELEGATE.evaluateIfShouldRegisterForNotifications()
			}
			else if error == "Timeout" {
				
				socket.close()
				socket.open()
				self.startupAuth()
			}
			else if error == "NoAuth" || error == "AuthFailed" {
				
				self.signupAsGuest()
			}
		}
	}
	
	func willEnter() {
		
		if (socket.status == .Closed || socket.status == .NotConnected || socket.status == .Reconnecting) {
			
			SocketManager.authenticateAndConnect { (error) -> () in
				
				if error == nil {
					
					
				}
				else if error == "Timeout" {
					
					
				}
				else if error == "NoAuth" || error == "AuthFailed" {
					
					User.resetUserData()
					self.signupAsGuest()
				}
			}
		}
	}
	
	func willResign() {
		
		socket.disconnect()
	}
	
	func showStart() {
		
		let start = Start(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		help.APPDELEGATE.window?.addSubview(start)
		
		help.APPDELEGATE.tabBarController.statusBarHidden = true
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.2)
		start.alpha = 0
		UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
			start.alpha = 1
			}, completion: nil)
	}
	
	func signupAsGuest() {
		
		socket.emitWithAck("signUp", ["type": "guest"])(timeoutAfter: 10, callback: { data in
			
			print(data)
			
			if (data[safe: 0] as? String)?.lowercaseString == "no ack" {
				
				showNotificationBar("Can't signup! Try re-opening the app.", type: .Error)
			}
			else if let user = data[safe: 0]?.objectForKey("user") as? NSDictionary {
				
				self.showStart()
				User.setUserData(user)
				profileVC?.reloadContent()
				
			}
			else if let err = data[safe: 0]?.valueForKey("error") as? String {
				
				showNotificationBar(err, type: .Error)
			}
			else {
				
				showNotificationBar("Can't signup! Try re-opening the app.", type: .Error)
			}
		})
	}
	
	func refreshWhatsNew() {
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("getWhatsNew", [creds, ["month": self.pickedMonthString]])(timeoutAfter: 5, callback: { data in
				
				if let games = (data[0] as? NSDictionary)?.objectForKey("games") as? NSArray {
					
					self.refreshWhatsNewSorting(games as [AnyObject])
					
					UIView.animateWithDuration(0.15, animations: { self.spinner?.alpha = 0; }, completion: {f in self.spinner?.stopAnimating(); self.spinner?.removeFromSuperview() })
				}
				else {
					
					showNotificationBar("Couldn't get games. Try again.", type: .Error)
				}
			})
		}
	}
	
	func refreshWhatsNewSorting(wn: [AnyObject]) {
		
		self.whatsNewRaw.removeAll()
		self.whatsNew.removeAll()
		
		for g in wn {
			self.whatsNewRaw.append(Game(obj: g as! NSDictionary))
		}
		
		let sortedOld = self.whatsNewRaw.filter({ g in
			
			if (g as! Game).releasedate != nil {
				return true
			}
			return false
			
		}).sort { (g1, g2) -> Bool in
			
			if ((g1 as! Game).releasedate?.compare((g2 as! Game).releasedate!) == NSComparisonResult.OrderedDescending) {
				
				return false
			}
			return true
		}
		
		var sortedNew : [AnyObject] = []
		var previousWeek : Int?
		
		for g in sortedOld {
			
			let w = NSCalendar.currentCalendar().component(NSCalendarUnit.WeekOfYear, fromDate: (g as! Game).releasedate!)
			let m = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: (g as! Game).releasedate!)
			let d = NSCalendar.currentCalendar().component(NSCalendarUnit.Day, fromDate: (g as! Game).releasedate!)
		
			if w != previousWeek {
				
				if sortedNew.count > 1 {
					
					var itemsInPreviousSection = 0
					
					for (var x = sortedNew.count-1; x > -1; x -= 1) {
						
						if sortedNew[safe: x]?.isKindOfClass(Game.self) == false {
							
							break
						}
						itemsInPreviousSection += 1;
					}
					
					if itemsInPreviousSection % 2 != 0 {
						
						sortedNew.append(["blank":true])
					}
				}
				
					
					sortedNew.append(["isheader": true, "title": "Week of \(NSDateFormatter().monthSymbols[m-1]) \(d.description)"])
			}
			
			sortedNew.append(g);
			
			previousWeek = w
		}
		
		self.whatsNew = sortedNew
		self.collectionView.reloadData()
		print(self.whatsNew)
	}
	
	func openCalendar() {
		
		self.calendar = Calendar(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		help.APPDELEGATE.tabBarController.view.addSubview(self.calendar!)
		self.calendar?.open()
	}
	
	func openSearch() {
		
		let search = Search(owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
		self.showModal(search)
		search.field.becomeFirstResponder()
	}
	
	func setupSlider() {
		
		self.slider = Slider(frame: CGRect(x: 0, y: 0, width: self.view.width, height: sliderHeight+128))
		
	}
	
	override func scrollViewDidScroll(scrollView: UIScrollView) {
		super.scrollViewDidScroll(scrollView)
		
		if scrollView.contentOffset.y < -64 {
			 
			self.slider?.y = (scrollView.contentOffset.y)
			self.slider?.height = sliderHeight+(-scrollView.contentOffset.y)+128
		}
		else {
			
			UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
				
				self.slider?.y = -64
				self.slider?.height = sliderHeight+192
				
				}, completion: nil)
			
		}
		self.slider.updateScroll(scrollView.contentOffset.y+64)
	}
	
	
	//collection view
	
	func setupCollectionView() {
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.scrollDirection = .Vertical
		layout.itemSize = CGSize(width: self.view.width, height: 1)
		self.collectionView = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.view.frame.size), collectionViewLayout: layout)
		self.collectionView?.dataSource = self
		self.collectionView?.delegate = self
		self.collectionView?.registerClass(GameCell.self, forCellWithReuseIdentifier: "GameCell")
		self.collectionView?.backgroundColor = .clearColor()
		self.collectionView?.alpha = 1
		self.collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 50, right: 0)
		self.collectionView?.directionalLockEnabled = true
		self.collectionView?.showsHorizontalScrollIndicator = false
		self.collectionView?.alwaysBounceVertical = true
		self.collectionView?.scrollEnabled = true
		self.collectionView?.clipsToBounds = false
		self.collectionView?.tag = 0
		
		self.view.insertSubview(self.collectionView!, belowSubview: self.topBar)
		self.collectionView.addSubview(self.slider)
		
		self.spinner = TinySpinner(frame: CGRect(x: self.view.width/2-10, y: self.slider.height+60, width: 20, height: 20))
		self.spinner.lineColor = .whiteColor()
		self.spinner.runSpinAnimation()
		self.spinner.alpha = 0.5
		self.collectionView.addSubview(spinner)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		
		return CGFloat(13)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
		
		if section % 2 == 0 { return UIEdgeInsets(top: self.slider.height-64, left: 0, bottom: 0, right: 0) }
		return UIEdgeInsets(top: 16, left: 14, bottom: 40, right: 14)
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		
		return 2
		
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if section % 2 == 0 { return 1 }
		if self.whatsNew.count == 0 { return 0 }
		return self.whatsNew.count-1
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if indexPath.section % 2 == 0 { return CGSize(width: self.view.width, height: 52) }
		if (self.whatsNew[safe: indexPath.item+1] as? NSDictionary)?.valueForKey("isheader") as? Bool == true {
			
			return CGSize(width: self.view.width, height: 40)
		}
		
		return CGSize(width: (self.view.width/2)-20, height: (self.view.width/2)+70)
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameCell", forIndexPath: indexPath) as! GameCell
		
		cell.layer.shouldRasterize = true
		cell.layer.rasterizationScale = UIScreen.mainScreen().scale
		cell.backgroundColor = .clearColor()
		
		cell.indexPath = indexPath
		cell.label.alpha = 0
		cell.textView.alpha = 0
		cell.imageView.image = nil
		cell.imageView.alpha = 0
		cell.month.text = ""
		cell.badge.text = ""
		
		if indexPath.section % 2 == 0 {
			
			cell.type = .LeftHeader
			cell.label.text = "What's new"
			
			if let h1 = self.whatsNew.first as? NSDictionary {

				cell.label.text = h1.valueForKey("title") as? String
 			}
			
			let mm = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: self.pickedMonth)-1
			let yy = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: self.pickedMonth)
			cell.month.text = "\(NSCalendar.currentCalendar().monthSymbols[mm]) \(yy)"
			
			cell.refresh()
			
			let attStr = NSMutableAttributedString(attributedString: cell.label.attributedText!)
			attStr.addAttributes([NSForegroundColorAttributeName:help.yellow], range: NSMakeRange(0, attStr.length))
			cell.label.attributedText = attStr
			
		}
		else {
			
			cell.type = .Grid
			
			let r = Int(arc4random_uniform(1000))
			cell.imageView.tag = r
			
			if (self.whatsNew[safe: indexPath.item+1] as? NSDictionary)?.valueForKey("isheader") as? Bool == true {
				
				cell.type = .Badge
				cell.badge.text = ((self.whatsNew[safe: indexPath.item+1] as? NSDictionary)?.valueForKey("title") as? String)?.uppercaseString
			}
			else if (self.whatsNew[safe: indexPath.item+1] as? NSDictionary)?.valueForKey("blank") as? Bool == true {
				
				cell.type = .Custom
			}
			else if let g = self.whatsNew[safe: indexPath.item+1] as? Game {
				
				cell.textView.text = g.name
				
				if g.coverurl.length > 0 {
					
					let tempIP = cell.indexPath
					let tempCount = collectionView.numberOfItemsInSection(indexPath.section)
					
					let c = g.coverurl
					
					getMediaBySrc(c, game: g.id, completion: { img in
						
						if tempIP == cell.indexPath && r == cell.imageView.tag {
							
							cell.imageView.image = img
							UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
								cell.imageView.alpha = 1
								}, completion: nil)
						}
						else if tempCount == self.collectionView.numberOfItemsInSection(tempIP!.section) && tempIP != cell.indexPath {
							
							self.collectionView.reloadItemsAtIndexPaths([tempIP!, cell.indexPath!])
						}
						
					})
				}
				else {
					
					//cell.imageView.image = gradientImage(help.purple1, toColor: help.babyPurple)
					//cell.imageView.alpha = 1
				}
			}
			cell.refresh()
		}
		
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if indexPath.section == 0 { return }
		
		if let g = self.whatsNew[safe: indexPath.item+1] as? Game {
			
			let v = GameScreen(game: g, owner: self, frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
			
			self.showModal(v)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

class TinySpinner: UIImageView {
	
	var line = CAShapeLayer()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		let path = UIBezierPath(ovalInRect: CGRect(origin: CGPointZero, size: frame.size))
		
		line = CAShapeLayer()
		line.frame = CGRect(origin: CGPointZero, size: frame.size)
		line.strokeColor = UIColor.blackColor().CGColor
		line.strokeEnd = 0.8
		line.lineWidth = 2
		line.fillColor = UIColor.clearColor().CGColor
		line.path = path.CGPath
		
		self.layer.addSublayer(line)
	}
	
	var lineColor : UIColor {
		
		get {
			return UIColor(CGColor: self.line.strokeColor!)
		}
		set(color) {
			
			self.line.strokeColor = color.CGColor
		}
	}
	
	var gapPercentage : CGFloat {
		
		get {
			return self.line.strokeEnd
		}
		set(gap) {
			
			line.strokeEnd = gap
		}
	}
	
	override var alpha : CGFloat {
		
		get {
			return self.alpha
		}
		set(alpha) {
			
			if alpha > 0 {
				
				self.runSpinAnimation()
			}
			else {
				
				self.layer.removeAnimationForKey("rotationAnimation")
			}
			super.alpha = alpha
		}
	}
	
	func runSpinAnimation() {
		
		let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z");
		rotationAnimation.toValue = M_PI * 2.0 * 6
		rotationAnimation.duration = 3;
		rotationAnimation.cumulative = true;
		rotationAnimation.repeatCount = HUGE
		self.layer.addAnimation(rotationAnimation, forKey:"rotationAnimation");
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

