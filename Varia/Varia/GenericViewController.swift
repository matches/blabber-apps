//
//  GenericViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class GenericModal: UIView {
	
	var owner : GenericViewController?
	var side = true
	var closing = false
	
	init(owner: GenericViewController, frame: CGRect) {
		super.init(frame: frame)
		
		self.owner = owner
		
		let r = UIPanGestureRecognizer(target: self.owner!, action: #selector(GenericViewController.didPan(_:)))
		self.addGestureRecognizer(r)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

class GenericViewController: UIViewController, UICollectionViewDelegateFlowLayout {
	
	var topBar: UIVisualEffectView!
	var topBarLabel : UILabel!
	var topBarLeftButton : UIButton!
	var topBarRightButton : UIButton!
	
	var modals : [GenericModal] = []
	
	var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.topBar = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: 64))
		self.topBar.effect = self.blurEffect
		self.topBar.backgroundColor = UIColor(white: 1, alpha: 0.35)//help.babyBlue.darkerByPercentage(6)//help.purple1
		self.view.addSubview(self.topBar)
		
		self.topBarLabel = UILabel(frame: CGRect(x: 50, y: 20, width: self.view.width-100, height: 44))
		self.topBarLabel.font = UIFont.systemFontOfSize(19, weight: UIFontWeightBold)
		self.topBarLabel.textAlignment = .Center
		self.topBarLabel.textColor = .whiteColor()
		self.topBar.addSubview(self.topBarLabel)
		
		self.topBarLeftButton = UIButton(frame: CGRect(x: 0, y: 20, width: 48, height: 44))
		self.topBar.addSubview(self.topBarLeftButton)
		
		self.topBarRightButton = UIButton(frame: CGRect(x: self.view.width-48, y: 20, width: 48, height: 44))
		self.topBar.addSubview(self.topBarRightButton)
	}
	
	func didPan(r: UIPanGestureRecognizer) {
		
		if self.modals.count == 0 || self.modals.last?.side == false || (r.state == .Began && r.locationInView(help.APPDELEGATE.tabBarController.view).x > 40) {
			
			r.enabled = false
			r.enabled = true
			r.delaysTouchesBegan = false
			r.delaysTouchesEnded = false
		}
		
		if r.state == .Changed {
			
			self.moveScreen(r.locationInView(help.APPDELEGATE.tabBarController.view).x)
		}
		else if r.state == .Cancelled {
			
			self.resetLastModal()
		}
		else if r.state == .Ended {
			
			if r.locationInView(help.APPDELEGATE.tabBarController.view).x < 160 {
				
				self.resetLastModal()
			}
			else {
				
				self.hideLastModalAnimated()
			}
		}
	}
	
	func moveScreen(x: CGFloat) {
		
		let r = x/self.view.width
		
		UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
			
			UIView.setAnimationDuration(0.1)
			self.modals.last?.x = x
			UIView.setAnimationDuration(0.2)
		
			if self.modals.count > 1 {
				
				self.modals[safe: self.modals.count-2]?.alpha = r
				self.modals[safe: self.modals.count-2]?.x = (-40)+40*r
			}
			else {
				
				self.view.alpha = r
				self.view.x = (-40)+40*r
			}
			
			}, completion: { f in })
	}
	
	func resetLastModal() {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.modals.last?.x = 0
			
			if self.modals.count < 2 {
				
				if self.modals.last?.closing == false { self.view.alpha = 0 }
				if self.modals.last?.side == true { self.view.x = -40 }
			}
			else {
				
				self.modals[safe: self.modals.count-2]?.alpha = 0
				if self.modals.last?.side == true { self.modals[safe: self.modals.count-2]?.x = -40 }
			}
			
			}, completion: { f in })
	}
	
	func showModal(view: GenericModal) {
		
		self.modals.append(view)
		
		if view.isKindOfClass(GameScreen.self) == false {
			
			help.APPDELEGATE.tabBarController.view.insertSubview(view, belowSubview: help.APPDELEGATE.tabBarController.bar)
		}
		else {
			
			help.APPDELEGATE.tabBarController.view.addSubview(view)
		}
		
		if view.side { self.modals.last?.x = self.view.width }
		self.resetLastModal()
		
	}
	
	func hideLastModalAnimated() {
		
		(self.modals.last as? Search)?.field.resignFirstResponder()
		
		let lessThanTwo = self.modals.count < 2
		let m = self.modals.removeLast()
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			if m.side == true {
				
				m.x = self.view.width
				
				if lessThanTwo {
					
					self.view.alpha = 1
					self.view.x = 0
				}
				else {
					
					self.modals[safe: self.modals.count-1]?.alpha = 1
					self.modals[safe: self.modals.count-1]?.x = 0
				}
			}
			else {
				
				self.view.alpha = 1
				m.alpha = 0
			}
			
			}, completion: { f in
				
				m.removeFromSuperview()
				
		})
	}
	
	var previousOffset = CGFloat(0)
	var consecutive = CGFloat(0)
	
	func scrollViewDidScroll(scrollView: UIScrollView) {
		
		if scrollView.contentOffset.y < -20 {
			
			if self.topBar?.y != 0 {
				
				self.topBar?.y = min(-(scrollView.contentOffset.y+64), 0)
				let a = 1.0 - (((scrollView.contentOffset.y)+64) / 44)
				self.topBar?.subviews.forEach({ sv in if sv.isKindOfClass(UILabel.self) || sv.isKindOfClass(UIButton.self) { sv.alpha = a } })
			}
		}
		else {
			
			if scrollView.contentOffset.y < self.previousOffset {
				
				consecutive += (self.previousOffset - scrollView.contentOffset.y)
				
				self.topBar?.y = min((self.consecutive-44), 0)
				let a = ((self.consecutive) / 44)
				self.topBar?.subviews.forEach({ sv in if sv.isKindOfClass(UILabel.self) || sv.isKindOfClass(UIButton.self) { sv.alpha = a } })
			}
			else {
				
				UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
					
					self.topBar?.y = -44
					self.topBar?.subviews.forEach({ sv in if sv.isKindOfClass(UILabel.self) || sv.isKindOfClass(UIButton.self) { sv.alpha = 0 } })
					
					}, completion: nil)
				
				self.consecutive = 0
			}
		}
		
		self.previousOffset = scrollView.contentOffset.y
	}
	
	func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		
	}
	
	func scrollViewDidScrollToTop(scrollView: UIScrollView) {
		
		self.previousOffset = 0
		self.consecutive = 0
		
		if self.topBar?.y != 0 {
			
			self.topBar?.y = 0
			self.topBar?.subviews.forEach({ sv in if sv.isKindOfClass(UILabel.self) || sv.isKindOfClass(UIButton.self) { sv.alpha = 1 } })
		}
	}
	
	func tabTapped() {
		
		
	}
}
