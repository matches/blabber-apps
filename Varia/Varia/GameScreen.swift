//
//  GameScreen.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-21.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit
import RealmSwift

class GameScreen: GenericModal, UIScrollViewDelegate {
	
	var altTopBar : UIVisualEffectView!
	
	var topBar : UIView!
	var add : UIButton!
	var buy : UIButton!
	
	var cover : UIImageView!
	var scrollView : UIScrollView!
	
	var gradient1 : UIImageView!
	var gradient2 : UIView!
	
	var rect1 : UIView!
	var name : UITextView!
	var misc : UITextView!
	
	var rect2 : UIView!
	var releaseD : UILabel!
	var releaseIcon : UIImageView!
	var genre : UILabel!
	var genreIcon : UIImageView!
	var metascore : UILabel!
	var metascoreIcon : UILabel!
	
	var featuredMediaView : UIView!
	var videoPlayer : MPMoviePlayerController!
	var playButton : UIButton!
	var mediaViews : [UIImageView] = []
	var media : [Media] = []
	var mediaBG : UIView!
	
	var summary : UITextView!
	var readMore : UILabel!
	
	var rate : UIButton!
	
	var rawGame : Game!
	var gameID = 0
	
	func loadMetascore() {
		
		if self.rawGame.metascore.length > 0 {
			
			self.metascoreIcon.text = self.rawGame.metascore
		}
		else if let name = self.rawGame.name.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.letterCharacterSet()) {
			
			let url = "http://www.metacritic.com/search/game/\(name)/results"
			
			let request = NSMutableURLRequest(URL: NSURL(string: url)!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 6)
			request.HTTPMethod = "GET"
			
			let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
				help.APPDELEGATE.activeMediaDownloads.removeValueForKey(url)
				
				if response != nil {
					
					let tempResponse = response as! NSHTTPURLResponse?
					
					if tempResponse?.statusCode == 200 && data != nil {
						
						dispatch_async(dispatch_get_main_queue(), {
							
							let rawHTML = NSString(data: data!, encoding: NSUTF8StringEncoding)
							
							if let tagRange = rawHTML?.rangeOfString("span class=\"metascore") {
								
								if tagRange.location < rawHTML?.length {
									
									
									if (rawHTML!.substringFromIndex(tagRange.location) as NSString).rangeOfString(">").length > 0 {
										
										let metascoreStart = (rawHTML!.substringFromIndex(tagRange.location) as NSString).rangeOfString(">").location.advancedBy(1)
										
										var metascore = ((rawHTML!.substringFromIndex(tagRange.location) as NSString).substringFromIndex(metascoreStart) as NSString).substringToIndex((rawHTML!.substringFromIndex(tagRange.location) as NSString).rangeOfString("<").location)
										metascore = metascore.substringToIndex(metascore.rangeOfString("<")!.startIndex)
										
										print(metascore)
										self.metascoreIcon.text = metascore
									}
								}
							}
						})
					}
					else {
						
					}
				}
				else {
					if error != nil {
						print(error)
					}
				}
			})
			
			help.APPDELEGATE.activeMediaDownloads[url] = task
			
			UIApplication.sharedApplication().networkActivityIndicatorVisible = true
			task.resume()
		}
	}
	
	func refreshAddButton() {
		
		if self.rawGame.addedat != nil && self.rawGame.state.length > 0 {
			
			if self.rawGame.state == "played" {
				
				self.add.backgroundColor = hexToColor("67d12d")
				self.add.setImage(UIImage(named: "littleCheck"), forState: .Normal)
			}
			else {
				
				self.add.backgroundColor = help.hardBlue
				self.add.setImage(UIImage(named: "clock"), forState: .Normal)
			}
			
			self.add.setAttributedTitle(NSAttributedString(string: self.rawGame.state.uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(12, weight: UIFontWeightBold), NSKernAttributeName:0.5]), forState: .Normal)
		}
		else {
			
			self.add.backgroundColor = .whiteColor()
			self.add.setImage(nil, forState: .Normal)
			self.add.setAttributedTitle(NSAttributedString(string: "+ ADD TO GAMES", attributes: [NSForegroundColorAttributeName:hexToColor("1869a8")!, NSFontAttributeName:UIFont.systemFontOfSize(12, weight: UIFontWeightBold), NSKernAttributeName:0.5]), forState: .Normal)
		}
	}
	
	func addAction(action: String?) {
		
		let d = ["game":self.gameID] as NSMutableDictionary
		if action != nil { d.setValue(action, forKey: "override") }
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("changeAdd", [creds, d])(timeoutAfter: 3, callback: { data in
				print(data)
				
				if let game = data[safe: 0]?["game"] as? NSDictionary {
					
					self.rawGame = Game(obj: game)
					
					if let r3 = try? Realm() {
					
						_ = try? r3.write({
						
							r3.add(self.rawGame, update: true)
						})
						
						self.refreshAddButton()
						self.refreshRate()
						countdownVC?.reloadContent()
						gamesVC?.reloadContent()
						profileVC?.reloadContent()
					}
				}
			})
		}
	}
	
	func changeAdd() {
		
		self.addAction(nil)
		self.closeMark()
	}
	
	var markView : UIView!
	var markClose : UIButton!
	var markBacklog : UIButton!
	var markPlayed : UIButton!
	var markBacklogL : UILabel!
	var markPlayedL : UILabel!
	var markWishList : UIButton!
	var markWishListL : UILabel!
	
	func addPlayed() {
		
		self.addAction("played")
		self.closeMark()
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			self.openRating()
		}
	}
	
	func addBacklog() {
		
		self.addAction("backlog")
		self.closeMark()
	}
	
	func addWish() {
		
		self.addAction("wishlist")
		self.closeMark()
	}
	
	func removeGame() {
		
		self.addAction("remove")
		self.closeMark()
	}
	
	func openMark() {
		
		if self.rawGame.releasedate?.compare(NSDate()) == .OrderedDescending {
			
			self.changeAdd()
			return
		}
		
		var c1 = CGFloat(10); if help.APPDELEGATE.window?.frame.width > 320 { c1 = 18 }
		var c2 = CGFloat(25); if help.APPDELEGATE.window?.frame.width > 320 { c2 = 40 }
		
		self.markView = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.markView.backgroundColor = self.backgroundColor?.colorWithAlphaComponent(0.9)//UIColor(white: 0.1, alpha: 0.9)//UIColor(white: 0.94, alpha: 0.98)
		self.addSubview(self.markView)
		
		let l = UILabel(frame: CGRect(x: 0, y: self.height*0.30, width: self.width, height: 30))
		l.attributedText = NSAttributedString(string: "Add game", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(24, weight: UIFontWeightRegular)])
		l.textAlignment = .Center
		self.markView.addSubview(l)
		
		self.markClose = UIButton(frame: CGRect(x: (0)+c2, y: self.height*0.4, width: 60, height: 60))
		self.markClose.backgroundColor = .whiteColor()
		self.markClose.setImage(UIImage(named: "close")?.fillWithColor(UIColor(white: 0, alpha: 0.5)), forState: .Normal)
		self.markClose.imageView?.contentMode = .Center
		self.markClose.layer.masksToBounds = true
		self.markClose.layer.cornerRadius = 30
		self.markClose.layer.borderWidth = 6
		self.markClose.layer.borderColor = self.markClose.backgroundColor?.CGColor
		self.markClose.addTarget(self, action: #selector(closeMark), forControlEvents: .TouchDown)
		self.markView.addSubview(self.markClose)
		
		self.markBacklog = UIButton(frame: CGRect(x: ((self.width/4)-(c1))+c2, y: self.height*0.4, width: 60, height: 60))
		self.markBacklog.backgroundColor = help.babyBlue
		self.markBacklog.setImage(UIImage(named: "markBacklog"), forState: .Normal)
		self.markBacklog.imageView?.contentMode = .Center
		self.markBacklog.layer.masksToBounds = true
		self.markBacklog.layer.cornerRadius = 30
		self.markBacklog.layer.borderWidth = 6
		self.markBacklog.layer.borderColor = self.markBacklog.backgroundColor?.CGColor
		self.markBacklog.addTarget(self, action: #selector(addBacklog), forControlEvents: .TouchUpInside)
		self.markView.addSubview(self.markBacklog)
		
		self.markBacklogL = UILabel(frame: CGRect(x: ((self.width/4)-(c1))+c2, y: self.height*0.4+82, width: 60, height: 30))
		self.markBacklogL.attributedText = NSAttributedString(string: "Backlog", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
		self.markBacklogL.textAlignment = .Center
		self.markView.addSubview(self.markBacklogL)
		
		self.markPlayed = UIButton(frame: CGRect(x: (((self.width/4)*2)-(c1*2))+c2, y: self.height*0.4, width: 60, height: 60))
		self.markPlayed.backgroundColor = hexToColor("67d12d")
		self.markPlayed.setImage(UIImage(named: "markPlayed"), forState: .Normal)
		self.markPlayed.layer.masksToBounds = true
		self.markPlayed.layer.cornerRadius = 30
		self.markPlayed.layer.borderWidth = 6
		self.markPlayed.layer.borderColor = self.markPlayed.backgroundColor?.CGColor
		self.markPlayed.addTarget(self, action: #selector(addPlayed), forControlEvents: .TouchUpInside)
		self.markView.addSubview(self.markPlayed)
		
		self.markPlayedL = UILabel(frame: CGRect(x: (((self.width/4)*2)-(c1*2))+c2, y: self.height*0.4+82, width: 60, height: 30))
		self.markPlayedL.attributedText = NSAttributedString(string: "Played", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
		self.markPlayedL.textAlignment = .Center
		self.markView.addSubview(self.markPlayedL)
		
		self.markWishList = UIButton(frame: CGRect(x: (((self.width/4)*3)-(c1*3))+c2, y: self.height*0.4, width: 60, height: 60))
		self.markWishList.backgroundColor = hexToColor("ff9000")
		self.markWishList.setImage(UIImage(named: "markWish"), forState: .Normal)
		self.markWishList.layer.masksToBounds = true
		self.markWishList.layer.cornerRadius = 30
		self.markWishList.layer.borderWidth = 6
		self.markWishList.layer.borderColor = self.markWishList.backgroundColor?.CGColor
		self.markWishList.addTarget(self, action: #selector(addWish), forControlEvents: .TouchUpInside)
		self.markView.addSubview(self.markWishList)
		
		self.markWishListL = UILabel(frame: CGRect(x: (((self.width/4)*3)-(c1*3))+c2, y: self.height*0.4+82, width: 60, height: 30))
		self.markWishListL.attributedText = NSAttributedString(string: "Wishlist", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
		self.markWishListL.textAlignment = .Center
		self.markView.addSubview(self.markWishListL)
		
		if self.rawGame.state == "backlog" {
			
			self.markBacklog.backgroundColor = hexToColor("ff4848")
			self.markBacklog.setImage(UIImage(named: "close"), forState: .Normal)
			self.markBacklog.setImage(UIImage(named: "close"), forState: .Highlighted)
			self.markBacklogL.attributedText = NSAttributedString(string: "Remove", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
			self.markBacklog.layer.borderColor = self.markBacklog.backgroundColor?.CGColor
			self.markBacklog.removeTarget(self, action: #selector(changeAdd), forControlEvents: .TouchUpInside)
			self.markBacklog.addTarget(self, action: #selector(removeGame), forControlEvents: .TouchUpInside)
			
		}
		else if self.rawGame.state == "played" {
			
			self.markPlayed.backgroundColor = hexToColor("ff4848")
			self.markPlayed.setImage(UIImage(named: "close"), forState: .Normal)
			self.markPlayed.setImage(UIImage(named: "close"), forState: .Highlighted)
			self.markPlayedL.attributedText = NSAttributedString(string: "Remove", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
			self.markPlayed.layer.borderColor = self.markPlayed.backgroundColor?.CGColor
			self.markPlayed.removeTarget(self, action: #selector(addPlayed), forControlEvents: .TouchUpInside)
			self.markPlayed.addTarget(self, action: #selector(removeGame), forControlEvents: .TouchUpInside)
		}
		else if self.rawGame.state == "wishlist" {
			
			self.markWishList.backgroundColor = hexToColor("ff4848")
			self.markWishList.setImage(UIImage(named: "close"), forState: .Normal)
			self.markWishList.setImage(UIImage(named: "close"), forState: .Highlighted)
			self.markWishListL.attributedText = NSAttributedString(string: "Remove", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
			self.markWishList.layer.borderColor = self.markWishList.backgroundColor?.CGColor
			self.markWishList.removeTarget(self, action: #selector(addWish), forControlEvents: .TouchUpInside)
			self.markWishList.addTarget(self, action: #selector(removeGame), forControlEvents: .TouchUpInside)
		}
		
		if self.rawGame.addedat != nil {

			l.attributedText = NSAttributedString(string: "Edit game", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(24, weight: UIFontWeightRegular)])
		}
		
		self.markView.alpha = 0
		self.markClose.alpha = 0
		self.markBacklog.alpha = 0
		self.markPlayed.alpha = 0
		self.markBacklogL.alpha = 0
		self.markPlayedL.alpha = 0
		self.markWishList.alpha = 0
		self.markWishListL.alpha = 0
		self.markClose.y += 600
		self.markBacklog.y += 600
		self.markPlayed.y += 600
		self.markWishList.y += 600
		
		UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut.union(.AllowUserInteraction), animations: {
			
			self.markView.alpha = 1
			
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.15, usingSpringWithDamping: 0.75, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseInOut.union(.AllowUserInteraction), animations: {
			
			self.markClose.alpha = 1
			self.markClose.y -= 600
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.2, usingSpringWithDamping: 0.75, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseInOut.union(.AllowUserInteraction), animations: {
			
			self.markBacklogL.alpha = 1
			self.markBacklog.alpha = 1
			self.markBacklog.y -= 600
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.3, usingSpringWithDamping: 0.75, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseInOut.union(.AllowUserInteraction), animations: {
			
			self.markPlayedL.alpha = 1
			self.markPlayed.alpha = 1
			self.markPlayed.y -= 600
			
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.6, usingSpringWithDamping: 0.75, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseInOut.union(.AllowUserInteraction), animations: {
			
			self.markWishListL.alpha = 1
			self.markWishList.alpha = 1
			self.markWishList.y -= 600
			
			}, completion: nil)
	}
	
	func closeMark() {
		
		UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations: {
			
			self.markView?.alpha = 0
			
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
			
			self.markClose?.alpha = 0
			self.markClose?.y += 600
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
			
			self.markBacklog?.alpha = 0
			self.markBacklog?.y += 600
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
			
			self.markPlayed?.alpha = 0
			self.markPlayed?.y += 600
			
			}, completion: nil)
		
		UIView.animateWithDuration(0.6, delay: 0.3, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
			
			self.markWishList?.alpha = 0
			self.markWishList?.y += 600
			
			}, completion: nil)
	}
	
	func loadVideo(vid: String) {
		
		XCDYouTubeClient.defaultClient().getVideoWithIdentifier(vid, completionHandler: { (video, error) -> Void in
			
			if video != nil {
				
				let vObj = video!
				var link : NSURL?
				
				if vObj.streamURLs[22] as? NSURL != nil {
					link = vObj.streamURLs[22] as? NSURL
				}
				else if vObj.streamURLs[134] as? NSURL != nil {
					link = vObj.streamURLs[134] as? NSURL
				}
				else {
					//   link = vObj.streamURLs.first as? NSURL
				}
				
				self.videoPlayer.view.alpha = 1
				self.videoPlayer.contentURL = link
				
				let downloadButtonImage : (url: NSURL)->() = { url in
					
					// are you fucking kidding me. --__--
					
					let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 15)
					request.HTTPMethod = "GET"
					
					let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
						
						UIApplication.sharedApplication().networkActivityIndicatorVisible = false
						help.APPDELEGATE.activeMediaDownloads.removeValueForKey(url.absoluteString!)
						
						if response != nil {
							
							let tempResponse = response as! NSHTTPURLResponse?
							
							if tempResponse?.statusCode == 200 && data != nil {
								
								let img = UIImage(data: data!)
								
								dispatch_async(dispatch_get_main_queue(), {
									if img != nil && self.alpha > 0 {
										
										self.playButton.setBackgroundImage(img, forState: .Normal)
									}
								})
							}
							else {
								
							}
						}
						else {
							if error != nil {
								print(error)
							}
						}
					})
					
					help.APPDELEGATE.activeMediaDownloads[url.absoluteString!] = task
					
					UIApplication.sharedApplication().networkActivityIndicatorVisible = true
					task.resume()
				}
				
				self.playButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.videoPlayer.view.width, height: self.videoPlayer.view.height))
				self.playButton.addTarget(self, action: #selector(self.playPlayer), forControlEvents: .TouchUpInside)
				self.playButton.setImage(UIImage(named: "playButton"), forState: .Normal)
				self.playButton.setImage(UIImage(named: "playButton")?.fillWithColor(UIColor(white: 1, alpha: 0.6)), forState: .Highlighted)
				self.featuredMediaView.addSubview(self.playButton)
				
				if vObj.mediumThumbnailURL != nil {
					
					downloadButtonImage(url: vObj.mediumThumbnailURL!)
				}
				else if vObj.largeThumbnailURL != nil {
					
					downloadButtonImage(url: vObj.largeThumbnailURL!)
				}
				else if vObj.smallThumbnailURL != nil {
					
					downloadButtonImage(url: vObj.smallThumbnailURL!)
				}
			}
			else {
				print(error)
			}
		})
	}
	
	func playPlayer() {
		
		UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {

			self.playButton.alpha = 0

			}, completion: nil)
		
		self.videoPlayer.play()
	}
	
	func loadCover() {
		
		let fillUIForCover : ((img: UIImage, ad: Double) -> ()) = { img, ad in
			
			let c = img.areaAverage().getRGB()
			
			var newR = c[0]; /*newR = min((newR*4)-1.2, 1.0); if newR > 0.9 {*/ newR -= 0.3 //}
			var newG = c[1]; /*newG = min((newG*4)-1.2, 1.0); if newG > 0.9 {*/ newG -= 0.3 //}
			var newB = c[2]; /*newB = min((newB*4)-1.2, 1.0); if newB > 0.9 {*/ newB -= 0.3 //}
			
			let color = UIColor(red: newR, green: newG, blue: newB, alpha: 1)
			
			self.cover.image = img
			let nh = img.size.height*(self.width/img.size.width)
			if nh < 300 { self.cover.contentMode = .ScaleAspectFill }
			else { self.cover.y = -(((self.cover.height-nh)/2)+0) }
			
			
			self.gradient1.contentMode = .ScaleToFill
			self.gradient1.image = UIImage(named: "gameGradient")?.fillWithColor(color)
			
			var rect1bg = color.darkerByPercentage(15).colorWithAlphaComponent(0.9)
			if rect1bg.getRGB()[0] + rect1bg.getRGB()[1] + rect1bg.getRGB()[2] < 1 { rect1bg = color.lighterByPercentage(15).colorWithAlphaComponent(0.9) }
			
			
			self.cover.alpha = 0
			self.gradient1.alpha = 0
			
			UIView.animateWithDuration(ad, delay: 0, options: .CurveEaseInOut, animations: {
				
				self.cover.alpha = 1
				self.gradient1.alpha = 1
				self.backgroundColor = color
				self.gradient2.alpha = 1
				self.gradient2.backgroundColor = color
				self.rect1.backgroundColor = rect1bg
				self.altTopBar.backgroundColor = rect1bg.colorWithAlphaComponent(0.4)
				
				}, completion: nil)
		}
		
		let coverurl = self.rawGame.coverurl
		
		let r1 = try? Realm()
		
		let o = r1?.objects(Media.self).filter("type = \'cover\' && src = \'\(coverurl)\' && game = \(self.gameID)")
		
		if let data = o?.first?.data {
			
			if let img = UIImage(data: data) {
				
				fillUIForCover(img: img, ad: 0)
			}
		}
		else if let url = NSURL(string: coverurl) {
			
			let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 15)
			request.HTTPMethod = "GET"
			
			let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
				help.APPDELEGATE.activeMediaDownloads.removeValueForKey(coverurl)
				
				if response != nil {
					
					let tempResponse = response as! NSHTTPURLResponse?
					
					if tempResponse?.statusCode == 200 && data != nil {
						
						//dispatch_async(realmBGQueue, {
							
						let img = UIImage(data: data!)
						
						dispatch_async(dispatch_get_main_queue(), {
							
							if img != nil && self.alpha > 0 {
								
								if let r2 = try? Realm() {
									
									let mediaObj = Media()
									mediaObj.data = data
									mediaObj.src = coverurl
									mediaObj.type = "cover"
									mediaObj.game = self.gameID
									
									_ = try? r2.write({
										
										r2.add(mediaObj, update: true)
									})
										
									fillUIForCover(img: img!, ad: 0.3)
								}
							}
							else {
								
							}
						})
					}
				}
				else {
					if error != nil {
						print(error)
					}
				}
			})
			
			help.APPDELEGATE.activeMediaDownloads[coverurl] = task
			
			UIApplication.sharedApplication().networkActivityIndicatorVisible = true
			task.resume()
		}
	}
	
	init(game: AnyObject, owner: GenericViewController, frame: CGRect) {
		super.init(owner: owner, frame: frame)
		
		self.clipsToBounds = true
		
		if game.isKindOfClass(Game.self) { self.rawGame = game as! Game }
		else { self.rawGame = Game(obj: game as! NSDictionary) }
		
		self.gameID = self.rawGame.id
		
		self.backgroundColor = help.babyBlue.darkerByPercentage(6)
		
		self.cover = UIImageView(frame: CGRect(x: 0, y: 0, width: self.width, height: 700))
		self.cover.contentMode = .ScaleAspectFit
		self.cover.alpha = 0.8
		self.addSubview(self.cover)
		
		self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.scrollView.delegate = self
		self.scrollView.contentSize.height = self.height
		self.scrollView.backgroundColor = .clearColor()
		self.scrollView.scrollIndicatorInsets.bottom = 0
		self.addSubview(self.scrollView)
		
		
		self.gradient1 = UIImageView(frame: CGRect(x: 0, y: self.height*0.40, width: self.width, height: 120))
		self.scrollView.addSubview(self.gradient1)
		
		self.gradient2 = UIView(frame: CGRect(x: 0, y: (self.height*0.4)+120, width: self.width, height: self.scrollView.contentSize.height-(120+(self.height*0.4))))
		self.scrollView.addSubview(self.gradient2)
		
		self.rect1 = UIView(frame: CGRect(x: 22, y: self.height*0.55, width: self.width-44, height: 6))
		self.rect1.backgroundColor = help.babyBlue.darkerByPercentage(12)
		self.rect1.layer.masksToBounds = true
		self.rect1.layer.cornerRadius = 8
		self.scrollView.addSubview(self.rect1)
		
		self.name = UITextView(frame: CGRect(x: 0, y: 6, width: self.rect1.width-20, height: 36))
		self.name.userInteractionEnabled = false
		self.name.attributedText = NSAttributedString(string: self.rawGame.name, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(25, weight: UIFontWeightBold), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)])
		self.name.sizeToFit()
		self.name.height = ceil(self.name.height)
		self.name.layoutIfNeeded()
		self.name.x = self.rect1.width/2-self.name.width/2
		self.name.backgroundColor = .clearColor()
		self.rect1.addSubview(self.name)
		self.rect1.height += self.name.height
		
		self.misc = UITextView(frame: CGRect(x: 0, y: 6+self.name.height-12, width: self.rect1.width, height: 40))
		self.misc.userInteractionEnabled = false
		let attStr = NSMutableAttributedString(string: self.rawGame.platforms+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6), NSFontAttributeName:UIFont.systemFontOfSize(13, weight: UIFontWeightBold), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)])
		attStr.appendAttributedString(NSAttributedString(string: self.rawGame.companies.uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.6), NSFontAttributeName:UIFont.systemFontOfSize(12, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center),NSKernAttributeName:0.5]))
		self.misc.attributedText = attStr
		self.misc.backgroundColor = .clearColor()
		self.misc.sizeToFit()
		self.misc.layoutIfNeeded()
		self.misc.x = self.rect1.width/2-self.misc.width/2
		self.rect1.addSubview(self.misc)
		self.rect1.height += self.misc.height; if self.misc.height < 10 { self.rect1.height -= 20 }
		
		self.scrollView.contentSize.height = (self.height*0.5)+self.rect1.height
		
		self.rect1.height = ceil(self.rect1.height)
		
		self.rect2 = UIView(frame: CGRect(x: 15, y: self.rect1.y+self.rect1.height-6, width: self.width-30, height: 0))
		self.rect2.backgroundColor = .whiteColor()
		self.rect2.layer.masksToBounds = true
		self.rect2.layer.cornerRadius = 8
		self.scrollView.addSubview(self.rect2)
		
		//Rect 2 contents
		
		let stats = UIView(frame: CGRect(x: 0, y: 0, width: self.rect2.width, height: 100))
		self.rect2.addSubview(stats)
		
		self.releaseD = UILabel(frame: CGRect(x: 0, y: 68, width: self.rect2.width/3, height: 25))
		self.releaseD.textAlignment = .Center
		self.releaseD.font = UIFont.systemFontOfSize(14.8, weight: UIFontWeightBold)
		self.releaseD.textColor = help.textBlueDark
		self.rect2.addSubview(self.releaseD)
		
		if self.rawGame.releasedate != nil {
			
			let year = NSCalendar.currentCalendar().component(.Year, fromDate: self.rawGame.releasedate!)
			let day = NSCalendar.currentCalendar().component(.Day, fromDate: self.rawGame.releasedate!)
			let mo = NSCalendar.currentCalendar().component(.Month, fromDate: self.rawGame.releasedate!)
			let month = (NSDateFormatter().monthSymbols[mo-1] as NSString).substringToIndex(3)
			
			let d = "\(month) \(day) \(year)"
			self.releaseD.text = d
		}
		
		self.releaseIcon = UIImageView(frame: CGRect(x: 0, y: 8, width: self.rect2.width/3, height: 60))
		self.releaseIcon.image = UIImage(named: "statsCalendar")
		self.releaseIcon.contentMode = .Center
		self.rect2.addSubview(self.releaseIcon)
		
		self.genre = UILabel(frame: CGRect(x: self.rect2.width/3, y: 68, width: self.rect2.width/3, height: 25))
		self.genre.textAlignment = .Center
		self.genre.font = UIFont.systemFontOfSize(14.8, weight: UIFontWeightBold)
		self.genre.textColor = help.textBlueDark
		self.rect2.addSubview(self.genre)
		
		self.genreIcon = UIImageView(frame: CGRect(x: self.rect2.width/3, y: 8, width: self.rect2.width/3, height: 60))
		self.genreIcon.contentMode = .Center
		self.rect2.addSubview(self.genreIcon)
		
		if self.rawGame.genre.length > 0 {
			
			self.genre.text = self.rawGame.genre
			self.genreIcon.image = UIImage(named: self.rawGame.genre.lowercaseString)
		}
		
		self.metascore = UILabel(frame: CGRect(x: (self.rect2.width/3)*2, y: 68, width: self.rect2.width/3, height: 25))
		self.metascore.textAlignment = .Center
		self.metascore.font = UIFont.systemFontOfSize(14.5, weight: UIFontWeightBold)
		self.metascore.textColor = help.textBlueDark
		self.rect2.addSubview(self.metascore)
		self.metascore.text = "Metascore"
		
		self.metascoreIcon = UILabel(frame: CGRect(x: (self.rect2.width/6)+((self.rect2.width/3)*2)-25, y: 14, width: 50, height: 50))
		self.metascoreIcon.layer.cornerRadius = 25
		self.metascoreIcon.layer.masksToBounds = true
		self.metascoreIcon.layer.borderWidth = 4
		self.metascoreIcon.layer.borderColor = hexToColor("a3acbf")?.CGColor
		self.metascoreIcon.textAlignment = .Center
		self.metascoreIcon.font = UIFont.systemFontOfSize(17, weight: UIFontWeightBold)
		self.metascoreIcon.textColor = hexToColor("a3acbf")?.darkerByPercentage(5)
		self.rect2.addSubview(self.metascoreIcon)
		
		if self.rawGame.metascore.length > 0 {
			
			self.metascoreIcon.text = self.rawGame.metascore
		}
		else {
			self.metascoreIcon.text = "-"
		}
		
		self.rect2.height += 100
		
		self.featuredMediaView = UIView(frame: CGRect(x: 0, y: 100, width: self.rect2.width, height: 200))
		self.featuredMediaView.backgroundColor = hexToColor("212939")
		self.featuredMediaView.contentMode = .ScaleAspectFill
		self.featuredMediaView.clipsToBounds = true
		self.rect2.addSubview(self.featuredMediaView)
		self.rect2.height += 200
		
		self.videoPlayer = MPMoviePlayerController()
		self.videoPlayer.view.frame = CGRect(x: 0, y: 0, width: self.rect2.width, height: 200)
		self.videoPlayer.view.backgroundColor = .clearColor()
		self.videoPlayer.view.alpha = 0
		self.featuredMediaView.addSubview(self.videoPlayer.view)
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didEnterFullscreen), name: MPMoviePlayerDidEnterFullscreenNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didExitFullscreen), name: MPMoviePlayerDidExitFullscreenNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didExitFullscreen), name: MPMoviePlayerWillExitFullscreenNotification, object: nil)
		
		if let ssa = try? NSJSONSerialization.JSONObjectWithData( self.rawGame.screenshots.dataUsingEncoding(NSUTF8StringEncoding)! , options: .MutableContainers) as? [String] {
			
			self.rect2.height += min(104*2, CGFloat(ceil(Float(ssa!.count)/2)*104))+15
			self.mediaBG = UIView(frame: CGRect(x: 0, y: 300, width: self.rect2.width, height: min(104*2, CGFloat(ceil(Float(ssa!.count)/2)*104))))
			self.mediaBG.backgroundColor = help.babyPurple.darkerByPercentage(15)
			self.rect2.addSubview(self.mediaBG)
			
			//self.media = ssa
			
			var xx = 0
			for _ in ssa! {
				
				var mediaView : UIImageView?
				let tempX = xx
					
				var xp = CGFloat(0); if tempX % 2 != 0 { xp = self.rect2.width/2 }
				var yp = CGFloat(0); if tempX > 1 { yp = CGFloat(floor(Float(tempX)/2)*104) }
				
				mediaView = UIImageView(frame: CGRect(x: xp, y: 100+200+yp, width: self.rect2.width/2, height: 104))
				mediaView?.clipsToBounds = true
				mediaView?.backgroundColor = hexToColor("212939")
				mediaView?.contentMode = .ScaleAspectFill
				mediaView?.tag = tempX
				mediaView?.userInteractionEnabled = true
				let r = UITapGestureRecognizer(target: self, action: #selector(openGallery(_:)))
				mediaView?.addGestureRecognizer(r)
				self.mediaViews.append(mediaView!)
				if xx < 4 { self.rect2.addSubview(mediaView!) }
				
				xx += 1
			}
				
				let r = try! Realm()
				
				var x = 0
				
				for s in ssa! {
					
					var mediaView : UIImageView?
					let tempX = x
					
					mediaView = self.mediaViews[safe: tempX]
						
					let o = r.objects(Media.self).filter("type = \'screenshot\' && src = \'\(s)\'")
					
					if let d = o.first?.data {
						
						let i = UIImage(data: d)
						
						if i != nil {
								
							mediaView?.image = i
						}
					}
					else if let url = NSURL(string: s) {
						
						let tempX = x
							
							let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 15)
							request.HTTPMethod = "GET"
							
							let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
								
								UIApplication.sharedApplication().networkActivityIndicatorVisible = false
								help.APPDELEGATE.activeMediaDownloads.removeValueForKey(s)
								
								if response != nil {
									
									let tempResponse = response as! NSHTTPURLResponse?
									
									if tempResponse?.statusCode == 200 && data != nil {
										
											
										let img = UIImage(data: data!)
										dispatch_async(dispatch_get_main_queue(), {
											
											if img != nil {
												
												if let r2 = try? Realm() {
													
													let mediaObj = Media()
													mediaObj.data = data
													mediaObj.src = s
													mediaObj.type = "screenshot"
													mediaObj.game = self.gameID
													mediaObj.index = tempX
													
													_ = try? r2.write({
														
														r2.add(mediaObj, update: true)
													})
														
													mediaView?.image = img
												}
											}
											else {
												
											}
										})
									}
								}
								else {
									if error != nil {
										print(error)
									}
								}
							})
							
							help.APPDELEGATE.activeMediaDownloads[s] = task
							
							UIApplication.sharedApplication().networkActivityIndicatorVisible = true
							task.resume()
					}
					
					x += 1
				}
		}
		
		if self.rawGame.summary.length > 0 {
			
			var extra = CGFloat(0); if self.featuredMediaView != nil { extra = self.featuredMediaView.height }
			
			self.summary = UITextView(frame: CGRect(x: 10, y: 100+extra+8, width: self.rect2.width-20, height: 200))
			self.summary.backgroundColor = .clearColor()
			self.summary.selectable = false
			self.summary.editable = false
			self.summary.scrollEnabled = false
			
			let attStr = NSMutableAttributedString(string: "Summary\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.4), NSFontAttributeName:UIFont.systemFontOfSize(22, weight: UIFontWeightBold), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(12, alignment: nil)])
			attStr.appendAttributedString(NSAttributedString(string: self.rawGame.summary+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.7), NSFontAttributeName:UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)]))
			self.summary.attributedText = attStr
			self.summary.sizeToFit()
			
			let oldHeight = self.summary.height
			self.summary.height = min(self.summary.height, 150)
			
			var ex = CGFloat(0)
			
			if oldHeight > 150 {
				
				ex = 40
				
				self.readMore = UILabel(frame: CGRect(x: 15, y: self.summary.y+self.summary.height-6, width: self.rect2.width-30, height: 20))
				self.readMore.text = "Tap to show more"
				self.readMore.font = UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)
				self.readMore.textColor = help.babyBlue
				self.rect2.addSubview(self.readMore)
				
				let r = UITapGestureRecognizer(target: self, action: #selector(openSummary))
				self.summary.addGestureRecognizer(r)
			}
			
			self.rect2.addSubview(self.summary)
			self.rect2.height += self.summary.height+ex
			
			self.mediaViews.forEach({ v in v.y += self.summary.height+ex })
			self.mediaBG?.y += self.summary.height+ex
		}
		
		self.rate = UIButton(frame: CGRect(x: 20, y: self.rect2.height+10, width: self.rect2.width-40, height: 50))
		self.rate.layer.cornerRadius = 8
		self.rate.layer.masksToBounds = true
		
		self.details = UITextView(frame: CGRect(x: 10, y: self.rect2.height, width: self.rect2.width-20, height: 200))
		self.details.backgroundColor = .clearColor()
		self.details.selectable = false
		self.details.editable = false
		self.details.scrollEnabled = false
		self.rect2.addSubview(self.details)
		
		//
		
		self.rect2.height = ceil(self.rect2.height)
		self.scrollView.contentSize.height += self.rect2.height
		self.scrollView.contentSize.height += 40
		
		self.refreshRate()
		
		//
		
		self.topBar = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: 64))
		self.topBar.clipsToBounds = false
		self.addSubview(self.topBar)
		
		let grad2 = UIImageView(frame: CGRect(x: 0, y: 0, width: self.width, height: 130))
		grad2.contentMode = .ScaleToFill
		grad2.image = UIImage(named: "gameGradient2")
		grad2.alpha = 0.5
		self.topBar.addSubview(grad2)
		
		let b = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 54))
		b.setImage(UIImage(named: "back")?.fillWithColor(UIColor.whiteColor()), forState: .Normal)
		b.addTarget(self.owner!, action: #selector(GenericViewController.hideLastModalAnimated), forControlEvents: .TouchDown)
		self.topBar.addSubview(b)
		
		self.add = UIButton(frame: CGRect(x: self.width-135-14, y: 30, width: 135, height: 34))
		self.add.backgroundColor = .whiteColor()
		self.add.layer.cornerRadius = 6
		self.add.layer.masksToBounds = true
		self.add.addTarget(self, action: #selector(self.openMark), forControlEvents: .TouchUpInside)
		self.topBar.addSubview(self.add)
		
		self.refreshAddButton()
		
		self.buy = UIButton(frame: CGRect(x: self.width-92-135-14, y: 30, width: 86, height: 34))
		self.buy.backgroundColor = help.yellow
		self.buy.setAttributedTitle(NSAttributedString(string: "Buy now".uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.blackColor(), NSFontAttributeName:UIFont.systemFontOfSize(12, weight: UIFontWeightBold), NSKernAttributeName:0.5]), forState: .Normal)
		self.buy.layer.cornerRadius = 6
		self.buy.layer.masksToBounds = true
		self.buy.addTarget(self, action: #selector(self.buyGame), forControlEvents: .TouchUpInside)
		self.topBar.addSubview(self.buy)
		
		//
		
		self.altTopBar = UIVisualEffectView(frame: CGRect(x: 0, y: -70, width: self.width, height: 70))
		self.altTopBar.backgroundColor = UIColor(white: 0, alpha: 0.4)
		self.altTopBar.effect = UIBlurEffect(style: .Light)
		self.altTopBar.layer.shadowColor = UIColor(white: 0, alpha: 0.5).CGColor
		self.altTopBar.layer.shadowRadius = 10
		self.altTopBar.layer.shadowOffset = CGSize(width: 0, height: 2)
		self.altTopBar.layer.shadowOpacity = 1
		self.addSubview(self.altTopBar)
		
		let b2 = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 54))
		b2.setImage(UIImage(named: "back")?.fillWithColor(UIColor.whiteColor()), forState: .Normal)
		b2.addTarget(self.owner!, action: #selector(GenericViewController.hideLastModalAnimated), forControlEvents: .TouchDown)
		self.altTopBar.addSubview(b2)
		
		let l = UILabel(frame: CGRect(x: 50, y: 25, width: self.width-100, height: 44))
		l.textAlignment = .Center
		l.font = UIFont.systemFontOfSize(18, weight: UIFontWeightBold)
		l.textColor = .whiteColor()
		l.text = self.rawGame.name
		self.altTopBar.addSubview(l)
		
		//
		
		self.loadCover()
		if self.rawGame.releasedate?.compare(NSDate()) == .OrderedAscending { self.loadMetascore() }
		
		//
		
		if let creds = User.getCredentials() {
			
			socket.emitWithAck("enteredGame", [creds, ["game":self.gameID]])(timeoutAfter: 3, callback: { data in
				print(data)
				
				if let game = data[safe: 0]?["game"] as? NSDictionary {
					
					dispatch_async(dispatch_get_main_queue(), {
						
						self.rawGameData = game
						self.rawGame = Game(obj: game)
						
						if let r2 = try? Realm() {
							_ = try? r2.write({
								
								r2.add(self.rawGame, update: true)
							})
						}
							
						self.refreshAddButton()
						self.refreshRate()
						self.refreshDetails()
					})
					
					if let vid = game.valueForKey("video_id") as? String {
						
						self.loadVideo(vid)
					}
					else {
						
						let l = UILabel(frame: CGRect(x: 0, y: self.featuredMediaView.height/2-15, width: self.featuredMediaView.width, height: 30))
						l.attributedText = NSAttributedString(string: "No video available.", attributes: [NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.5), NSFontAttributeName: UIFont.systemFontOfSize(16, weight: UIFontWeightRegular)])
						l.textAlignment = .Center
						self.featuredMediaView.addSubview(l)
					}
				}
			})
		}
	}
	
	func didEnterFullscreen() {
		
		help.APPDELEGATE.fullscreen = true
	}
	
	func didExitFullscreen() {
		
		help.APPDELEGATE.fullscreen = false
		let value = UIInterfaceOrientation.LandscapeLeft.rawValue
		UIDevice.currentDevice().setValue(value, forKey: "orientation")
	}
	
	var details : UITextView!
	var rawGameData : NSDictionary?
	
	func refreshDetails() {
		
		if let dd = ((self.rawGameData?.objectForKey("data") as? String)?.stringByRemovingPercentEncoding)?.dataUsingEncoding(NSUTF8StringEncoding) {
			
			if let data = try? (NSJSONSerialization.JSONObjectWithData(dd, options: .MutableContainers) as? NSDictionary)?.objectForKey("game") as? NSDictionary {
				
				let attStr4 = NSMutableAttributedString()
				
				let ac = self.backgroundColor!.lighterByPercentage(5)
				
				if let rds = data?.objectForKey("release_dates") as? [NSDictionary] {
					
					attStr4.appendAttributedString(NSAttributedString(string: "Release dates\n".uppercaseString, attributes: [NSForegroundColorAttributeName:ac, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(6, alignment: .Center), NSKernAttributeName: 0.5]))
					
					for x in rds {
						
						if let pn = x.valueForKey("platform_name") as? String {
							
							attStr4.appendAttributedString(NSAttributedString(
								attStr4.appendAttributedString(NSAttributedString(string: pn+": ", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(17, weight: UIFontWeightBold), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)]))))
							
							if let rd = x.valueForKey("release_date") as? String {
								
								attStr4.appendAttributedString(NSAttributedString(
									attStr4.appendAttributedString(NSAttributedString(string: rd+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(17, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)]))))
							}
						}
					}
				}
				
				if let gs = data?.objectForKey("genres") as? [NSDictionary] {
					
					attStr4.appendAttributedString(NSAttributedString(string: "\nGenres\n".uppercaseString, attributes: [NSForegroundColorAttributeName:ac, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(6, alignment: .Center), NSKernAttributeName: 0.5]))
					
					for x in gs {
						
						if let gn = x.valueForKey("name") as? String {
							
							attStr4.appendAttributedString(NSAttributedString(
								attStr4.appendAttributedString(NSAttributedString(string: gn+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(17, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)]))))
							
						}
					}
				}
				
				if let ts = data?.objectForKey("themes") as? [NSDictionary] {
					
					attStr4.appendAttributedString(NSAttributedString(string: "\nThemes\n".uppercaseString, attributes: [NSForegroundColorAttributeName:ac, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(6, alignment: .Center), NSKernAttributeName: 0.5]))
					
					for x in ts {
						
						if let tn = x.valueForKey("name") as? String {
							
							attStr4.appendAttributedString(NSAttributedString(
								attStr4.appendAttributedString(NSAttributedString(string: tn+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(17, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)]))))
							
						}
					}
				}
				
				if let cs = data?.objectForKey("companies") as? [NSDictionary] {
					
					attStr4.appendAttributedString(NSAttributedString(string: "\nCompanies\n".uppercaseString, attributes: [NSForegroundColorAttributeName:ac, NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(6, alignment: .Center), NSKernAttributeName: 0.5]))
					
					for x in cs {
						
						if let cn = x.valueForKey("name") as? String {
							
							attStr4.appendAttributedString(NSAttributedString(
								attStr4.appendAttributedString(NSAttributedString(string: cn+"\n", attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(17, weight: UIFontWeightRegular), NSParagraphStyleAttributeName: paragraphStyleWithLineHeight(nil, alignment: .Center)]))))
							
						}
					}
				}
				
				
				self.details.attributedText = attStr4
				self.details.layoutIfNeeded()
				self.details.sizeToFit()
				self.details.width = self.rect2.width-20
				
				self.rect2.height += self.details.height
				self.scrollView.contentSize.height += self.details.height
				
				self.rate?.y += self.details.height
			}
		}
	}
	
	func buyGame() {
		
		UIApplication.sharedApplication().openURL(NSURL(string: "https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=\((self.rawGame.name as NSString).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.letterCharacterSet())!.stringByReplacingOccurrencesOfString("%20", withString: "+"))")!)
	}
	
	var rv : UIView!
	
	func openRating() {
		
		self.rv = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		self.rv.backgroundColor = self.backgroundColor?.colorWithAlphaComponent(0.9)
		self.rv.alpha = 0
		self.addSubview(self.rv)
		
		let c = UIButton(frame: CGRect(x: 0, y: self.height/2+100, width: self.width, height: 50))
		c.setAttributedTitle(NSAttributedString(string: "Rate it later", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(20)]), forState: .Normal)
		c.addTarget(self, action: #selector(closeRating), forControlEvents: .TouchUpInside)
		self.rv.addSubview(c)
		
		let l = UILabel(frame: CGRect(x: 0, y: 20, width: self.width, height: 50))
		l.attributedText = NSAttributedString(string: "Rate this game?", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 1), NSFontAttributeName:UIFont.systemFontOfSize(22)])
		l.textAlignment = .Center
		self.rv.addSubview(l)
		
		let r = [1,2,3,4,5,6,7,8,9,10]
		for v in r {
			
			let rb = UIButton(frame: CGRect(x: (CGFloat(floor(Float(v-1)))*(self.width/5)), y: (self.height/2-60), width: self.width/5, height: 50))
			if v > 5 { rb.y += 70; rb.x -= 5*(self.width/5) }
			
			rb.setAttributedTitle(NSAttributedString(string: "\(v)", attributes: [NSForegroundColorAttributeName: UIColor(white: 1, alpha: 1), NSFontAttributeName: UIFont.systemFontOfSize(30, weight: UIFontWeightBold)]), forState: .Normal)
			rb.tag = v
			rb.addTarget(self, action: #selector(rate(_:)), forControlEvents: .TouchDown)
			self.rv.addSubview(rb)
		}
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.rv.alpha = 1
			
			}, completion: nil)
	}

	func refreshRate() {

		if self.rawGame.releasedate == nil {
			
			self.rate.backgroundColor = UIColor(white: 0.9, alpha: 1)
			
			let rs = "Rate this game when it's out!"
			
			self.rate.setAttributedTitle(NSAttributedString(string: rs, attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.4), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)]), forState: .Normal)
			self.rate.setAttributedTitle(nil, forState: .Highlighted)
		}
		else if self.rawGame.releasedate?.compare(NSDate()) == .OrderedDescending {
			
			self.rate.backgroundColor = UIColor(white: 0.9, alpha: 1)
			
			let year = NSCalendar.currentCalendar().component(.Year, fromDate: self.rawGame.releasedate!)
			let day = NSCalendar.currentCalendar().component(.Day, fromDate: self.rawGame.releasedate!)
			let mo = NSCalendar.currentCalendar().component(.Month, fromDate: self.rawGame.releasedate!)
			let month = (NSDateFormatter().monthSymbols[mo-1] as NSString).substringToIndex(3)
			
			let rs = "Rate this game on \(month) \(day) \(year)!"
			
			self.rate.setAttributedTitle(NSAttributedString(string: rs, attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.4), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)]), forState: .Normal)
			self.rate.setAttributedTitle(nil, forState: .Highlighted)
		}
		else if self.rawGame.addedat != nil {
			
			self.rate.backgroundColor = help.babyBlue
			
			var rs = "Rate this game"
			if self.rawGame.rating != 0 {
				
				rs = "\(self.rawGame.rating)/10 (Tap to edit)"
			}
			
			self.rate.setAttributedTitle(NSAttributedString(string: rs, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightBold)]), forState: .Normal)
			self.rate.setBackgroundImage(help.babyBlue.darkerByPercentage(10).toImage(), forState: .Highlighted)
			self.rate.addTarget(self, action: #selector(openRating), forControlEvents: .TouchUpInside)
		}
		else {
			
			self.rate.backgroundColor = UIColor(white: 0.9, alpha: 1)
			
			let rs = "Add this game to rate it!"
			
			self.rate.setAttributedTitle(NSAttributedString(string: rs, attributes: [NSForegroundColorAttributeName:UIColor(white: 0, alpha: 0.4), NSFontAttributeName:UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)]), forState: .Normal)
			self.rate.setAttributedTitle(nil, forState: .Highlighted)
			self.rate.removeTarget(self, action: #selector(openRating), forControlEvents: .TouchUpInside)
		}
		
		
		if self.rawGame.state == "played" && self.rawGame.reviewed == false && self.rate?.superview == nil {
			
			self.rect2.addSubview(self.rate)
			self.rect2.height += 80
			self.scrollView.contentSize.height += 80
		}
		else {
		}
	}
	
	func rate(b: UIButton) {
		
		self.rv?.subviews.forEach({ sv in
		
			if sv.isKindOfClass(UIButton.self) == true && sv.tag > 0 && sv.tag < 11 && sv.tag != b.tag {

				UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
					sv.alpha = 0.2
					}, completion: nil)
			}
		})
		
		b.setTitleColor(help.babyBlue, forState: .Normal)

		if let creds = User.getCredentials() {
			
			socket.emitWithAck("rateGame", [creds, ["game": self.gameID, "rating": b.tag]])(timeoutAfter: 5, callback: { data in
			
				if let game = data[safe: 0]?["game"] as? NSDictionary {
			
					self.rawGame = Game(obj: game)
					self.refreshRate()
				}
				else {

					showNotificationBar("Error rating game, try again!", type: .Error)
				}
			})
		}
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			self.closeRating()
		}
	}
	
	func closeRating() {
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.rv?.alpha = 0
			
			}, completion: { f in self.rv?.removeFromSuperview() })
	}
	
	func openGallery(r : UITapGestureRecognizer) {
		
		let gallery = Gallery(game: self.gameID, frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
		help.APPDELEGATE.tabBarController.view.addSubview(gallery)
		gallery.goToImage(r.view!.tag)
		
		gallery.alpha = 0
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			gallery.alpha = 1
			
			}, completion: nil)
		
		help.APPDELEGATE.tabBarController.statusBarHidden = true
		help.APPDELEGATE.tabBarController.setNeedsStatusBarAppearanceUpdate(0.15)
	}
	
	func openSummary() {
		
		self.summary.userInteractionEnabled = false
		
		let oldHeight = self.summary.height
		
		UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
			
			self.summary.sizeToFit()
			let d = self.summary.height - oldHeight - 40
			
			self.readMore.y += d
			self.mediaBG?.y += d
			self.mediaViews.forEach({ v in v.y += d })
			self.rate.y += d
			
			self.details.y += d
			
			self.rect2.height += d
			self.scrollView.contentSize.height += d
			
			self.readMore.alpha = 0
			
			}, completion: nil)
	}
	
	func scrollViewDidScroll(scrollView: UIScrollView) {
		
		if scrollView.contentOffset.y > self.height*0.45 {
			
			UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
				
				self.topBar.y = -64
				
				}, completion: { f in
			})
			
			UIView.animateWithDuration(0.3, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
				
				self.altTopBar.y = -6
				
				}, completion: nil)
		}
		else {
			
			UIView.animateWithDuration(0.3, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
				
				self.topBar.y = 0
				
				}, completion: { f in
			})
			
			UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
				
				self.altTopBar.y = -70
				
				}, completion: nil)
		}
		
		if scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < scrollView.contentSize.height-self.height {
			
			self.cover.alpha = 0.8 - 0.8*((scrollView.contentOffset.y)/250)
			
		}
		else if scrollView.contentOffset.y >= scrollView.contentSize.height-self.height {
			
			self.cover.alpha = 0
		}
		else {
			
			self.cover.alpha = 0.8 + 0.2*((-scrollView.contentOffset.y)/150)
			self.cover.height = 700 + -scrollView.contentOffset.y
			self.cover.width = self.width + -scrollView.contentOffset.y
			self.cover.x = -((self.cover.width-self.width)/2)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
