//
//  Calendar.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Calendar: UIView {
	
	var bg : UIView!
	
	var leftButton : UIButton!
	var rightButton : UIButton!
	
	var year:UILabel!
	
	var months:[UIButton] = []
	var pickedMonth : Int!
	var pickedYear : Int!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.backgroundColor = UIColor(white: 0, alpha: 0.6)
		
		self.bg = UIView(frame: CGRect(x: self.width*0.12, y: self.height/2-160, width: self.width*0.76, height: 260))
		self.bg.backgroundColor = UIColor(white: 0.97, alpha: 0.97)
		self.bg.layer.masksToBounds = true
		self.bg.layer.cornerRadius = 10
		self.addSubview(self.bg)
		
		self.year = UILabel(frame: CGRect(x: 0, y: 0, width: self.bg.width, height: 60))
		self.year.textAlignment = .Center
		self.year.font = UIFont.systemFontOfSize(24, weight: UIFontWeightBold)
		self.year.textColor = help.purple1.darkerByPercentage(5)
		self.bg.addSubview(self.year)
		
		self.leftButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 60))
		self.leftButton.setImage(UIImage(named: "left")?.fillWithColor(help.purple1), forState: .Normal)
		self.leftButton.setImage(UIImage(named: "left")?.fillWithColor(help.hardBlue), forState: .Highlighted)
		self.leftButton.addTarget(self, action: #selector(previousYear), forControlEvents: .TouchDown)
		self.leftButton.tag = 666
		self.bg.addSubview(self.leftButton)
		
		self.rightButton = UIButton(frame: CGRect(x: self.bg.width-50, y: 0, width: 50, height: 60))
		self.rightButton.setImage(UIImage(named: "right")?.fillWithColor(help.purple1), forState: .Normal)
		self.rightButton.setImage(UIImage(named: "right")?.fillWithColor(help.hardBlue), forState: .Highlighted)
		self.rightButton.addTarget(self, action: #selector(nextYear), forControlEvents: .TouchDown)
		self.rightButton.tag = 666
		self.bg.addSubview(self.rightButton)
		
		var m = 0
		
		let cellWidth = self.bg.width/4
		
		while m < 12 {
			
			let b = UIButton(frame: CGRect(x: 0, y: 0, width: cellWidth, height: cellWidth))
			b.setTitleColor(help.purple1, forState: .Normal)
			b.setTitleColor(help.hardBlue, forState: .Highlighted)
			b.tag = m
			
			if m == 0 {
				
				b.frame.origin = CGPoint(x: 0, y: 50)
				b.setTitle("Jan", forState: .Normal)
			}
			if m == 1 {
				
				b.frame.origin = CGPoint(x: cellWidth*1, y: (cellWidth*0)+50)
				b.setTitle("Feb", forState: .Normal)
			}
			if m == 2 {
				
				b.frame.origin = CGPoint(x: cellWidth*2, y: (cellWidth*0)+50)
				b.setTitle("Mar", forState: .Normal)
			}
			if m == 3 {
				
				b.frame.origin = CGPoint(x: cellWidth*3, y: (cellWidth*0)+50)
				b.setTitle("Apr", forState: .Normal)
			}
			if m == 4 {
				
				b.frame.origin = CGPoint(x: cellWidth*0, y: (cellWidth*1)+50)
				b.setTitle("May", forState: .Normal)
			}
			if m == 5 {
				
				b.frame.origin = CGPoint(x: cellWidth*1, y: (cellWidth*1)+50)
				b.setTitle("Jun", forState: .Normal)
			}
			if m == 6 {
				
				b.frame.origin = CGPoint(x: cellWidth*2, y: (cellWidth*1)+50)
				b.setTitle("Jul", forState: .Normal)
			}
			if m == 7 {
				
				b.frame.origin = CGPoint(x: cellWidth*3, y: (cellWidth*1)+50)
				b.setTitle("Aug", forState: .Normal)
			}
			if m == 8 {
				
				b.frame.origin = CGPoint(x: cellWidth*0, y: (cellWidth*2)+50)
				b.setTitle("Sep", forState: .Normal)
			}
			if m == 9 {
				
				b.frame.origin = CGPoint(x: cellWidth*1, y: (cellWidth*2)+50)
				b.setTitle("Oct", forState: .Normal)
			}
			if m == 10 {
				
				b.frame.origin = CGPoint(x: cellWidth*2, y: (cellWidth*2)+50)
				b.setTitle("Nov", forState: .Normal)
			}
			if m == 11 {
				
				b.frame.origin = CGPoint(x: cellWidth*3, y: (cellWidth*2)+50)
				b.setTitle("Dec", forState: .Normal)
			}
			
			b.titleLabel?.font = UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)
			
			self.bg.addSubview(b)
			
			m += 1
		}
		
		self.close()
	}
	
	func pickMonth(b: UIButton) {
		
		self.pickedMonth = b.tag
		self.pickedYear = Int(self.year.text!)!
		
		self.bg.subviews.forEach({ sv in
			
			if sv.isKindOfClass(UIButton.self) && (sv as? UIButton)?.tag != self.pickedMonth {
				UIView.animateWithDuration(0.15, animations: { sv.alpha = 0.4 })
				(sv as! UIButton).setTitleColor(help.purple1, forState: .Normal)
			
			} else if sv.isKindOfClass(UIButton.self) && (sv as? UIButton)?.tag == b.tag {
				(sv as! UIButton).setTitleColor(help.hardBlue, forState: .Normal)
			}
		})
		
		var mo = (self.pickedMonth+1).description; if self.pickedMonth+1 < 10 { mo = "0"+(self.pickedMonth+1).description }
		let newDate = help.APPDELEGATE.dateFormatter.dateFromString("\(self.pickedYear)-\(mo)-01 04:20:01")
		
		help.APPDELEGATE.tabBarController.exploreViewController.pickedMonthString = "\(self.pickedYear)-\(mo)-01 04:20:01"
		help.APPDELEGATE.tabBarController.exploreViewController.pickedMonth = newDate
		
		self.userInteractionEnabled = false
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			self.close()
			exploreVC?.refreshWhatsNew()
		}
	}
	
	func previousYear() {
		
		self.year.text = (Int(self.year.text!)!-1).description
		self.refreshMonths()
	}
	
	func nextYear() {
		
		self.year.text = (Int(self.year.text!)!+1).description
		self.refreshMonths()
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		super.touchesBegan(touches, withEvent: event)
		
		if touches.first?.locationInView(self).x < self.bg.x || touches.first?.locationInView(self).y < self.bg.y || touches.first?.locationInView(self).x > (self.bg.x + self.bg.width) || touches.first?.locationInView(self).y > (self.bg.y + self.bg.height) {
			
			self.close()
		}
	}
	
	func refreshMonths() {
		
		self.bg.subviews.forEach({ sv in sv.alpha = 1;
			
			/*if sv.isKindOfClass(UIButton.self) && sv.tag != 666 && ((sv.tag > exploreVC?.currentMonth && exploreVC?.currentYear == Int(self.year.text!)) || exploreVC?.currentYear < Int(self.year.text!)) {
				
				(sv as! UIButton).setTitleColor(UIColor(white: 0, alpha: 0.15), forState: .Normal)
				(sv as! UIButton).removeTarget(self, action: #selector(pickMonth(_:)), forControlEvents: .TouchUpInside)
			}
			else*/ if sv.tag == self.pickedMonth && sv.isKindOfClass(UIButton.self) && sv.tag != 666 && self.pickedYear == Int(self.year.text!) {
				
				(sv as! UIButton).setTitleColor(help.hardBlue, forState: .Normal)
				(sv as! UIButton).addTarget(self, action: #selector(pickMonth(_:)), forControlEvents: .TouchUpInside)
			
			} else if sv.isKindOfClass(UIButton.self) && sv.tag != 666 {
				
				(sv as! UIButton).setTitleColor(help.purple1, forState: .Normal)
				(sv as! UIButton).addTarget(self, action: #selector(pickMonth(_:)), forControlEvents: .TouchUpInside)
			}
		})
		
	}
	
	func open() {
		
		self.userInteractionEnabled = true
		
		self.pickedMonth = NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: help.APPDELEGATE.tabBarController.exploreViewController.pickedMonth)-1
		self.pickedYear = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: help.APPDELEGATE.tabBarController.exploreViewController.pickedMonth)
		
		self.year.text = self.pickedYear.description
		
		self.refreshMonths()
		
		UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseOut.union(.AllowUserInteraction), animations: {
			
			self.bg.layer.transform = CATransform3DMakeScale(1, 1, 1)
			self.alpha = 1
			
			}, completion: nil)
	}
	
	func close() {
		
		UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseOut.union(.AllowUserInteraction), animations: {
			
			self.bg.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
			self.alpha = 0
			
			}, completion: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
