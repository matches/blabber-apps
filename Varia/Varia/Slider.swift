//
//  Slider.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-16.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Slider: UIView, UIGestureRecognizerDelegate {
	
	var slideViews : [UIView] = []
	var slides: [NSDictionary] = []
	var current = 0
	var timer : NSTimer?
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.backgroundColor = UIColor(white: 0.08, alpha: 1)
		self.clipsToBounds = true
		
		self.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(nextSlide), userInfo: nil, repeats: false)
		
		let r = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
		r.delegate = self
		self.addGestureRecognizer(r)
	}
	
	override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
		
		let velocity = (gestureRecognizer as! UIPanGestureRecognizer).velocityInView(self)
		return fabs(velocity.y) < fabs(velocity.x);
	}
	
	var initial = CGFloat(0)
	var delta = CGFloat(0)
	var swiping = false
	
	func didPan(r: UIPanGestureRecognizer) {
		
		if r.state == .Began {
			
			self.delta = 0
			self.swiping = true
			self.timer?.invalidate()
			self.initial = r.locationInView(self).x
			
			self.slideViews[safe: self.current]?.x = 0
			self.slideViews[safe: self.current+1]?.x = self.width
			self.slideViews[safe: self.current-1]?.x = -self.width
			
			self.slideViews[safe: self.current]?.alpha = 1
			self.slideViews[safe: self.current+1]?.alpha = 1
			self.slideViews[safe: self.current-1]?.alpha = 1
		}
		else if r.state == .Changed {
			
			self.delta = initial - r.locationInView(self).x
			
			self.slideViews[safe: self.current]?.x = 0 - (delta)
			self.slideViews[safe: self.current+1]?.x = self.width - (delta)
			self.slideViews[safe: self.current-1]?.x = -self.width - (delta)
			
			if self.slideViews[safe: self.current-1] == nil && self.delta < 0 {
				
				self.slideViews[safe: self.current]?.x = 0 - (delta/2)
			}
			else if self.slideViews[safe: self.current+1] == nil && self.delta > 0 {
				
				self.slideViews[safe: self.current]?.x = 0 - (delta/2)
			}
		}
		else if r.state == .Cancelled {
			
			self.resetSlidePositions()
			
			self.swiping = false
			self.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(nextSlide), userInfo: nil, repeats: false)
		}
		else if r.state == .Ended {
			
			if self.slideViews[safe: self.current-1] == nil && self.delta < 0 {
				
				//self.slideViews[safe: self.current]?.x = 0 - (delta/2)
			}
			else if self.slideViews[safe: self.current+1] == nil && self.delta > 0 {
				
				//self.slideViews[safe: self.current]?.x = 0 - (delta/2)
			}
			else if self.delta > 100 {
				
				self.current += 1
			}
			else if self.delta < -100 {
				
				self.current -= 1
			}
			
			self.resetSlidePositions()
			
			self.swiping = false
			self.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(nextSlide), userInfo: nil, repeats: false)
		}
	}
	
	func resetSlidePositions() {
		
		UIView.animateWithDuration(0.30, delay: 0, options: UIViewAnimationOptions.CurveEaseOut.union(.AllowUserInteraction), animations: {
			
			self.slideViews[safe: self.current]?.x = 0
			self.slideViews[safe: self.current+1]?.x = self.width
			self.slideViews[safe: self.current-1]?.x = -self.width
			
			self.slideViews[safe: self.current]?.alpha = 1
			self.slideViews[safe: self.current+1]?.alpha = 1
			self.slideViews[safe: self.current-1]?.alpha = 1
			
			}, completion: nil)
		
		
		self.slideViews.forEach({ v in
			
			if v.tag > self.current+1 { v.x = self.width; v.alpha = 1 }
			if v.tag < self.current-1 { v.x = -self.width; v.alpha = 1 }
		})
		
	}
	
	func revertSlidePositions() {
		
		if let v = self.slideViews[safe: self.current] { self.bringSubviewToFront(v) }
		
		UIView.animateWithDuration(0.30, delay: 0, options: .CurveEaseOut, animations: {
			
			self.slideViews[safe: self.current]?.x = 0
			self.slideViews[safe: self.current]?.alpha = 1
			self.slideViews.last?.alpha = 0
			
			}, completion: { f in self.resetSlidePositions() })
	}
	
	func nextSlide() {
		
		self.timer?.invalidate()
		
		if self.swiping { return }
		
		self.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(nextSlide), userInfo: nil, repeats: false)
		
		if self.slides.count == 0 { return }
		
		self.current += 1
		if self.current == self.slideViews.count {
			
			self.current = 0
			self.revertSlidePositions()
			return
		}
		
		self.resetSlidePositions()
	}
	
	func refreshAll() {
		
		self.random = arc4random_uniform(1000)
		self.slideViews.forEach({ v in v.removeFromSuperview() })
		self.slideViews.removeAll()
		
		var x = 0
		for s in self.slides {
			
			let slide = UIView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
			self.addSubview(slide)
			slide.alpha = 0
			slide.tag = x
			slide.clipsToBounds = true
			slide.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToGame(_:))))
			
			let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.width, height: self.height))
			imgView.contentMode = .ScaleAspectFill
			imgView.alpha = 0
			imgView.clipsToBounds = true
			slide.addSubview(imgView)
			
			/*let b = UIButton(frame: CGRect(x: 0, y: self.height-70, width: self.width, height: 70))
			b.backgroundColor = UIColor(white: 0.2, alpha: 0.85)
			b.setBackgroundImage(UIColor(white: 0.3, alpha: 0.8).toImage(), forState: .Highlighted)
			//b.layer.cornerRadius = 18
			b.layer.masksToBounds = true
			b.tag = x
			b.addTarget(self, action: #selector(goToGame(_:)), forControlEvents: .TouchUpInside)
			b.setImage(UIImage(named: "rightArrow"), forState: .Normal)
			b.setImage(UIImage(named: "rightArrow"), forState: .Highlighted)
			slide.addSubview(b)
			
			let title = UITextView(frame: CGRect(x: 12, y: self.height-65, width: self.width, height: 70))
			title.backgroundColor = .clearColor()
			title.userInteractionEnabled = false
			slide.addSubview(title)
			
			let attStr = NSMutableAttributedString(string: "\((s.valueForKey("heading") as! String).stringByRemovingPercentEncoding!)\n", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName:UIFont.systemFontOfSize(20, weight: UIFontWeightBold)])
			attStr.appendAttributedString(NSAttributedString(string: "\((s.valueForKey("subheading") as! String).stringByRemovingPercentEncoding!)", attributes: [NSForegroundColorAttributeName:UIColor(white: 1, alpha: 0.7), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightRegular),NSKernAttributeName:0.4]))
			title.attributedText = attStr
			title.layoutIfNeeded()
			//title.sizeToFit()
			//title.width = min(title.width, self.width-28-40)
			//b.width = title.width+60
			b.imageEdgeInsets.left = b.width-40*/
			
			self.slideViews.append(slide)
			
			if (s.valueForKey("badge") as? String)?.length > 0 {
				
				let b = UILabel(frame: CGRect(x: 0, y: 64+15, width: 0, height: 34))
				b.backgroundColor = help.yellow
				b.layer.masksToBounds = true
				b.layer.cornerRadius = 5
				b.attributedText = NSAttributedString(string: (s.valueForKey("badge") as! String).uppercaseString+"  ", attributes: [NSForegroundColorAttributeName: UIColor(white: 0, alpha: 0.8), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold), NSKernAttributeName:0.5])
				b.textAlignment = .Center
				b.sizeToFit()
				b.height = 34
				b.width += 18
				b.x = self.width-(b.width-4)
				slide.addSubview(b)
			}
			
			x += 1
		}
		
		if self.slides.count > 0 { self.loadImageForSlide(0) }
		
		self.current = 0
		self.revertSlidePositions()
		self.timer?.invalidate()
	}
	
	func goToGame(r: UITapGestureRecognizer) {
		
		if let game = self.slides[safe: r.view!.tag]?.objectForKey("game") as? NSDictionary {
			
			let v = GameScreen(game: game, owner: exploreVC!, frame: CGRect(x: 0, y: 0, width: exploreVC!.view.width, height: exploreVC!.view.height))
			
			exploreVC?.showModal(v)
		}
	}
	
	func goToGameIndex() {
		
		if let game = self.slides[safe: self.current]?.objectForKey("game") as? NSDictionary {
			print(game)
			let v = GameScreen(game: game, owner: exploreVC!, frame: CGRect(x: 0, y: 0, width: exploreVC!.view.width, height: exploreVC!.view.height))
			
			exploreVC?.showModal(v)
		}
	}
	
	func refresh() {
		
		if let slideView = self.slideViews[safe: self.current] {
			
			self.slideViews.forEach({ v in if v.tag != self.current {
				UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
					v.alpha = 0
					}, completion: nil)
				} })
			
			UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
				slideView.alpha = 1
				}, completion: nil)
		}
	}
	
	func updateScroll(delta:CGFloat) {
		
		let r = abs(delta)/200
		
		self.slideViews.forEach({ v in v.subviews.forEach({ sv in
			
			if sv.isKindOfClass(UITextView.self) == true {
				
				sv.y = (self.height-65)
				sv.alpha = 1.0-r
			}
			else if sv.isKindOfClass(UIImageView.self) == true {
				
				//sv.y = (self.height/2)-(sv.height/2)
				sv.height = (self.height)
				v.height = (self.height)
			}
			else if sv.isKindOfClass(UIButton.self) == true {
				
				sv.y = self.height-70
				sv.alpha = 1.0-r
			}
			
		}) })
		
	}
	
	var random = arc4random_uniform(1000)
	
	func loadImageForSlide(s: Int) {
		
		let tempRandom = random
		
		if let link = (self.slides[safe: s]?.valueForKey("image_src") as? String)?.stringByReplacingOccurrencesOfString("https://", withString: "http://") {
			
			if let url = NSURL(string: link) {
				
				let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 10)
				request.HTTPMethod = "GET"
				
				let task = help.secretSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
					
					UIApplication.sharedApplication().networkActivityIndicatorVisible = false
					
					if response as? NSHTTPURLResponse != nil {
						
						let tempResponse = response as! NSHTTPURLResponse?
						
						if data != nil && tempResponse?.statusCode == 200 {
							
							dispatch_async(dispatch_get_main_queue(), {
								
								if tempRandom == self.random {
									
									if let imgView = self.slideViews[safe: s]?.subviews.filter({ v in if v.isKindOfClass(UIImageView.self) == true { return true }; return false }).first as? UIImageView {
										
										imgView.image = UIImage(data: data!)
										
										UIView.animateWithDuration(0.25, delay: 0, options: .CurveEaseInOut, animations: {
											imgView.alpha = 1
											}, completion: nil)
										
										if s == 0 {
											
											self.timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(self.nextSlide), userInfo: nil, repeats: false)
										}
										
										if (self.slideViews[safe: s+1]?.subviews.filter({ v in if v.isKindOfClass(UIImageView.self) == true { return true }; return false }).first as? UIImageView)?.image == nil {
											
											self.loadImageForSlide(s + 1)
										}
									}
								}
							})
						}
					}
					else {
						if error != nil {
							print(error)
						}
					}
				})
				
				UIApplication.sharedApplication().networkActivityIndicatorVisible = true
				task.resume()
			}
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
