//
//  Filter.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-18.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Filter: UIView {
	
	var bg : UIView!
	
	var added : UIButton!
	var released : UIButton!
	var grid : UIButton!
	var list : UIButton!
	
	var switchTo : Optional<(mode: Int, mode2: Int)->()> = nil
	var mode = 0
	var mode2 = 0
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.backgroundColor = UIColor(white: 0, alpha: 0.6)
		
		self.bg = UIView(frame: CGRect(x: self.width*0.12, y: self.height/2-130, width: self.width*0.76, height: 290))
		self.bg.backgroundColor = UIColor(white: 0.97, alpha: 0.97)
		self.bg.layer.masksToBounds = true
		self.bg.layer.cornerRadius = 10
		self.addSubview(self.bg)
		
		let l = UILabel(frame: CGRect(x: 0, y: 0, width: self.bg.width, height: 60))
		l.attributedText = NSAttributedString(string: "Sort by".uppercaseString, attributes: [NSForegroundColorAttributeName:help.purple1.lighterByPercentage(15), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold),NSKernAttributeName:0.5])
		l.textAlignment = .Center
		self.bg.addSubview(l)
		
		self.added = UIButton(frame: CGRect(x: 0, y: 46, width: self.bg.width, height: 44))
		self.added.setTitle("Date added", forState: .Normal)
		self.added.titleLabel?.font = UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)
		self.added.setTitleColor(help.hardBlue, forState: .Normal)
		self.added.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
		self.added.tag = 0
		self.added.addTarget(self, action: #selector(switchMode(_:)), forControlEvents: .TouchUpInside)
		self.added.imageEdgeInsets.right = 10
		self.added.imageEdgeInsets.top = 1
		self.added.titleEdgeInsets.left = 8
		self.bg.addSubview(self.added)
		
		self.released = UIButton(frame: CGRect(x: 0, y: 86, width: self.bg.width, height: 44))
		self.released.setTitle("Release date", forState: .Normal)
		self.released.titleLabel?.font = UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)
		self.released.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
		self.released.tag = 1
		self.released.addTarget(self, action: #selector(switchMode(_:)), forControlEvents: .TouchUpInside)
		self.released.imageEdgeInsets.right = 8
		self.released.imageEdgeInsets.top = 1
		self.released.titleEdgeInsets.left = 8
		self.bg.addSubview(self.released)
		
		let l2 = UILabel(frame: CGRect(x: 0, y: 144, width: self.bg.width, height: 60))
		l2.attributedText = NSAttributedString(string: "Show games in".uppercaseString, attributes: [NSForegroundColorAttributeName:help.purple1.lighterByPercentage(15), NSFontAttributeName:UIFont.systemFontOfSize(14, weight: UIFontWeightBold),NSKernAttributeName:0.5])
		l2.textAlignment = .Center
		self.bg.addSubview(l2)
		
		self.grid = UIButton(frame: CGRect(x: 0, y: 190, width: self.bg.width, height: 44))
		self.grid.setTitle("Grid mode", forState: .Normal)
		self.grid.titleLabel?.font = UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)
		self.grid.setTitleColor(help.hardBlue, forState: .Normal)
		self.grid.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
		self.grid.tag = 0
		self.grid.addTarget(self, action: #selector(switchViewMode(_:)), forControlEvents: .TouchUpInside)
		self.grid.imageEdgeInsets.right = 10
		self.grid.imageEdgeInsets.top = 1
		self.grid.titleEdgeInsets.left = 8
		self.bg.addSubview(self.grid)
		
		self.list = UIButton(frame: CGRect(x: 0, y: 230, width: self.bg.width, height: 44))
		self.list.setTitle("List mode", forState: .Normal)
		self.list.titleLabel?.font = UIFont.systemFontOfSize(20, weight: UIFontWeightRegular)
		self.list.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
		self.list.tag = 1
		self.list.addTarget(self, action: #selector(switchViewMode(_:)), forControlEvents: .TouchUpInside)
		self.list.imageEdgeInsets.right = 8
		self.list.imageEdgeInsets.top = 1
		self.list.titleEdgeInsets.left = 8
		self.bg.addSubview(self.list)
		
		self.close()
	}
	
	func refreshButtons() {
		
		if self.mode == 0 {
			
			self.added.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
			self.released.setImage(nil, forState: .Normal)
			self.added.setTitleColor(help.hardBlue, forState: .Normal)
			self.released.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
		}
		else if self.mode == 1 {
			
			self.added.setImage(nil, forState: .Normal)
			self.released.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
			self.added.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
			self.released.setTitleColor(help.hardBlue, forState: .Normal)
		}
		
		if self.mode2 == 0 {
			
			self.grid.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
			self.list.setImage(nil, forState: .Normal)
			self.grid.setTitleColor(help.hardBlue, forState: .Normal)
			self.list.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
		}
		else if self.mode2 == 1 {
			
			self.grid.setImage(nil, forState: .Normal)
			self.list.setImage(UIImage(named: "check")?.fillWithColor(help.hardBlue), forState: .Normal)
			self.grid.setTitleColor(help.purple1.darkerByPercentage(10), forState: .Normal)
			self.list.setTitleColor(help.hardBlue, forState: .Normal)
		}
	}
	
	func switchMode(button: UIButton) {
		
		self.mode = button.tag
		
		self.refreshButtons()
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			self.close()
			self.switchTo?(mode: self.mode, mode2: self.mode2)
		}
	}
	
	func switchViewMode(button: UIButton) {
		
		self.mode2 = button.tag
		
		self.refreshButtons()
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(300) * Int64(NSEC_PER_MSEC)), dispatch_get_main_queue()) {
			
			self.close()
			self.switchTo?(mode: self.mode, mode2: self.mode2)
		}
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		super.touchesBegan(touches, withEvent: event)
		
		if touches.first?.locationInView(self).x < self.bg.x || touches.first?.locationInView(self).y < self.bg.y || touches.first?.locationInView(self).x > (self.bg.x + self.bg.width) || touches.first?.locationInView(self).y > (self.bg.y + self.bg.height) {
			
			self.close()
		}
	}
	
	func open() {
		
		self.refreshButtons()
		self.userInteractionEnabled = true
		
		UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseOut.union(.AllowUserInteraction), animations: {
			
			self.bg.layer.transform = CATransform3DMakeScale(1, 1, 1)
			self.alpha = 1
			
			}, completion: nil)
	}
	
	func close() {
		
		UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseOut.union(.AllowUserInteraction), animations: {
			
			self.bg.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
			self.alpha = 0
			
			}, completion: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}