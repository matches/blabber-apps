//
//  Tab.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class Tab: UIButton {
	
	var active : Bool {
		
		get {
			
			return self.active
		}
		set(a) {
			
			if a {
				
				self.setImage(self.imageForState(.Normal)?.fillWithColor(help.yellow), forState: .Normal)
				self.setTitleColor(help.yellow, forState: .Normal)
			}
			else {
				
				self.setImage(self.imageForState(.Normal)?.fillWithColor(UIColor(white: 0.5, alpha: 1)), forState: .Normal)
				self.setTitleColor(UIColor(white: 0.5, alpha: 1).lighterByPercentage(6), forState: .Normal)
			}
			
			self.setImage(self.imageForState(.Normal), forState: .Highlighted)
		}
	}
}
