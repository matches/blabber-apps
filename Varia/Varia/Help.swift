//
//  Help.swift
//  Varia
//
//  Created by Hazim Judi on 2015-12-15.
//  Copyright © 2015 Butlered Bits. All rights reserved.
//

import UIKit

let linkFinder = try? NSDataDetector(types: NSTextCheckingType.Link.rawValue)

struct help {
    
    static let APPDELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate
    
    static let version = UIDevice.currentDevice().systemVersion
    
    static let secretSession = NSURLSession.sharedSession()
	
    static let messageFontSize = CGFloat(17)
    
    static let purple1 = hexToColor("3a4264")!
	static let dimPurple = hexToColor("737a94")!
	static let dimBlue = hexToColor("718293")!
	static let darkBlue = hexToColor("2e3440")!
	static let babyBlue = hexToColor("61bbff")!
	static let babyPurple = hexToColor("f0f1f6")!
	static let hardBlue = hexToColor("1a95f4")!
	static let yellow = hexToColor("ffd131")!
	static let textBlueDark = hexToColor("394356")!
    
    static var IS_IPHONE_6 : Bool {
        if UIScreen.mainScreen().bounds.size.height == 667.0 {
            return true
        }
        else {
            return false
        }
    }
    
    static var IS_IPHONE_6P : Bool {
        if UIScreen.mainScreen().bounds.size.height == 736.0 {
            return true
        }
        else {
            return false
        }
    }
    
    static var IS_IOS9 : Bool {
        
        if version.rangeOfString("9.") != nil { return true }
        return false
    }
}

class TextField : UITextField {
    
    var leftPadding = CGFloat(10)
    var topPadding = CGFloat(1)
    var rightPadding = CGFloat(0)
    var bottomPadding = CGFloat(0)
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        
        return CGRect(x: leftPadding, y: topPadding, width: bounds.width-(leftPadding)-(rightPadding), height: bounds.height)
    }
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        
        return CGRect(x: leftPadding, y: topPadding, width: bounds.width-(leftPadding)-(rightPadding), height: bounds.height)
    }
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        
        return CGRect(x: leftPadding, y: topPadding, width: bounds.width-(leftPadding)-(rightPadding), height: bounds.height)
    }
}

extension NSDate {
    
    func timeElapsed() -> String {
        
        let elapsed = abs(self.timeIntervalSinceNow)
        
        if elapsed > 0 && elapsed < 120 {
            
            return "Now"
        }
        else if elapsed > 120 && elapsed < 3600 {
            
            let r = NSString(format: "%.f", elapsed/120)
			if r == "1" { return "\(r) minute" }
            return "\(r) minutes"
        }
        else if elapsed > 3600 && elapsed < 86400 {
            
			let r = NSString(format: "%.f", elapsed/3600)
			if r == "1" { return "\(r) hour" }
            return "\(r) hours"
        }
        else if elapsed > 86400 && elapsed < 604800 {
            
			let r = NSString(format: "%.f", elapsed/86400)
			if r == "1" { return "\(r) day" }
            return "\(r) days"
        }
        else if elapsed > 604800 && elapsed < 2419200 {
            
			let r = NSString(format: "%.f", elapsed/604800)
			if r == "1" { return "\(r) week" }
            return "\(r) weeks"
        }
        else if elapsed > 2419200 && elapsed < 29030400 {
            
			let r = NSString(format: "%.f", elapsed/2419200)
			if r == "1" { return "\(r) month" }
            return "\(r) months"
        }
        else {
			let r = NSString(format: "%.f", elapsed/29030400)
			if r == "1" { return "\(r) year" }
            return "\(r) years"
        }
    }
}


extension UIView {
    
    func removeAllSubviews() {
        
        for subview in self.subviews {
            
            subview.removeFromSuperview()
        }
    }
    
    var x : CGFloat {
        
        get {
            return self.frame.origin.x
        }
        set(newX) {
            self.frame.origin.x = newX
        }
    }
    var y : CGFloat {
        
        get {
            return self.frame.origin.y
        }
        set(newY) {
            self.frame.origin.y = newY
        }
    }
    var width : CGFloat {
        
        get {
            return self.frame.width
        }
        set(newW) {
            self.frame.size.width = newW
        }
    }
    var height : CGFloat {
        
        get {
            return self.frame.height
        }
        set(newH) {
            self.frame.size.height = newH
        }
    }
}

func gradientImage(fromColor: UIColor, toColor: UIColor) -> UIImage {
    
    let w = help.APPDELEGATE.window!
    
    UIGraphicsBeginImageContext(w.frame.size)
    let context = UIGraphicsGetCurrentContext()!
    let locations : [CGFloat] = [ 0.0, 1 ]
    let colors = [fromColor.CGColor, toColor.CGColor]
    let colorspace : CGColorSpaceRef = CGColorSpaceCreateDeviceRGB()
    let gradient : CGGradientRef = CGGradientCreateWithColors(colorspace, colors, locations)!
    let startPoint : CGPoint = CGPoint(x: 0, y: 0)
    let endPoint : CGPoint = CGPoint(x: w.width,y: w.height)
    CGContextDrawLinearGradient(context, gradient,startPoint, endPoint, CGGradientDrawingOptions.init(rawValue: 0))
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return img!
}

func hexToColor(hex: String) -> UIColor? {
	
    if let hexIntValue = UInt(hex, radix: 16) {
        
        return UIColor(
            red: CGFloat((hexIntValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hexIntValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hexIntValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    return nil
}

func colorToHex(color: UIColor) -> String {
    
    let values = color.getRGB()
    
    return String(format: "%02x%02x%02x", Int(values[0]*255), Int(values[1]*255), Int(values[2]*255))
}

func paragraphStyleWithLineHeight(lineHeight: CGFloat?, alignment: NSTextAlignment?) -> NSParagraphStyle {
    
    let paragraphStyle = NSMutableParagraphStyle()
    
    if lineHeight != nil { paragraphStyle.lineSpacing = lineHeight! }
    if alignment != nil { paragraphStyle.alignment = alignment! }
    
    return paragraphStyle as NSParagraphStyle
}

func intOrZero(int: Int?) -> Int {
    
    if int != nil { return int! }
    return 0
}

extension Array {
    
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

extension String {
    
    var length: Int { return self.characters.count }
    
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    func toMentionsArray() -> Array<String> {
        
        var arrayOfMentions : Array<String> = []
        
        if self.length > 0 {
            
            var tempStr = self
            
            var containsMore = true
            
            while containsMore == true && tempStr.length > 0 {
                
                if let r = tempStr.rangeOfString("@") {
                    
                    if arrayOfMentions.count == 0 && r.endIndex == tempStr.endIndex { containsMore = false; break }
                    
                    var finalMentionRange = r
                    finalMentionRange.startIndex = finalMentionRange.startIndex.advancedBy(1)
                    var hitASpace = false
                    
                    while hitASpace == false {
                        if finalMentionRange.endIndex == tempStr.endIndex { hitASpace = true; break }
                        if String(tempStr[finalMentionRange.endIndex]).rangeOfCharacterFromSet(NSCharacterSet(charactersInString: " .@:!$%^&*()+=?\"\'[]\n")) != nil { hitASpace = true;break }
                        
                        finalMentionRange.endIndex = finalMentionRange.endIndex.advancedBy(1)
                    }
                    
                    if tempStr.substringWithRange(finalMentionRange) != "" {
                        
                        arrayOfMentions.append(tempStr.substringWithRange(finalMentionRange))
                    }
                    tempStr = tempStr.substringFromIndex(finalMentionRange.endIndex)
                }
                else {
                    
                    containsMore = false
                }
            }
        }
        
        return arrayOfMentions
    }
    
    
    func minifiedURL(suffix: String) -> String {
        
        var s = self.lowercaseString.stringByReplacingOccurrencesOfString("http://", withString: "", options: [], range: nil).stringByReplacingOccurrencesOfString("https://", withString: "", options: [], range: nil).stringByReplacingOccurrencesOfString("www.", withString: "", options: [], range: nil)
        
        if let r = s.rangeOfString("/") {
            
            let range = r.startIndex..<s.endIndex
            s = s.stringByReplacingCharactersInRange(range, withString: suffix)
        }
        else {
            
            s += suffix
        }
        
        return s
    }
    
    var stringByDecodingHTMLEntities : String {
        
        // ===== Utility functions =====
        
        // Convert the number in the string to the corresponding
        // Unicode character, e.g.
        //    decodeNumeric("64", 10)   --> "@"
        //    decodeNumeric("20ac", 16) --> "€"
        func decodeNumeric(string : String, base : Int32) -> Character? {
            let code = UInt32(strtoul(string, nil, base))
            return Character(UnicodeScalar(code))
        }
        
        // Decode the HTML character entity to the corresponding
        // Unicode character, return `nil` for invalid input.
        //     decode("&#64;")    --> "@"
        //     decode("&#x20ac;") --> "€"
        //     decode("&lt;")     --> "<"
        //     decode("&foo;")    --> nil
        func decode(entity : String) -> Character? {
            
            if entity.hasPrefix("&#x") || entity.hasPrefix("&#X"){
                return decodeNumeric(entity.substringFromIndex(entity.startIndex.advancedBy(3)), base: 16)
            } else if entity.hasPrefix("&#") {
                return decodeNumeric(entity.substringFromIndex(entity.startIndex.advancedBy(2)), base: 10)
            } else {
                return characterEntities[entity]
            }
        }
        
        // ===== Method starts here =====
        
        var result = ""
        var position = startIndex
        
        // Find the next '&' and copy the characters preceding it to `result`:
        while let ampRange = self.rangeOfString("&", range: position ..< endIndex) {
            result.appendContentsOf(self[position ..< ampRange.startIndex])
            position = ampRange.startIndex
            
            // Find the next ';' and copy everything from '&' to ';' into `entity`
            if let semiRange = self.rangeOfString(";", range: position ..< endIndex) {
                let entity = self[position ..< semiRange.endIndex]
                position = semiRange.endIndex
                
                if let decoded = decode(entity) {
                    // Replace by decoded character:
                    result.append(decoded)
                } else {
                    // Invalid entity, copy verbatim:
                    result.appendContentsOf(entity)
                }
            } else {
                // No matching ';'.
                break
            }
        }
        // Copy remaining characters to `result`:
        result.appendContentsOf(self[position ..< endIndex])
        return result
    }
}

private let characterEntities : [ String : Character ] = [
    // XML predefined entities:
    "&quot;"    : "\"",
    "&amp;"     : "&",
    "&apos;"    : "'",
    "&lt;"      : "<",
    "&gt;"      : ">",
    
    // HTML character entity references:
    "&nbsp;"    : "\u{00a0}",
    // ...
    "&diams;"   : "♦",
]

extension UIImage {
    
    func dim(alpha: CGFloat) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.drawInRect(CGRect(origin: CGPointZero, size: self.size), blendMode: .Normal, alpha: alpha)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func fillWithColor(color: UIColor) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let c = UIGraphicsGetCurrentContext()
        self.drawInRect(CGRect(origin: CGPointZero, size: self.size), blendMode: .Normal, alpha: 1)
        CGContextSetBlendMode(c!, CGBlendMode.SourceIn)
        CGContextSetFillColorWithColor(c!, color.CGColor)
        CGContextFillRect(c!, CGRect(origin: CGPointZero, size: self.size))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
	
	func areaAverage() -> UIColor {
		var bitmap = [UInt8](count: 4, repeatedValue: 0)
		
		if #available(iOS 9.0, *) {
			// Get average color.
			let context = CIContext()
			let inputImage = CIImage ?? CoreImage.CIImage(CGImage: CGImage!)
			let extent = inputImage.extent
			let inputExtent = CIVector(x: extent.origin.x, y: extent.origin.y, z: extent.size.width, w: extent.size.height)
			let filter = CIFilter(name: "CIAreaAverage", withInputParameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: inputExtent])!
			let outputImage = filter.outputImage!
			let outputExtent = outputImage.extent
			assert(outputExtent.size.width == 1 && outputExtent.size.height == 1)
			
			// Render to bitmap.
			context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: kCIFormatRGBA8, colorSpace: CGColorSpaceCreateDeviceRGB())
		} else {
			// Create 1x1 context that interpolates pixels when drawing to it.
			let context = CGBitmapContextCreate(&bitmap, 1, 1, 8, 4, CGColorSpaceCreateDeviceRGB(), CGBitmapInfo.ByteOrderDefault.rawValue | CGImageAlphaInfo.PremultipliedLast.rawValue)!
			let inputImage = CGImage ?? CIContext().createCGImage(CIImage!, fromRect: CIImage!.extent)
			
			// Render to bitmap.
			CGContextDrawImage(context, CGRect(x: 0, y: 0, width: 1, height: 1), inputImage!)
		}
		
		// Compute result.
		let result = UIColor(red: CGFloat(bitmap[0]) / 255.0, green: CGFloat(bitmap[1]) / 255.0, blue: CGFloat(bitmap[2]) / 255.0, alpha: CGFloat(bitmap[3]) / 255.0)
		return result
	}
}

extension UIColor {
	
	
    func toImage() -> UIImage {
		
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context!, self.CGColor);
        CGContextFillRect(context!, rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func darkerByPercentage(percentage: CGFloat) -> UIColor {
        
        var red = CGFloat(); var green = CGFloat(); var blue = CGFloat(); var a = CGFloat()
        self.getRed(&red, green: &green, blue: &blue, alpha: &a)
        
        return UIColor(red: red-(percentage/100), green: green-(percentage/100), blue: blue-(percentage/100), alpha: a)
    }
    
    func lighterByPercentage(percentage: CGFloat) -> UIColor {
        
        var red = CGFloat(); var green = CGFloat(); var blue = CGFloat(); var a = CGFloat()
        self.getRed(&red, green: &green, blue: &blue, alpha: &a)
        
        return UIColor(red: red+(percentage/100), green: green+(percentage/100), blue: blue+(percentage/100), alpha: a)
    }
    
    func getRGB() -> [CGFloat] {
        
        var red = CGFloat(); var green = CGFloat(); var blue = CGFloat(); var a = CGFloat();
        
        self.getRed(&red, green: &green, blue: &blue, alpha: &a)
        
        return [red,green,blue]
    }
}
