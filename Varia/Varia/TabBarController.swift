//
//  ViewController.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-15.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class TabBarController: UIViewController {
	
	let tabBarHeight = CGFloat(50)
	let buttonWidth = CGFloat(60)
	let buttonWeight = UIFontWeightSemibold
	let buttonTextSize = CGFloat(14)
	
	var statusBarHidden = false
	var statusBarBlack = false
	
	let profileViewController = ProfileViewController(nibName: nil, bundle: nil)
	let exploreViewController = ExploreViewController(nibName: nil, bundle: nil)
	let countdownViewController = CountdownViewController(nibName: nil, bundle: nil)
	let gamesViewController = GamesViewController(nibName: nil, bundle: nil)
	
	var tabs : [Tab] = []
	var viewControllers : [GenericViewController] = []
	
	var bar : UIVisualEffectView!
	

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
		self.viewControllers = [self.exploreViewController, self.gamesViewController, self.countdownViewController, self.profileViewController]
		
		let exploreTab = Tab(frame: CGRect(x: 0, y: 0, width: 0, height: self.tabBarHeight))
		exploreTab.setTitle("Explore", forState: .Normal)
		exploreTab.tag = 0
		exploreTab.addTarget(self, action: #selector(switchToTab(_:)), forControlEvents: .TouchDown)
		exploreTab.setImage(UIImage(named: "explore"), forState: .Normal)
		
		let gamesTab = Tab(frame: CGRect(x: 0, y: 0, width: 0, height: self.tabBarHeight))
		gamesTab.setTitle("My Games", forState: .Normal)
		gamesTab.tag = 1
		gamesTab.addTarget(self, action: #selector(switchToTab(_:)), forControlEvents: .TouchDown)
		gamesTab.setImage(UIImage(named: "games"), forState: .Normal)
		
		let countdownTab = Tab(frame: CGRect(x: 0, y: 0, width: 0, height: self.tabBarHeight))
		countdownTab.setTitle("Countdown", forState: .Normal)
		countdownTab.tag = 2
		countdownTab.addTarget(self, action: #selector(switchToTab(_:)), forControlEvents: .TouchDown)
		countdownTab.setImage(UIImage(named: "countdown"), forState: .Normal)
		
		let profileTab = Tab(frame: CGRect(x: 0, y: 0, width: 0, height: self.tabBarHeight))
		profileTab.setTitle("Profile", forState: .Normal)
		profileTab.tag = 3
		profileTab.addTarget(self, action: #selector(switchToTab(_:)), forControlEvents: .TouchDown)
		profileTab.setImage(UIImage(named: "profile"), forState: .Normal)
		
		self.tabs = [exploreTab, gamesTab, countdownTab, profileTab]
	}
	
	var blurEffect : UIVisualEffect!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor(white: 0.08, alpha: 1)//help.darkBlue.darkerByPercentage(5)
		self.view.clipsToBounds = false
		
		self.blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
		
		self.bar = UIVisualEffectView(frame: CGRect(x: 0, y: self.view.height-50, width: self.view.width, height: 50))
		self.bar.backgroundColor = UIColor(white: 0.5, alpha: 0.5)//help.darkBlue//help.purple1
		self.bar.effect = self.blurEffect
		self.view.addSubview(self.bar)
		
		self.refreshTabs()
		self.switchToTab(self.tabs.first!)
	}
	
	func refreshTabs() {
		
		var x = -1;
		
		for t in self.tabs {
			
			x += 1;
			
			t.removeFromSuperview()
			
			//
			
			t.x = ceil(self.view.width/CGFloat(self.tabs.count)) * CGFloat(x)
			t.width = self.view.width/CGFloat(self.tabs.count)
			
			t.titleLabel?.font = UIFont.systemFontOfSize(10, weight: UIFontWeightBold)
			t.setTitle(t.titleForState(.Normal)?.uppercaseString, forState: .Normal)
			t.layoutSubviews()
			t.titleEdgeInsets.top = 16
			t.titleEdgeInsets.bottom = -16
			t.titleEdgeInsets.left = -(t.imageForState(.Normal)!.size.width)
			t.imageEdgeInsets.bottom = 10
			t.imageEdgeInsets.top = -4
			t.imageEdgeInsets.left = ((t.titleLabel!.width/2)*(t.width/t.titleLabel!.width)-t.imageView!.width/2)-1
			if self.viewControllers[safe: t.tag]?.view.superview != nil { t.active = true }
			else { t.active = false }
			
			self.bar.addSubview(t)
		}
	}
	
	func switchToTab(tab: Tab) {
		
		self.tabs.forEach({ t in if t.tag == tab.tag { t.active = true; } else { t.active = false; }})
		
		if let vc = self.viewControllers[safe: tab.tag] {
			
			if vc.view.superview != nil {
				
				vc.tabTapped()
			}
			else {
				
				self.viewControllers.forEach({ vc in
					
					if vc.view.superview != nil { vc.view.removeFromSuperview() }
					vc.modals.forEach({ m in m.removeFromSuperview() })
				})
				
				self.view.insertSubview(vc.view, belowSubview: self.bar)
				
				vc.modals.forEach({ m in
					
					self.view.addSubview(m)
					
					if m.isKindOfClass(GameScreen.self) == false {
						
						self.view.bringSubviewToFront(self.bar)
					}
				})
				
			}
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		if self.statusBarBlack == true { return .Default } else { return .LightContent }
	}
	
	override func prefersStatusBarHidden() -> Bool {
		return self.statusBarHidden
	}
	
	override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
		return UIStatusBarAnimation.Slide
	}
	
	func setNeedsStatusBarAppearanceUpdate(d: Double) {
		UIView.animateWithDuration(d, delay: 0, options: .CurveEaseInOut, animations: {
			super.setNeedsStatusBarAppearanceUpdate()
			}, completion: nil)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

