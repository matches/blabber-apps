//
//  User.swift
//  Varia
//
//  Created by Hazim Judi on 2016-04-20.
//  Copyright © 2016 LC. All rights reserved.
//

import UIKit

class User {
	
	internal static var token : String?
	static var userID : String?
	static var userName : String?
	static var guest : Bool?
	static var city : String?
	static var avatar : String?
	internal static var bulkUser : NSMutableDictionary?
	
	static var notifications = false
	
	static func getTokenAndStoreAuth() -> String? {
		
		if let user = KeychainWrapper.objectForKey("user") as? [NSObject:AnyObject] {
			
			if let token = user["token"] as? String {
				
				self.token = token
			}
			else {
				
				showNotificationBar("Couldn't authenticate, try logging in.", type: .Error)
				
				return nil
			}
			
			if let id = (user["id"] as? Int)?.description {
				
				self.userID = id
			}
			if let name = (user["name"] as? String)?.stringByRemovingPercentEncoding {
				
				self.userName = name
			}
			if let city = (user["city"] as? String)?.stringByRemovingPercentEncoding {
				
				self.city = city
			}
			if let n = user["notifications"] as? Bool {
				
				self.notifications = n
			}
			
			self.bulkUser = NSMutableDictionary(dictionary: user)
			
			return token!
		}
		
		return nil
	}
	
	static func resetUserData() {
		
		self.bulkUser = nil
		self.guest = nil
		self.userID = nil
		self.userName = nil
		self.token = nil
		self.city = nil
		self.notifications = false
		KeychainWrapper.removeObjectForKey("user")
	}
	
	static func getCredentials() -> NSDictionary? {
		
		if (self.userID == nil || self.token == nil) {
			
			if getTokenAndStoreAuth() == nil {
				
				return nil
			}
		}
		
		if self.userID != nil && self.token != nil {
			
			return ["id": self.userID!, "token": self.token!]
		}
		else {
			
			return nil
		}
	}
	
	static func getUserData() -> NSDictionary? {
		
		if bulkUser != nil {
			
			return bulkUser!
		}
		else if let user = KeychainWrapper.objectForKey("user") as? [NSObject:AnyObject] {
			
			return NSDictionary(dictionary: user)
		}
		
		return nil
	}
	
	static func setNotifications(n: Bool) {
		
		self.notifications = n
		KeychainWrapper.setString(n.description, forKey: "notifications")
	}
	
	static func setUserData(data: NSDictionary) {
		
		if self.bulkUser != nil {
			
			self.bulkUser?.setValuesForKeysWithDictionary(data as! [String : AnyObject])
		}
		else { self.bulkUser = NSMutableDictionary(dictionary: data) }
		
		//
		
		if let userName = (data.valueForKey("name") as? String)?.stringByRemovingPercentEncoding {
			
			self.userName = userName
		}
		if let id = (data.valueForKey("id") as? Int)?.description {
			
			self.userID = id
		}
		if let guest = data.valueForKey("guest") as? Bool {
			
			self.guest = guest
		}
		if let n = data.valueForKey("notifications") as? Bool {
			
			self.notifications = n
		}
		if let city = (data.valueForKey("city") as? String)?.stringByRemovingPercentEncoding {
			
			self.city = city
		}
		if let token = data.valueForKey("token") as? String {
			
			self.token = token as String
			self.bulkUser?.setValue(token, forKey: "token")
		}
		
		KeychainWrapper.setObject(self.bulkUser!, forKey: "user")
		
		//Todo: refresh profile tab
	}
}